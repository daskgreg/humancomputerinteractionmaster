import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { DashboardComponent } from './pages/tablet/dashboard/dashboard.component';
import { TopsideUserBarComponent } from './pages/tablet/topside-user-bar/topside-user-bar.component';
import { SpinnerComponent } from './pages/tablet/spinner/spinner.component';
import { ConfigurationComponent } from './pages/tablet/configuration/configuration.component';
import { RecipesComponent } from './pages/tablet/recipes/recipes.component';
import { RecipesSidebarComponent } from './pages/tablet/recipes/recipes-sidebar/recipes-sidebar.component';
import { InventoryManagerComponent } from './pages/tablet/inventory-manager/inventory-manager.component';
import { ProfileComponent } from './pages/tablet/profile/profile.component';
import { SuperMarketComponent } from './pages/tablet/super-market/super-market.component';
import { KitchenComponent } from './pages/tablet/kitchen/kitchen.component';
import { HomepageComponent } from './pages/tablet/homepage/homepage.component';
import { ShoppingListComponent } from './pages/tablet/shopping-list/shopping-list.component';
import { ScanReceiptComponent } from './pages/tablet/super-market/scan-receipt/scan-receipt.component';
import { ScannedReceiptProductsComponent } from './pages/tablet/scanned-receipt-products/scanned-receipt-products.component';
import { QrScannerComponent, NgQrScannerModule } from 'angular2-qrscanner';
import { FridgeInventoryComponent } from './pages/tablet/kitchen/fridge-inventory/fridge-inventory.component';
import { CabbinetsComponent } from './pages/tablet/kitchen/cabbinets/cabbinets.component';
import { GoShoppingComponent } from './pages/tablet/super-market/go-shopping/go-shopping.component';
import { MenuBarGoshoppingComponent } from './pages/tablet/Menu Bar/menu-bar-goshopping/menu-bar-goshopping.component';
import { MenuBarRecurringComponent } from './pages/tablet/Menu Bar/menu-bar-recurring/menu-bar-recurring.component';
import { MenuBarFavouriteComponent } from './pages/tablet/Menu Bar/menu-bar-favourite/menu-bar-favourite.component';
import { MenuBarCustomProductsComponent } from './pages/tablet/Menu Bar/menu-bar-custom-products/menu-bar-custom-products.component';
import { TopSideBarRecurringComponent } from './pages/tablet/top-side-bar-general/top-side-bar-recurring/top-side-bar-recurring.component';
import { TopSideBarFavouriteComponent } from './pages/tablet/top-side-bar-general/top-side-bar-favourite/top-side-bar-favourite.component';
import { TopSideBarCustomProductComponent } from './pages/tablet/top-side-bar-general/top-side-bar-custom-product/top-side-bar-custom-product.component';
import { PageNotFoundErrorComponent } from './pages/tablet/page-not-found/page-not-found-error/page-not-found-error.component';
import { TopSideBarReceiptComponent } from './pages/tablet/top-side-bar-general/top-side-bar-receipt/top-side-bar-receipt.component';
import { MenuBarReceiptComponent } from './pages/tablet/Menu Bar/menu-bar-receipt/menu-bar-receipt.component';
import { ScannedProductIdDirective } from './global/directives/scanned-product-id.directive';
import { SearchPipe } from './global/pipes/search.pipe';
import { TestComponent } from './pages/tablet/test/test.component';
import { ProductsSearchBarComponent } from './pages/tablet/search-bars/products-search-bar/products-search-bar/products-search-bar.component';
import { MaterialComponents } from './global/material/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as Globals from './global/variables/variables';
import { BottomSideBarShoppingListComponent } from './pages/tablet/shopping-list/sidebars/bottom-side-bar-shopping-list/bottom-side-bar-shopping-list.component';
import { MenuBarShoppingListComponent } from './pages/tablet/shopping-list/sidebars/menu-bar-shopping-list/menu-bar-shopping-list.component';
import { RecurringComponent } from './pages/tablet/shopping-list/products/recurring/recurring.component';
import { CustomProductsComponent } from './pages/tablet/shopping-list/products/custom-products/custom-products.component';

import { TopSideBarShoppingListComponent } from './pages/tablet/shopping-list/sidebars/top-side-bar-shopping-list/top-side-bar-shopping-list.component';
import { BottomSideBarGoshoppingComponent } from './pages/tablet/super-market/go-shopping/sidebars/bottom-side-bar-goshopping/bottom-side-bar-goshopping.component';
import { TopSideBarGoshoppingComponent } from './pages/tablet/super-market/go-shopping/sidebars/top-side-bar-goshopping/top-side-bar-goshopping.component';
import { BottomSideBarFridgeComponent } from './pages/tablet/kitchen/fridge-inventory/sidebars/bottom-side-bar-fridge/bottom-side-bar-fridge.component';
import { TopSideBarFridgeComponent } from './pages/tablet/kitchen/fridge-inventory/sidebars/top-side-bar-fridge/top-side-bar-fridge.component';
import { TopSideBarShelvesComponent } from './pages/tablet/kitchen/shelves/sidebars/top-side-bar-shelves/top-side-bar-shelves.component';
import { BottomSideBarShelvesComponent } from './pages/tablet/kitchen/shelves/sidebars/bottom-side-bar-shelves/bottom-side-bar-shelves.component';
import { ShelvesComponent } from './pages/tablet/kitchen/shelves/shelves/shelves.component';
import { BottomSideBarCabbinetComponent } from './pages/tablet/kitchen/cabbinets/bottom-side-bar-cabbinet/bottom-side-bar-cabbinet.component';
import { TopSideBarCabbinetComponent } from './pages/tablet/kitchen/cabbinets/top-side-bar-cabbinet/top-side-bar-cabbinet.component';
import { TopSideBarScannedReceiptComponent } from './pages/tablet/scanned-receipt-products/top-side-bar-scanned-receipt/top-side-bar-scanned-receipt.component';
import { BottomSideBarProfileComponent } from './pages/tablet/profile/bottom-side-bar-profile/bottom-side-bar-profile.component';
import { TopSideBarProfileComponent } from './pages/tablet/profile/top-side-bar-profile/top-side-bar-profile.component';
import { TopSideBarInventoryManagerComponent } from './pages/tablet/inventory-manager/sidebar/top-side-bar-inventory-manager/top-side-bar-inventory-manager.component';

import { ProductsMenu } from './global/directives/product-menu.directive';
import { PoorMansPushNotificationComponent } from './pages/poor-mans-push-notification/poor-mans-push-notification.component';
import { PushNotificationService } from './global/services/push-notification/push-notification.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SpListRemoveDirective } from './global/directives/sp-list-remove.directive';
import { AllProductsComponent } from './pages/tablet/kitchen/all-products/all-products.component';
import { InventoryManagerCarouselComponent } from './pages/tablet/inventory-manager/inventory-manager-carousel/inventory-manager-carousel.component';
import { InventoryManagerProductComponent } from './pages/tablet/inventory-manager/inventory-manager-product/inventory-manager-product.component';

// Mobile Routing
import { LoginPageComponent } from './pages/mobile/login-page/login-page/login-page.component';
import { DashboardPhoneComponent } from './pages/mobile/dashboard/dashboard-phone/dashboard-phone.component';
import { MenubarComponent } from './pages/mobile/shopping-list-mobile/shopping-list-mobile/menu-bar/menubar/menubar.component';
import { InvManagerCarouselComponent } from './pages/tablet/inventory-manager/inv-manager-carousel/inv-manager-carousel.component';
import { GregisplayingComponent } from './pages/tablet/gregs-workspace/gregisplaying/gregisplaying.component';
import { GregsListComponent } from './pages/tablet/gregs-workspace/gregisplaying/gregs-list/gregs-list.component';
import { GregsItenComponent } from './pages/tablet/gregs-workspace/gregisplaying/gregs-iten/gregs-iten.component';
import { SplMobileItemComponent } from './pages/mobile/shopping-list-mobile/spl-mobile-item/spl-mobile-item.component';
import { ProductFilterPipe } from './pages/mobile/shopping-list-mobile/product-filter.pipe';
import { SuperMarketMobileComponent } from './pages/mobile/super-market-mobile/super-market-mobile.component';
import { GoshoppingMobileComponent } from './pages/mobile/super-market-mobile/goshopping-mobile/goshopping-mobile.component';
import { ScanReceiptMobileComponent } from './pages/mobile/super-market-mobile/scan-receipt-mobile/scan-receipt-mobile.component';
import { GoshoppingMobileProductComponent } from './pages/mobile/super-market-mobile/goshopping-mobile/goshopping-mobile-product/goshopping-mobile-product.component';


//npm installs
import * as Hammer from 'hammerjs';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { DragScrollModule } from 'ngx-drag-scroll';

import { ShoppingListProductComponent } from './pages/tablet/shopping-list/shopping-list-product/shopping-list-product.component';
import { FavouriteProductItemComponent } from './pages/tablet/shopping-list/products/favourite/favourite-product-item/favourite-product-item.component';
import { HomeDevicesComponent } from './pages/home-devices/home-devices.component';
import { ProductDetailsComponent } from './pages/tablet/product-details/product-details-component.component';
import { SuperMarketProductItemComponent } from './pages/tablet/super-market/super-market-product-item/super-market-product-item.component';
import { ShoppingListMobileComponent } from './pages/mobile/shopping-list-mobile/shopping-list-mobile.component';
import { PrintReceiptMobileComponent } from './pages/mobile/super-market-mobile/print-receipt-mobile/print-receipt-mobile.component';
import { ScanReceiptItemMobileComponent } from './pages/mobile/super-market-mobile/scan-receipt-item-mobile/scan-receipt-item-mobile.component';
import { HomeDevicesTestComponent } from './pages/home-devices/home-devices-test/home-devices-test.component';
import { FridgeMonitorComponent } from './pages/home-devices/fridge-monitor/fridge-monitor.component';
import { CabbinetMonitorComponent } from './pages/home-devices/cabbinet-monitor/cabbinet-monitor.component';
import { HomeDevicesNotifListComponent } from './pages/global-components/home-devices-notif-list/home-devices-notif-list.component';
import { HomeDevicesNotifItemComponent } from './pages/global-components/home-devices-notif-list/home-devices-notif-item/home-devices-notif-item.component';
import { HomeDevicesDateTimeComponent } from './pages/global-components/home-devices-date-time/home-devices-date-time.component';


// Notifications
import { NotificationsComponent } from './pages/tablet/notifications/notifications.component';
import { NotificationsDataComponent } from './pages/tablet/notifications/notifications-data/notifications-data.component';
import { NotificationItemComponent } from './pages/tablet/notifications/notifications-data/notification-item/notification-item.component';

import { NotificationsMobileComponent } from './pages/mobile/notifications-mobile/notifications-mobile.component';
import { NotificationsDataMobileComponent } from './pages/mobile/notifications-mobile/notifications-data-mobile/notifications-data-mobile.component';
import { NotificationsItemMobileComponent } from './pages/mobile/notifications-mobile/notifications-data-mobile/notifications-item-mobile/notifications-item-mobile.component';
import { VoiceAssistantLogsComponent } from './pages/global-components/voice-assistant-logs/voice-assistant-logs.component';
import { AwaitingConfirmationComponent } from './pages/tablet/kitchen/awaiting-confirmation/awaiting-confirmation.component';
import { AwaitingConfirmationProductItemComponent } from './pages/tablet/kitchen/awaiting-confirmation/awaiting-confirmation-product-item/awaiting-confirmation-product-item.component';
import { LoggerComponent } from './pages/global-components/logger/logger.component';
import { CartComponent } from './pages/tablet/super-market/go-shopping/cart/cart.component';
import { IManagerMobileComponent } from './pages/mobile/i-manager-mobile/i-manager-mobile.component';
import { InvManagerCarouselMobileComponent } from './pages/mobile/i-manager-mobile/inv-manager-carousel-mobile/inv-manager-carousel-mobile.component';
import { InvManagerProductMobileComponent } from './pages/mobile/i-manager-mobile/inv-manager-product-mobile/inv-manager-product-mobile.component';
import { InvSidebarComponent } from './pages/mobile/i-manager-mobile/inv-sidebar/inv-sidebar.component';
import { CabMobileComponent } from './pages/mobile/kitchen/cab-mobile/cab-mobile.component';
import { FridgeMobileComponent } from './pages/mobile/kitchen/fridge-mobile/fridge-mobile.component';
import { ShelvesMobileComponent } from './pages/mobile/kitchen/shelves-mobile/shelves-mobile.component';
import { CartMobileComponent } from './pages/mobile/super-market-mobile/goshopping-mobile/cart-mobile/cart-mobile.component';
import { AwaitingMobileComponent } from './pages/mobile/awaiting-mobile/awaiting-mobile.component';
import { AwaitingItemMobileComponent } from './pages/mobile/awaiting-mobile/awaiting-item-mobile/awaiting-item-mobile.component';

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    swipe: { direction: Hammer.DIRECTION_ALL },
  };
}

@NgModule({
  declarations: [
    AppComponent,
    TopsideUserBarComponent,
    SpinnerComponent,
    ConfigurationComponent,
    RecipesComponent,
    RecipesSidebarComponent,
    InventoryManagerComponent,
    ProfileComponent,
    SuperMarketComponent,
    ShoppingListComponent,
    ScanReceiptComponent,
    ScannedReceiptProductsComponent,
    KitchenComponent,
    DashboardComponent,
    HomepageComponent,
    FridgeInventoryComponent,
    CabbinetsComponent,
    GoShoppingComponent,
    MenuBarGoshoppingComponent,
    ScannedProductIdDirective,
    SearchPipe,
    TestComponent,
    ProductsSearchBarComponent,
    RecurringComponent,
    CustomProductsComponent,
    MenuBarRecurringComponent,
    MenuBarFavouriteComponent,
    MenuBarCustomProductsComponent,
    TopSideBarRecurringComponent,
    TopSideBarFavouriteComponent,
    TopSideBarCustomProductComponent,
    PageNotFoundErrorComponent,
    TopSideBarReceiptComponent,
    MenuBarReceiptComponent,
    BottomSideBarShoppingListComponent,
    MenuBarGoshoppingComponent,
    MenuBarShoppingListComponent,
    NotificationsComponent,
    TopSideBarShoppingListComponent,
    BottomSideBarGoshoppingComponent,
    TopSideBarGoshoppingComponent,
    BottomSideBarFridgeComponent,
    TopSideBarFridgeComponent,
    TopSideBarShelvesComponent,
    BottomSideBarShelvesComponent,
    ShelvesComponent,
    BottomSideBarCabbinetComponent,
    TopSideBarCabbinetComponent,
    TopSideBarScannedReceiptComponent,
    BottomSideBarProfileComponent,
    TopSideBarProfileComponent,
    TopSideBarInventoryManagerComponent,
    NotificationsDataComponent,
    InventoryManagerCarouselComponent,
    ProductFilterPipe,
    PrintReceiptMobileComponent,
    ScanReceiptItemMobileComponent,


    //Directives
    ProductsMenu,
    PoorMansPushNotificationComponent,
    SpListRemoveDirective,
    AllProductsComponent,
    LoginPageComponent,
    DashboardPhoneComponent,
    MenubarComponent,
    InventoryManagerProductComponent,
    NotificationItemComponent,
    ShoppingListProductComponent,
    FavouriteProductItemComponent,
    HomeDevicesComponent,
    ProductDetailsComponent,
    SuperMarketProductItemComponent,
    ShoppingListMobileComponent,



    InvManagerCarouselComponent,

    GregisplayingComponent,

    GregsListComponent,

    GregsItenComponent,

    SplMobileItemComponent,

    SuperMarketMobileComponent,

    GoshoppingMobileComponent,

    ScanReceiptMobileComponent,

    GoshoppingMobileProductComponent,

    NotificationsMobileComponent,

    NotificationsDataMobileComponent,

    NotificationsItemMobileComponent,

    PrintReceiptMobileComponent,

    ScanReceiptItemMobileComponent,

    HomeDevicesTestComponent,
    FridgeMonitorComponent,
    CabbinetMonitorComponent,
    HomeDevicesNotifListComponent,
    HomeDevicesNotifItemComponent,
    HomeDevicesDateTimeComponent,
    VoiceAssistantLogsComponent,
    AwaitingConfirmationComponent,
    AwaitingConfirmationProductItemComponent,
    LoggerComponent,
    CartComponent,
    IManagerMobileComponent,
    InvManagerCarouselMobileComponent,
    InvManagerProductMobileComponent,
    InvSidebarComponent,
    CabMobileComponent,
    FridgeMobileComponent,
    ShelvesMobileComponent,
    CartMobileComponent,
    AwaitingMobileComponent,
    AwaitingItemMobileComponent,


  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgQrScannerModule,
    MaterialComponents,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialComponents,
    DragScrollModule,
  ],
  providers: [{
    provide: HAMMER_GESTURE_CONFIG,
    useClass: MyHammerConfig,
  },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    InventoryManagerProductComponent,
    NotificationItemComponent,]
})

export class AppModule { }
