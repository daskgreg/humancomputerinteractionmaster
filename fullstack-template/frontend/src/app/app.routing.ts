import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/tablet/dashboard/dashboard.component';
import { ShoppingListComponent } from './pages/tablet/shopping-list/shopping-list.component';
import { KitchenComponent } from './pages/tablet/kitchen/kitchen.component';
import { HomepageComponent } from './pages/tablet/homepage/homepage.component';
import { SuperMarketComponent } from './pages/tablet/super-market/super-market.component';
import { ProfileComponent } from './pages/tablet/profile/profile.component';
import { InventoryManagerComponent } from './pages/tablet/inventory-manager/inventory-manager.component';
import { RecipesComponent } from './pages/tablet/recipes/recipes.component';
import { ConfigurationComponent } from './pages/tablet/configuration/configuration.component';
import { ScanReceiptComponent } from './pages/tablet/super-market/scan-receipt/scan-receipt.component';
import { ScannedReceiptProductsComponent } from './pages/tablet/scanned-receipt-products/scanned-receipt-products.component';
import { FridgeInventoryComponent } from './pages/tablet/kitchen/fridge-inventory/fridge-inventory.component';
import { CabbinetsComponent } from './pages/tablet/kitchen/cabbinets/cabbinets.component';
import { GoShoppingComponent } from './pages/tablet/super-market/go-shopping/go-shopping.component';
import { TestComponent } from './pages/tablet/test/test.component';
import { MenuBarRecurringComponent } from './pages/tablet/Menu Bar/menu-bar-recurring/menu-bar-recurring.component';
import { TopSideBarReceiptComponent } from './pages/tablet/top-side-bar-general/top-side-bar-receipt/top-side-bar-receipt.component';
import { PageNotFoundErrorComponent } from './pages/tablet/page-not-found/page-not-found-error/page-not-found-error.component';
import { RecurringComponent } from './pages/tablet/shopping-list/products/recurring/recurring.component';
import { CustomProductsComponent } from './pages/tablet/shopping-list/products/custom-products/custom-products.component';
import { TopSideBarShoppingListComponent } from './pages/tablet/shopping-list/sidebars/top-side-bar-shopping-list/top-side-bar-shopping-list.component';
import { BottomSideBarShoppingListComponent } from './pages/tablet/shopping-list/sidebars/bottom-side-bar-shopping-list/bottom-side-bar-shopping-list.component';
import { ShelvesComponent } from './pages/tablet/kitchen/shelves/shelves/shelves.component';
import { NotificationsComponent } from './pages/tablet/notifications/notifications.component';
import { NotificationsDataComponent } from './pages/tablet/notifications/notifications-data/notifications-data.component';
import { AllProductsComponent } from './pages/tablet/kitchen/all-products/all-products.component';


import { GregisplayingComponent } from './pages/tablet/gregs-workspace/gregisplaying/gregisplaying.component';
// mobile routing 
import { LoginPageComponent } from './pages/mobile/login-page/login-page/login-page.component';
import { DashboardPhoneComponent } from './pages/mobile/dashboard/dashboard-phone/dashboard-phone.component';
import { MenubarComponent } from './pages/mobile/shopping-list-mobile/shopping-list-mobile/menu-bar/menubar/menubar.component';
import { ProductDetailsComponent } from './pages/tablet/product-details/product-details-component.component';
import { InvManagerCarouselComponent } from './pages/tablet/inventory-manager/inv-manager-carousel/inv-manager-carousel.component';
import { SuperMarketMobileComponent } from './pages/mobile/super-market-mobile/super-market-mobile.component';
import { GoshoppingMobileComponent } from './pages/mobile/super-market-mobile/goshopping-mobile/goshopping-mobile.component';
import { ScanReceiptMobileComponent } from './pages/mobile/super-market-mobile/scan-receipt-mobile/scan-receipt-mobile.component';
import { NotificationsMobileComponent } from './pages/mobile/notifications-mobile/notifications-mobile.component';
import { NotificationsDataMobileComponent } from './pages/mobile/notifications-mobile/notifications-data-mobile/notifications-data-mobile.component';
import { NotificationsItemMobileComponent } from './pages/mobile/notifications-mobile/notifications-data-mobile/notifications-item-mobile/notifications-item-mobile.component';
import { MenuBarGoshoppingComponent } from './pages/tablet/Menu Bar/menu-bar-goshopping/menu-bar-goshopping.component';
import { MenuBarReceiptComponent } from './pages/tablet/Menu Bar/menu-bar-receipt/menu-bar-receipt.component';
import { PrintReceiptMobileComponent } from './pages/mobile/super-market-mobile/print-receipt-mobile/print-receipt-mobile.component';
import { ShoppingListMobileComponent } from './pages/mobile/shopping-list-mobile/shopping-list-mobile.component';
import { HomeDevicesTestComponent } from './pages/home-devices/home-devices-test/home-devices-test.component';
import { FridgeMonitorComponent } from './pages/home-devices/fridge-monitor/fridge-monitor.component';
import { CabbinetMonitorComponent } from './pages/home-devices/cabbinet-monitor/cabbinet-monitor.component';
import { VoiceAssistantLogsComponent } from './pages/global-components/voice-assistant-logs/voice-assistant-logs.component';
import { AwaitingConfirmationComponent } from './pages/tablet/kitchen/awaiting-confirmation/awaiting-confirmation.component';
import { CartComponent } from './pages/tablet/super-market/go-shopping/cart/cart.component';
import { IManagerMobileComponent } from './pages/mobile/i-manager-mobile/i-manager-mobile.component';
import { CabMobileComponent } from './pages/mobile/kitchen/cab-mobile/cab-mobile.component';
import { FridgeMobileComponent } from './pages/mobile/kitchen/fridge-mobile/fridge-mobile.component';
import { ShelvesMobileComponent } from './pages/mobile/kitchen/shelves-mobile/shelves-mobile.component';
import { CartMobileComponent } from './pages/mobile/super-market-mobile/goshopping-mobile/cart-mobile/cart-mobile.component';
import { AwaitingItemMobileComponent } from './pages/mobile/awaiting-mobile/awaiting-item-mobile/awaiting-item-mobile.component';
import { AwaitingMobileComponent } from './pages/mobile/awaiting-mobile/awaiting-mobile.component';


const routes: Routes = [

  // { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) },
  { path: 'homepage', component: HomepageComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'shopping-list', component: ShoppingListComponent },
  { path: 'kitchen', component: KitchenComponent },
  { path: 'super-market', component: SuperMarketComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'inventory-manager', component: InventoryManagerComponent },
  { path: 'recipes', component: RecipesComponent },
  { path: 'configuration', component: ConfigurationComponent },
  { path: 'scan-receipt', component: ScanReceiptComponent },
  { path: 'scanned-receipt-products', component: ScannedReceiptProductsComponent },
  { path: 'fridge', component: FridgeInventoryComponent },
  { path: 'cabbinet', component: CabbinetsComponent },
  { path: 'go-shopping', component: GoShoppingComponent },
  { path: 'menubar-goshopping', component: MenuBarGoshoppingComponent },
  { path: 'test', component: TestComponent },
  { path: 'recurring-products', component: RecurringComponent },
  { path: 'custom-products', component: CustomProductsComponent },
  { path: 'menu-bar-reccuring', component: MenuBarRecurringComponent },
  { path: 'top-side-receipt', component: TopSideBarReceiptComponent },
  { path: 'menu-bar-receipt', component: MenuBarReceiptComponent },
  { path: 'top-side-bar-shopping-list', component: TopSideBarShoppingListComponent },
  { path: 'bottom-side-bar-shoppping-list', component: BottomSideBarShoppingListComponent },
  { path: 'shelves', component: ShelvesComponent },
  { path: 'notifications', component: NotificationsDataComponent },
  { path: 'product_details/:id', component: ProductDetailsComponent },
  { path: 'notifications-item-mobile', component: NotificationsItemMobileComponent },
  { path: 'inv-carousel', component: InvManagerCarouselComponent },
  // mobile routing starts
  { path: 'loginpage-mobile', component: LoginPageComponent },
  { path: 'dashboard-mobile', component: DashboardPhoneComponent },
  { path: 'shoppinglist-mobile', component: ShoppingListMobileComponent },
  { path: 'shoppinglist-menubar-mobile', component: MenubarComponent },
  { path: 'gregspace', component: GregisplayingComponent },
  { path: 'super-market-mobile', component: SuperMarketMobileComponent },
  { path: 'goshopping-mobile', component: GoshoppingMobileComponent },
  { path: 'scan-receipt-mobile', component: ScanReceiptMobileComponent },
  { path: 'notifications-mobile', component: NotificationsMobileComponent },
  { path: 'notifications-m', component: NotificationsDataMobileComponent },
  { path: 'notifications-item', component: NotificationsItemMobileComponent },
  { path: 'print-receipt-mobile', component: PrintReceiptMobileComponent },
  { path: 'i-manager-mobile', component: IManagerMobileComponent },
  { path: 'print-receipt-mobile', component: PrintReceiptMobileComponent },
  { path: 'home-devices-test', component: HomeDevicesTestComponent },
  { path: 'home-devices-fridge', component: FridgeMonitorComponent },
  { path: 'home-devices-cabbinets', component: CabbinetMonitorComponent },
  { path: 'voice-assistant', component: VoiceAssistantLogsComponent },
  { path: 'awaiting-confirmation', component: AwaitingConfirmationComponent },
  { path: 'in-cart', component: CartComponent },
  { path: 'fridge-mobile', component: FridgeMobileComponent },
  { path: 'cab-mobile', component: CabMobileComponent },
  { path: 'shelves-mobile', component: ShelvesMobileComponent },
  { path: 'cart-mobile', component: CartMobileComponent },
  { path: 'await-mobile', component: AwaitingMobileComponent },
  // mobike routing ends   


  //testing
  { path: 'home-devices', loadChildren: () => import('./pages/home-devices/home-devices.module').then(m => m.HomeDevicesModule) },

  // { path: 'socket-events', loadChildren: () => import('./pages/socket-events/socket-events.module').then(m => m.SocketEventsModule) },
  // { path: 'tasks', loadChildren: () => import('./pages/tasks/tasks.module').then(m => m.TasksModule) },
  { path: '', redirectTo: 'homepage', pathMatch: 'full' },
  { path: '**', component: PageNotFoundErrorComponent },


  // mobile routing 


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

