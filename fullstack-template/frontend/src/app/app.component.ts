import { Component, ChangeDetectorRef, AfterViewInit, HostListener, OnInit } from '@angular/core';
import { SocketsService } from './global/services';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';

import { CustomService } from './global/services/custom/custom.service'
import { TopsideUserBarComponent } from './pages/tablet/topside-user-bar/topside-user-bar.component';
import { UserSessionService } from './global/services/session/user-session.service';
import { SuperMarketSingletonService } from './global/services/frontend-services/super-market-singleton/super-market-singleton.service';
import { NotificationsService } from './global/services/notifications/notifications.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'ami-fullstack-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public loggedIn = false;
  public loaded = false;
  public loadedImg = false;
  public isLoading: boolean;

  public superMarketData;

  constructor(private socketsService: SocketsService,
    private router: Router,
    private custom: CustomService,
    private superMarketSingleton: SuperMarketSingletonService,
    private notification: NotificationsService,
  ) {

    this.socketsService.initAndConnect();

    // this.socketsService.syncAllMessages().subscribe((data) => {
    //   console.log("data");
    //   console.log(data);
    // });

    this.isLoading = false;

    router.events.subscribe((val) => {
      new TopsideUserBarComponent(this.socketsService, router, custom, this.notification);
    });


    this.superMarketSingleton.getData().subscribe(data => {
      this.superMarketData = data;
      console.log("changed Data");
      console.log(this.superMarketData);
    })
  }

  ngOnInit(): void {
    // var ua = navigator.userAgent;

    // if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua)) {
    //   this.custom.redirect('loginpage-mobile');
    // } else if (/Chrome/i.test(ua)) {
    //   this.custom.redirect('homepage');
    // } else
    //   this.custom.redirect('homepage');
  }
  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    new TopsideUserBarComponent(this.socketsService, this.router, this.custom, this.notification);
  }

  //TESTING
  redirectToAnotherModule() {
    console.log("red");
    this.custom.redirect('home-devices');
  }

}
