import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GregsListComponent } from './gregs-list.component';

describe('GregsListComponent', () => {
  let component: GregsListComponent;
  let fixture: ComponentFixture<GregsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GregsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GregsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
