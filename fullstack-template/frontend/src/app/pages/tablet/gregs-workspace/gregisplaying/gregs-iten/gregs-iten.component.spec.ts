import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GregsItenComponent } from './gregs-iten.component';

describe('GregsItenComponent', () => {
  let component: GregsItenComponent;
  let fixture: ComponentFixture<GregsItenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GregsItenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GregsItenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
