import { Component, OnInit, Input, ViewChildren, QueryList, ElementRef, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ViewEncapsulation} from '@angular/core';
import { SuperMarketProductsService } from 'src/app/global/services/products/super-market-products/super-market-products.service';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { ShoppingListProduct } from 'src/app/global/models/products/shopping-list-product.model';
import { ShoppingListComponent } from '../../shopping-list.component';
import { PushNotificationService } from 'src/app/global/services/push-notification/push-notification.service';
import { PoorMansPushNotificationComponent } from 'src/app/pages/poor-mans-push-notification/poor-mans-push-notification.component';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { CustomService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-bottom-side-bar-shopping-list',
  templateUrl: './bottom-side-bar-shopping-list.component.html',
  styleUrls: ['./bottom-side-bar-shopping-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BottomSideBarShoppingListComponent implements OnInit {

  
public showMenu = false;
public showFilter = false;
public LeftBottomBarTxtIcon = true;
public addProduct = false;
public whereToSearch = this.supermarketProd;
  constructor(private router: Router,
    private supermarketProd : SuperMarketProductsService,
    private shoppingListProd : ShoppingListProductService,
    private pushNotification : PoorMansPushNotificationComponent,
    private custom : CustomService) { }

  ngOnInit() {
    // this.initLocked();
  }

  redirect(target:String){
    console.log(target);
    this.router.navigate([target]);
  }

  menuController(){
    this.showMenu = !this.showMenu;
    this.showFilter = false;
  }

  filterController(){
    this.showFilter = !this.showFilter;
    this.showMenu = false;
  }
  productController(){
    this.showMenu = false;
    this.showFilter = false;
    this.addProduct = !this.addProduct;
  }

  // filterClicked(){
  //   var p = document.getElementById('product-list');
  //   var l = p.childNodes.length;
  //   while(p.firstChild){
  //     if(p.childNodes.length == 1){
  //       break;
  //     }
  //       p.removeChild(p.lastChild);
  //   }
  //   console.log(p);
  // }
  filterClicked(category){
    var p = document.getElementById('product-list');
    var l = p.childNodes.length;
    while(p.firstChild){
      if(p.childNodes.length == 1){
        break;
      }
        p.removeChild(p.lastChild);
    }
    console.log(p);
  }
  
}
