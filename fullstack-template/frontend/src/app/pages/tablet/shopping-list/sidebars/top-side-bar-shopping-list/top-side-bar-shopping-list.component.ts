import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewEncapsulation} from '@angular/core';
import { CustomService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-top-side-bar-shopping-list',
  templateUrl: './top-side-bar-shopping-list.component.html',
  styleUrls: ['./top-side-bar-shopping-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TopSideBarShoppingListComponent implements OnInit {

  today: number = Date.now();

  constructor(private router: Router,
    private custom : CustomService) {
    setInterval(() => {this.today = Date.now()}, 1);
  }
  
  ngOnInit() {
    this.setUserImg();
  }

  redirect(target:String){
    console.log(target);
    this.router.navigate([target]);
  }

  setUserImg(){
    if(localStorage.getItem('userData')){
      var userData = localStorage.getItem('userData');
      var imgType = JSON.parse(userData).image_type;
      var base64Img = JSON.parse(userData).image_data;
      (document.getElementById('user-profile') as HTMLImageElement).src=
      "data: " + imgType + ";base64, " + base64Img;
    }
  }
  
}
