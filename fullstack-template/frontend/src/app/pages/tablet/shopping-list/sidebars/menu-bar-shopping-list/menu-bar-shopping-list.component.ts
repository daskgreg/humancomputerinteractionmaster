import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'ami-fullstack-menu-bar-shopping-list',
  templateUrl: './menu-bar-shopping-list.component.html',
  styleUrls: ['./menu-bar-shopping-list.component.scss']
})
export class MenuBarShoppingListComponent implements OnInit {
  public showMenu = false;
  public LeftBottomBarTxtIcon = true;

  
  constructor(private router : Router) { }

  ngOnInit() {
  }

  redirect(target:String){
    this.router.navigate([target]);
  }

  MenuBarClose(){
    this.showMenu = false;
    this.LeftBottomBarTxtIcon = true;

  }
}
