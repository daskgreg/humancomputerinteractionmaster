import { Component, OnInit, ViewEncapsulation, Input, ViewChildren, QueryList, ElementRef, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentRef, ReflectiveInjector } from '@angular/core';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { Router } from '@angular/router';
import { SuperMarketProductsService } from 'src/app/global/services/products/super-market-products/super-market-products.service';
import { ShoppingListProduct } from 'src/app/global/models/products/shopping-list-product.model';
import { CustomService, SocketsService } from 'src/app/global/services';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { PoorMansPushNotificationComponent } from '../../poor-mans-push-notification/poor-mans-push-notification.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Product } from 'src/app/global/models/products/product.model';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { Notification } from 'rxjs';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { ShoppingListDeleteProducts } from 'src/app/global/services/frontend-services/shopping-list-delete-product.ts/shopping-list-delete-products';
import { ProductsService } from 'src/app/global/services/products/products/products.service';

@Component({
  selector: 'ami-fullstack-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class ShoppingListComponent implements OnInit {

  public spProducts$: ShoppingListProduct[];
  public totalProducts = 0;

  //ngIf Booleans    
  public showShoppingList: boolean = false;
  public showProductsBool: boolean = true;
  public addProduct: boolean = false;
  public showMenu: boolean = false;
  public showFilter: boolean = false;
  public LeftBottomBarTxtIcon: boolean = true;
  public showSearch = false;
  public showDropDown = false;
  public change_z_index = false;
  public change_z_index_clicked_search = false;
  public focused = false;

  //search
  public listSize;
  public search: any = '';
  productsArray: any = [];
  public clickedProduct = false;
  public selectedProdFromSearch;

  //filters
  public filterProductsTotal = 0;

  //rec-fav
  public showingRecurring = false;
  public showingFavourite = false;

  constructor(
    private supermarketProdService: SuperMarketProductsService,
    private router: Router,
    private customService: CustomService,
    private shoppingListProdService: ShoppingListProductService,
    private pushNotification: PoorMansPushNotificationComponent,
    private componentFactoryResolver: ComponentFactoryResolver,
    private notificationService: NotificationsService,
    private shoppingListDeleteService: ShoppingListDeleteProducts,
    private productService: ProductsService,
    private socketsService: SocketsService
  ) {
    this.shoppingListDeleteService.deletionDetected.subscribe(
      (status: boolean) => {
        console.log(status)
        this.showShoppingList = false;
        this.fetchShoppingList();
        // this.shoppingListDeleteService.updateStatus(false);
        // this.shoppingListDeleteService.deletionDetected.emit(false);
      }
    )

    var searchBox = document.querySelectorAll('.search-box input[type="text"] + span');

    searchBox.forEach((elm: any) => {
      elm.addEventListener('click', () => {
        elm.previousElementSibling.value = '';
      });
    });
  }

  ngOnInit() {
    this.socketsService.syncMessages("scanned-receipt").subscribe((data) => {
      this.fetchShoppingList();
    });

    this.fetchShoppingList();
    this.initLocked();
  }

  public async fetchShoppingList() {
    try {
      var shoppingListArray: any = await this.shoppingListProdService
        .getAllProductNames()
        .toPromise();

      if (!shoppingListArray) {
        console.log("ERROR - fetchShoppingList - shoppingListArray is null");
        return;
      }
      if (!shoppingListArray.success) {
        console.log("ERROR - fetchShoppingList - shoppingListArray failed");
        return;
      }
      this.showShoppingList = true;
      this.spProducts$ = shoppingListArray.data;
      // this.reloadData(shoppingListArray);
    } catch (e) {
      console.error(e);
    }
  }

  public async loadProductsByCategory(category) {
    var categorizedProd: any;
    try {
      if (category == "all") {
        categorizedProd = await this.shoppingListProdService
          .getAllProductNames()
          .toPromise();
      } else {
        categorizedProd = await this.shoppingListProdService
          .getAllProductsByCategory(category)
          .toPromise();
      }

      if (!categorizedProd) {
        console.log("keno");
        return;
      }

      if (!categorizedProd.success) {
        console.log("keno");
        return;
      }

      if (categorizedProd.data.length == 0) {
        console.log("keno");
        return;
      }

      this.reloadData(categorizedProd.data);
    } catch (e) {
      console.error(e);
    }
  }

  filterClicked(category) {
    this.loadProductsByCategory(category);
  }

  getRecurringFromSPList(e) {
    //"../images/menuBarIcons/shoppingList/recurring-btn@2x.png"
    if (this.showingRecurring) {
      (document.getElementById(e.id) as HTMLButtonElement).classList.remove('mbio-rec-fav-btn-sp-list');
      (document.getElementById(e.id) as HTMLButtonElement).classList.add('mbio-recurring-btn');
      this.fetchShoppingList();
    } else {
      (document.getElementById(e.id) as HTMLButtonElement).classList.add('mbio-rec-fav-btn-sp-list');
      (document.getElementById(e.id) as HTMLButtonElement).classList.remove('mbio-recurring-btn');
      this.getAllRecFavProducts('recurring');
    }
    this.showingRecurring = !this.showingRecurring;
  }

  getFavouritesFromSPList(e) {
    if (this.showingFavourite) {
      (document.getElementById(e.id) as HTMLButtonElement).classList.remove('mbio-rec-fav-btn-sp-list');
      (document.getElementById(e.id) as HTMLButtonElement).classList.add('mbio-favourite-btn');
      this.fetchShoppingList();
    } else {
      (document.getElementById(e.id) as HTMLButtonElement).classList.add('mbio-rec-fav-btn-sp-list');
      (document.getElementById(e.id) as HTMLButtonElement).classList.remove('mbio-favourite-btn');
      this.getAllRecFavProducts('favourite');
    }
    this.showingFavourite = !this.showingFavourite;
  }

  public async getAllRecFavProducts(recORfav: string) {
    var productsArray;
    console.log(recORfav);
    try {
      if (recORfav == "recurring") {
        productsArray = await this.productService
          .getRecurringProducts()
          .toPromise();
      } else if (recORfav == "favourite") {
        productsArray = await this.productService
          .getFavouriteProducts()
          .toPromise();
      } else {
        console.log("ERROR - getAllRecFavProducts - recORfav is wrong!");
      }

      if (!productsArray) {
        console.log("ERROR - getAllRecFavProducts - productsArray is null!");
        return;
      }

      if (!productsArray.success) {
        console.log("ERROR - getAllRecFavProducts - getRec or getFav failed");
        return;
      }

      this.reloadData(productsArray.data);
    } catch (e) {
      console.error(e);
    }
  }

  reloadData(spProductsArray: ShoppingListProduct[]) {
    this.showShoppingList = false;
    this.showShoppingList = true;
    this.spProducts$ = spProductsArray;
  }

  openSearchBar() {
    var searchBoxMini = document.getElementById("search-box-mini");
    var searchBoxOpen = document.getElementById("search-box-open");

    if (searchBoxMini.style.display === "block") {
      searchBoxMini.style.display = "none";
      searchBoxOpen.style.display = "block";
    } else {
      searchBoxMini.style.display = "none";
      searchBoxOpen.style.display = "block";
    }
  }

  closeSearchBar() {
    var searchBoxMini = document.getElementById("search-box-mini");
    var searchBoxOpen = document.getElementById("search-box-open");

    if (searchBoxOpen.style.display === "block") {
      searchBoxOpen.style.display = "none";
      searchBoxMini.style.display = "block";
    } else {
      searchBoxOpen.style.display = "none";
      searchBoxMini.style.display = "block";
      alert("skata");
    }
  }

  OpenPopMenu() {
    var Prod1 = document.getElementById('shopping-list-background-product1');
    var Prod1Pop = document.getElementById('shopping-list-pop-up-menu');
    var RecPop = (document.getElementsByClassName('rec-p1-btn') as HTMLCollectionOf<any>)[0];
    var FavPop = (document.getElementsByClassName('fav-p1-btn') as HTMLCollectionOf<any>)[0];

    if (Prod1.style.display === 'none') {
      Prod1.style.display = 'block';
    } else {
      Prod1.style.display = 'none';
      Prod1Pop.style.display = 'block';
      RecPop.style.display = 'block';
      FavPop.style.display = 'block';
    }

  }

  ClosePopMenu() {
    var Prod1 = document.getElementById('shopping-list-background-product1');
    var Prod1Pop = document.getElementById('shopping-list-pop-up-menu');
    var RecPop = (document.getElementsByClassName('rec-p1-btn') as HTMLCollectionOf<any>)[0];
    var FavPop = (document.getElementsByClassName('fav-p1-btn') as HTMLCollectionOf<any>)[0];

    if (Prod1Pop.style.display === 'block') {
      Prod1Pop.style.display = 'none';
      RecPop.style.display = 'none';
      FavPop.style.display = 'none';
      Prod1.style.display = 'block';
    } else {
      console.log('poutses mple');
    }
  }

  public async initLocked() {
    try {
      var d: any = await this.supermarketProdService
        .getAllProductNames()
        .toPromise();

      // var d : any= await this.productsService//REMOVE
      // .getAllProductNames()
      // .toPromise();


      this.listSize = d.products_list.length;

      for (var i = 0; i < d.products_list.length; i++) {
        var t;

        t = {
          "Index": i,
          "Name": d.products_list[i].product_details.name.toString(),
          "UniqueId": d.products_list[i].product_details.id.toString()
        }
        // }
        this.productsArray[i] = t;
      };

    } catch (e) {
      console.error(e);
    }
  }

  searchInputCheck(input) {
    if (input === "") {
      this.showDropDown = false;

    }

    if (input) {
      if (input.length == 0) {
        this.showDropDown = false;
      } else {
        this.showDropDown = true;
      }
    }
  }

  @ViewChildren("searchBar") searchBar: QueryList<ElementRef>;
  searchBarController() {
    this.showSearch = !this.showSearch;
    this.change_z_index_clicked_search = true;
    this.focused = true;
    document.getElementById('SearchContainer').style.zIndex = "9";


    this.showProductsBool = false;
    // if(document.getElementById('searchBar')){
    //   (document.getElementById('searchBar') as HTMLInputElement).value="";
    // }
    this.searchBar.changes.subscribe(() => {
      this.focusSearch();
    })
  }

  focusSearch() {
    if (!this.showSearch) return;
    document.getElementById('searchBar').focus();

    if (document.getElementById('searchBar') && this.focused) {
      console.log("reset");
      (document.getElementById('searchBar') as HTMLInputElement).value = "";
      console.log("reset" + (document.getElementById('searchBar') as HTMLInputElement).value);
      this.focused = false;
    }
  }

  clickedProductFromSearchElements(prodName: string, prodArrayId: number) {
    (document.getElementById('searchBar') as HTMLInputElement).value = prodName;
    this.showDropDown = false;
    // this.addProductToSPList(prodName, prodArrayId);
    this.selectedProdFromSearch = this.productsArray[prodArrayId];
  }


  public async addProductToSPList() {
    var element = this.selectedProdFromSearch;
    var addedProdObj;

    if ((document.getElementById('product_to_add_to_splist_ammount') as HTMLInputElement).value === "") {
      console.log("input is null");
      var n: PushNotification = {
        message: "Please provide ammount for " + element.name,
        source: "Shopping List"
      };
      this.pushNotification.generateNotification(n, false);
      return;
    }

    this.clickedProduct = true;
    this.focusOut();

    try {
      var superMarketProd: any = await this.supermarketProdService
        .getSuperMarketProductById(element.UniqueId)
        .toPromise();

      if (!superMarketProd) {
        console.log("Error - getSuperMarketProductById");
        return;
      }

      // console.log(superMarketProd.products_list);

      var prod2add: ShoppingListProduct = {
        product_details: {
          id: superMarketProd.products_list.product_details.prod_id,
          name: superMarketProd.products_list.product_details.name
        },
        details: {
          ammount_to_buy: parseInt((document.getElementById('product_to_add_to_splist_ammount') as HTMLInputElement).value),
          category: superMarketProd.products_list.details.category,
          ammount_unit: superMarketProd.products_list.details.ammountType,
          tags: superMarketProd.products_list.details.tags
        },
        image: {
          image_data: superMarketProd.products_list.image.image_data.toString(),
          image_type: superMarketProd.products_list.image.image_type

        }
      }

      var check: any = await this.shoppingListProdService
        .checkIfProductIsInShoppingList(element.UniqueId)
        .toPromise();

      if (!check) {
        console.log("Error - getSuperMarketProductById - check null");
        return;
      }

      if (check.exists) {
        var push: PushNotification = {
          message: "Product already exists in shopping list!",
          source: "Shopping List"
        };
        this.pushNotification.generateNotification(push, false);
        console.log("Error - already added");
        return;
      }

      var addProd2ShopList: any = this.shoppingListProdService
        .addProductToShoppingList(prod2add)
        .toPromise();

      if (!addProd2ShopList) {
        console.log("Error - getSuperMarketProductById - addProd2ShopList null");
        return;
      }

      console.log("added");

      this.showShoppingList = false;
      this.fetchShoppingList();
      console.log(superMarketProd);
      addedProdObj = superMarketProd.products_list;

      this.shoppingListProdService.addProductToShoppingList(element.UniqueId);
    } catch (e) {
      console.error(e);
    }

    var pushNotif: PushNotification = {
      message: prod2add.product_details.name.toString() + " added to shopping list",
      source: "Shopping List",
    };

    var notif: SIMNotification = {
      id: "",
      index: 0,
      severity: "info",
      source: "Shopping List",
      message: prod2add.product_details.name.toString() + " added to shopping list",
      isRead: false
    }

    this.pushNotification.generateNotification(pushNotif, false);
    // this.notification.createNotification(notif);
    this.createNotification(notif);

    this.productController();

    console.log("addedProdObj");
    console.log(addedProdObj);
    console.log(prod2add);


    this.totalProducts++;
  }

  public async clickedProdFomSearch(id) {
    var addedProdObj;
    if ((document.getElementById('product_to_add_to_splist_ammount') as HTMLInputElement).value === "") {
      console.log("input is null");
      return;
    }

    var element = this.productsArray[id]
    this.clickedProduct = true;
    this.focusOut();

    try {
      var superMarketProd: any = await this.supermarketProdService
        .getSuperMarketProductById(element.UniqueId)
        .toPromise();

      if (!superMarketProd) {
        console.log("Error - getSuperMarketProductById");
        return;
      }

      // console.log(superMarketProd.products_list);

      var prod2add: ShoppingListProduct = {
        product_details: {
          id: superMarketProd.products_list.product_details.prod_id,
          name: superMarketProd.products_list.product_details.name
        },
        details: {
          ammount_to_buy: parseInt((document.getElementById('product_to_add_to_splist_ammount') as HTMLInputElement).value),
          category: superMarketProd.products_list.details.category,
          ammount_unit: superMarketProd.products_list.details.ammountType,
          tags: superMarketProd.products_list.details.tags
        },
        image: {
          image_data: superMarketProd.products_list.image.image_data.toString(),
          image_type: superMarketProd.products_list.image.image_type

        }
      }

      var check: any = await this.shoppingListProdService
        .checkIfProductIsInShoppingList(element.UniqueId)
        .toPromise();

      if (!check) {
        console.log("Error - getSuperMarketProductById - check null");
        return;
      }

      if (check.exists) {
        console.log("Error - already added");
        return;
      }

      var addProd2ShopList: any = this.shoppingListProdService
        .addProductToShoppingList(prod2add)
        .toPromise();

      if (!addProd2ShopList) {
        console.log("Error - getSuperMarketProductById - addProd2ShopList null");
        return;
      }

      console.log("added");

      this.showShoppingList = false;
      this.fetchShoppingList();
      console.log(superMarketProd);
      addedProdObj = superMarketProd.products_list;

      this.shoppingListProdService.addProductToShoppingList(element.UniqueId);
    } catch (e) {
      console.error(e);
    }



    var pushNotif: PushNotification = {
      message: prod2add.product_details.name.toString() + " added to shopping list",
      source: "Shopping List",
    };

    var notif: SIMNotification = {
      id: "",
      index: 0,
      severity: "info",
      source: "Shopping List",
      message: prod2add.product_details.name.toString() + " added to shopping list",
      isRead: false
    }

    this.pushNotification.generateNotification(pushNotif, false);
    // this.notification.createNotification(notif);
    this.createNotification(notif);

    this.productController();

    console.log("addedProdObj");
    console.log(addedProdObj);
    console.log(prod2add);

    this.totalProducts++;
  }

  productController() {
    this.showMenu = false;
    this.showFilter = false;
    this.addProduct = !this.addProduct;
  }

  focusOut() {
    if (this.clickedProduct) {
      this.showSearch = false;
      this.showProductsBool = false;
      this.clickedProduct = false;
    } else {
      this.showSearch = false;
      this.showProductsBool = false;
    }
  }

  containerClicked() {
    console.log("click!");
    if (!this.change_z_index_clicked_search) {
      document.getElementById('SearchContainer').style.zIndex = "0";
      this.change_z_index_clicked_search = false;
      if (this.clickedProduct) {
        this.showSearch = false;
        this.showProductsBool = false;
        this.clickedProduct = false;
      } else {
        this.showSearch = false;
        this.showProductsBool = false;
      }
    }
    this.change_z_index_clicked_search = false;

  }

  public async createNotification(n) {
    try {
      var generatedNotif = await this.notificationService
        .createNotification(n)
        .toPromise();

      if (!generatedNotif) {
        console.log("ERROR - createNotification - generatedNotif is null");
        return;
      }
    } catch (e) {
      console.error(e);
    }
  }

  menuController() {
    this.showMenu = !this.showMenu;
    this.showFilter = false;
  }

  filterController() {
    this.showFilter = !this.showFilter;
    this.showMenu = false;
  }

  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }
}
