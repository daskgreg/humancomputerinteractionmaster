import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'ami-fullstack-recurring',
  templateUrl: './recurring.component.html',
  styleUrls: ['./recurring.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RecurringComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
