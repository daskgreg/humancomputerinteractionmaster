import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/global/models/products/product.model';

@Component({
  selector: 'ami-fullstack-favourite-product-item',
  templateUrl: './favourite-product-item.component.html',
  styleUrls: ['./favourite-product-item.component.scss']
})
export class FavouriteProductItemComponent implements OnInit {
  @Input() product : Product;
  constructor() { }

  ngOnInit() {
  }

}
