import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/global/models/products/product.model';
import { ProductsService } from 'src/app/global/services/products/products/products.service';

@Component({
  selector: 'ami-fullstack-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.scss']
})
export class FavouriteProductComponent implements OnInit {

  public favouriteProducts$: Product;
  public showFavProducts = false;
  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.getFavouriteProducts();
  }

  public async getFavouriteProducts() {
    try {
      var favouriteProductsArray: any = await this.productsService
        .getFavouriteProducts()
        .toPromise();

      if (!favouriteProductsArray || !favouriteProductsArray.success) {
        console.log("ERROR - getFavouriteProducts - favouriteProductsArray");
        return;
      }

      this.showFavProducts = true;

      this.favouriteProducts$ = favouriteProductsArray;
      console.log(favouriteProductsArray);
    } catch (e) {
      console.error(e);
    }
  }
  
}
