import { Component, OnInit, Input } from '@angular/core';
import { ShoppingListProduct } from 'src/app/global/models/products/shopping-list-product.model';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { Product } from 'src/app/global/models/products/product.model';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { PoorMansPushNotificationComponent } from 'src/app/pages/poor-mans-push-notification/poor-mans-push-notification.component';
import { ShoppingListDeleteProducts } from 'src/app/global/services/frontend-services/shopping-list-delete-product.ts/shopping-list-delete-products';
import { CustomService } from 'src/app/global/services';
import { ActivatedRoute, Router } from '@angular/router';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ami-fullstack-shopping-list-product',
  templateUrl: './shopping-list-product.component.html',
  styleUrls: ['./shopping-list-product.component.scss']
})
export class ShoppingListProductComponent implements OnInit {
  @Input() index: number;
  @Input() product: Product;
  // @Input() id: string;

  public imgSrc;

  constructor(
    private shoppingListService: ShoppingListProductService,
    private notificationService: NotificationsService,
    private push: PoorMansPushNotificationComponent,
    private shoppingListDeleteService: ShoppingListDeleteProducts,
    private customService: CustomService,
    private route: ActivatedRoute,
    private router: Router,
    private dom: DomSanitizer
  ) {
  }

  ngOnInit() {
    this.loadImg();
  }

  public loadImg() {
    //working
    var imgData: any = this.product.image.image_data;
    imgData = imgData.data;
    var temp = this.customService._arrayBufferToBase64(imgData);
    var t = "data:" + this.product.image.image_type + "; base64, " + temp;
    this.imgSrc = this.dom.bypassSecurityTrustUrl(t);
  }

  plusAmmountBtn() {
    var ammount = parseInt((document.getElementById('productammount-' + this.product.product_details.id) as HTMLInputElement).value);
    var a = ammount;
    if (this.product.details.ammount_unit == "pcs") {
      a += 1;
    } else if (this.product.details.ammount_unit == "gr") {
      a += 5;
    }
    console.log(a);
    this.setAmmount(a);
    (document.getElementById('productammount-' + this.product.product_details.id) as HTMLInputElement).value = a.toString();
  }

  ammountChanged(e) {
    console.log(e);
  }

  minusAmmountBtn() {
    var ammount = parseInt((document.getElementById('productammount-' + this.product.product_details.id) as HTMLInputElement).value);
    console.log(ammount);
    if (ammount == 0) {
      return;
    }
    var a = ammount;
    if (this.product.details.ammount_unit == "pcs") {
      a -= 1;
    } else if (this.product.details.ammount_unit == "gr") {
      a -= 5;
    }
    if (ammount < 0) {
      a = 0;
    }
    this.setAmmount(a);
    (document.getElementById('productammount-' + this.product.product_details.id) as HTMLInputElement).value = a.toString();
  }

  public async setAmmount(ammount: number) {
    try {
      var updated = await this.shoppingListService
        .setAmmountToBuyOfProduct(this.product.product_details.id.toString(), ammount)
        .toPromise();

      if (!updated) {
        console.log("ERROR - setAmmount - updated is null");
        return;
      }
    } catch (e) {
      console.error(e);
    }
  }

  deleteProduct() {
    var product_id: string = this.product.product_details.id.toString();
    this.deleteProductWithId(product_id);
    this.shoppingListDeleteService.updateStatus(true);
    this.shoppingListDeleteService.deletionDetected.emit(true);
  }

  public async deleteProductWithId(id: string) {
    try {
      var toDelete: any = await this.shoppingListService
        .removeProductFromShoppingList(id)
        .toPromise();

      if (!toDelete) {
        console.log("ERROR - deleteProductWithId - toDelete is null");
        return;
      }

      if (!toDelete.success) {
        console.log(toDelete.message.toString());
        return;
      }

      console.log("Deleted Product with id " + toDelete.productId);

      var n: SIMNotification = {
        id: "",
        index: 0,
        severity: "info",
        message: "Deleted Product with id " + toDelete.productId,
        source: "shopping-list",
        isRead: false
      };

      var push: PushNotification = {
        message: "Deleted Product with id " + toDelete.productId,
        source: "Shopping List"
      };
      this.notificationService.createNotification(n);
      this.push.generateNotification(push, false);

    } catch (e) {
      console.error(e);
    }
  }

  showDetails() {
    this.router.navigate(['product_details', this.product.product_details.id]);
  }
}
