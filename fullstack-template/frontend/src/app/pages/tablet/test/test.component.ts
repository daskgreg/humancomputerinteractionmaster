import { Component, OnInit, ComponentFactoryResolver, ViewChild, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { PushNotificationService } from 'src/app/global/services/push-notification/push-notification.service';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { NotificationsComponent } from '../notifications/notifications.component';
import { PoorMansPushNotificationComponent } from '../../poor-mans-push-notification/poor-mans-push-notification.component';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { SocketsService, CustomService } from 'src/app/global/services';
import { FridgeOpenedListener } from 'src/app/global/services/frontend-services/fridge-opened-listener/fridge-opened-listener.service';


@Component({
  selector: 'ami-fullstack-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class TestComponent implements OnInit {
  public showProductDetails = false;
  public productPosition = false;
  public hideIcon = true;
  public availabilityPercentage = false;

  notifications: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers', 'Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers', 'Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers', 'Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];

  public search: any = '';
  locked: any = [];

  constructor(private product: ProductsService,
    private socketService: SocketsService,
    private fridgeListener: FridgeOpenedListener,
    private notificationObj: PoorMansPushNotificationComponent,
    private custom: CustomService
  ) {


  }

  @ViewChild('container', { read: ViewContainerRef, static: false }) container: ViewContainerRef;
  private _counter = 1;




  async ngOnInit() {
    // this.initLocked();


  }

  sendSocketReq() {
    var data = {
      source: "Test",
      info: "Test Info"
    }
    this.custom.sendVoiceAssistantNotification(data);
  }

  public async steileToGamwRequest(data) {
    try {
      var event = (document.getElementById('socketevent') as HTMLInputElement).value;

      var s = await this.socketService.sendSocketRequest(event, data).toPromise();
    } catch (e) {
      console.error(e);
    }
  }

  public async initLocked() {
    try {
      // this.locked  
      var d: any = await this.product
        .getAllProductNames()
        .toPromise();

      console.log(d.prod);
      for (var i = 0; i < d.prod.length; i++) {
        var t = { "ID": i, "Prod": d.prod[i].product_details.name.toString(), "Prod_ID": d.prod[i].product_details.id.toString() }
        this.locked[i] = t;
      };

      console.log(this.locked);
      console.log(d);
    } catch (e) {
      console.error(e);
    }
  }

  clickedProd(id) {
    console.log(this.locked[id]);
  }

  quantity: number = 1;
  i = 1
  plus() {
    if (this.i != 10) {
      this.i++;
      this.quantity = this.i;
    }
  }
  minus() {
    if (this.i != 1) {
      this.i--;
      this.quantity = this.i;
    }
  }

  detailsMenu() {
    this.showProductDetails = !this.showProductDetails;
  }
  fridgePosition() {
    this.productPosition = !this.productPosition;
    this.showProductDetails = false;
  }

  hideNseek() {
    this.hideIcon = false;
    this.availabilityPercentage = true;
  }
  hideNseek2() {
    this.hideIcon = true;
    this.availabilityPercentage = false;
  }



}