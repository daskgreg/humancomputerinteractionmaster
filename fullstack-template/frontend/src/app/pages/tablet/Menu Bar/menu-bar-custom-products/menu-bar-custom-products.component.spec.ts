import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuBarCustomProductsComponent } from './menu-bar-custom-products.component';

describe('MenuBarCustomProductsComponent', () => {
  let component: MenuBarCustomProductsComponent;
  let fixture: ComponentFixture<MenuBarCustomProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuBarCustomProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuBarCustomProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
