import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuBarRecurringComponent } from './menu-bar-recurring.component';

describe('MenuBarRecurringComponent', () => {
  let component: MenuBarRecurringComponent;
  let fixture: ComponentFixture<MenuBarRecurringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuBarRecurringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuBarRecurringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
