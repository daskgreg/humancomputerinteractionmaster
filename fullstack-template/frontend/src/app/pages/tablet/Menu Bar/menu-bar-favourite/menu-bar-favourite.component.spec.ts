import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuBarFavouriteComponent } from './menu-bar-favourite.component';

describe('MenuBarFavouriteComponent', () => {
  let component: MenuBarFavouriteComponent;
  let fixture: ComponentFixture<MenuBarFavouriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuBarFavouriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuBarFavouriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
