import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { CustomService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-menu-bar-goshopping',
  templateUrl: './menu-bar-goshopping.component.html',
  styleUrls: ['./menu-bar-goshopping.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuBarGoshoppingComponent implements OnInit {

  constructor(private router: Router, private custom: CustomService) { }

  ngOnInit() {
  }
  redirect(target: string) {
    this.custom.redirect(target);
  }
  setPageLocation() {
    var fullURl = this.router.url.toString();
    fullURl = fullURl.replace("-", " ");
    fullURl = fullURl.replace("/", "");
    var length = fullURl.length;
    for (var i = 0; i < length; i++) {
      if (i == 0) {
        var capital = fullURl[i].toUpperCase();
        fullURl = capital + fullURl.substr(1, fullURl.length)
      }
      if (fullURl[i] == "-") {
        fullURl = fullURl.replace("-", " ");
      }
      if (fullURl[i] == " ") {
        var capital = " " + fullURl[i + 1].toUpperCase();
        fullURl = fullURl.substr(0, i) + capital + fullURl.substr(i + 2, fullURl.length)
      }
    }
    (document.getElementById('top-side-bar-where-am-i') as HTMLDivElement).innerText = fullURl;
  }
  ShowMenuSideBarWhileAfilterIsOn() {
    var LeftMenuBar = document.getElementById("sp-list-left-sidebarCat");
    var LeftMenuBarMiniCat = document.getElementById("sp-list-left-sidebar-minimizedCat");
    var TopSideBoard = document.getElementById("topside-user-board");
    var TopsideBoardWhileMenuOn = document.getElementById("topside-user-board2");
    var CheeseCategory = document.getElementById("CheeseCategory");
    var CheeseCategoryWhileMenuOn = document.getElementById("CheeseCategoryWhileMenuOn");
    var HideFilterMenu = document.getElementById("sp-list-filter-sidebar");
    var HideFilter = document.getElementById("sp-list-filter-sidebar-minimized");
    var HideFilterMenu2 = document.getElementById("sp-list-filter-sidebar-while-product-is-selected");
    var HideFilter2 = document.getElementById("sp-list-filter-sidebar-minimized-while-product-is-selected");
    if (LeftMenuBarMiniCat.style.display === "none") {
      LeftMenuBarMiniCat.style.display = "block";
    } else {
      LeftMenuBar.style.display = "block";
      TopSideBoard.style.display = "none";
      TopsideBoardWhileMenuOn.style.display = "block";
      CheeseCategory.style.display = "none";
      CheeseCategoryWhileMenuOn.style.display = "block";
      HideFilter.style.display = "none";
      HideFilterMenu.style.display = "none";
      HideFilter2.style.display = "block";
      HideFilterMenu2.style.display = "none";

    }
  }
  leftMenuSideBarShow() {
    var x = document.getElementById("sp-list-left-sidebar-minimized");
    var y = document.getElementById("sp-list-left-sidebar");
    var z = document.getElementById("topside-user-board2");
    var q = document.getElementById("topside-user-board");
    var sp_list = document.getElementById("shopping-list-page");
    var sp_list_mini = document.getElementById("shopping-list-page-minimized");
    var sp_list_filter = document.getElementById("sp-list-filter-sidebar");
    var sp_list_filter_mini = document.getElementById("sp-list-filter-sidebar-minimized");
    if (x.style.display === "none") {
      x.style.display = "block";
      sp_list_filter.style.display = "block";

    } else {
      x.style.display = "none";
      y.style.display = "block";
      z.style.display = "block";
      q.style.display = "none";
      sp_list.style.display = "none";
      sp_list_mini.style.display = "block";
      sp_list_filter.style.display = "none";
      sp_list_filter_mini.style.display = "block";
    }

  }

  leftMenuSideBarHide() {
    var x = document.getElementById("sp-list-left-sidebar-minimized");
    var y = document.getElementById("sp-list-left-sidebar");
    var z = document.getElementById("topside-user-board2");
    var q = document.getElementById("topside-user-board");
    var sp_list = document.getElementById("shopping-list-page");
    var sp_list_mini = document.getElementById("shopping-list-page-minimized");

    if (x.style.display === "block") {
      x.style.display = "none";

    } else {
      x.style.display = "block";
      y.style.display = "none";
      z.style.display = "none";
      q.style.display = "block";
      sp_list.style.display = "block";
      sp_list_mini.style.display = "none";
    }
  }

  LeftMenuSideBarHideCat() {
    var LeftSideBarMini = document.getElementById("sp-list-left-sidebar-minimizedCat");
    var LeftSideBar = document.getElementById("sp-list-left-sidebarCat");
    var TopSideBar = document.getElementById("topside-user-board");
    var TopSideBar2 = document.getElementById("topside-user-board2");
    var CheeseCategoryWhileMenuOn = document.getElementById("CheeseCategoryWhileMenuOn");
    var CheeseCategory = document.getElementById("CheeseCategory");
    var HideFilter = document.getElementById("sp-list-filter-sidebar-minimized");

    if (LeftSideBarMini.style.display === "block") {
      LeftSideBarMini.style.display = "none";
    } else {
      LeftSideBarMini.style.display = "block";
      LeftSideBar.style.display = "none";
      TopSideBar.style.display = "block";
      TopSideBar2.style.display = "none";
      CheeseCategoryWhileMenuOn.style.display = "none";
      CheeseCategory.style.display = "block";
      HideFilter.style.display = "block"
    }

  }
}
