import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'ami-fullstack-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements AfterViewInit {
  isLoading: boolean;
  constructor() { 
    this.isLoading = true;
  }
  ngAfterViewInit(): void {
    this.isLoading = false;
  }

}
