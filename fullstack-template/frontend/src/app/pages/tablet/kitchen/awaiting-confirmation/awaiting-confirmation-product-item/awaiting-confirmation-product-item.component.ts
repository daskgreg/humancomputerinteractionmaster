import { Component, OnInit, Input } from '@angular/core';
import { ShoppingListComponent } from '../../../shopping-list/shopping-list.component';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { RunningOutProductsService } from 'src/app/global/services/products/running-out-products/running-out-products.service';
import { SocketsService } from 'src/app/global/services';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'ami-fullstack-awaiting-confirmation-product-item',
  templateUrl: './awaiting-confirmation-product-item.component.html',
  styleUrls: ['./awaiting-confirmation-product-item.component.scss']
})
export class AwaitingConfirmationProductItemComponent implements OnInit {
  @Input() product;

  constructor(
    private shoppingListService: ShoppingListProductService,
    private awaitingService: RunningOutProductsService,
    private socketsService: SocketsService
  ) { }

  ngOnInit() {
  }

  async accept() {
    try {
      var prod = {
        product_details: {
          id: this.product.product_details.id,
          name: this.product.product_details.name
        },
        details: {
          ammount_to_buy: 0,
          category: this.product.details.category,
          ammount_unit: this.product.details.ammount_unit,
          tags: this.product.details.tags
        },
        image: {
          image_type: this.product.image.image_type,
          image_data: this.product.image.image_data
        }
      }

      await this.shoppingListService
        .addProductToShoppingList(prod)
        .toPromise();

      this.removeFromAwaiting();
    } catch (e) {
      console.error(e);
    }
  }

  reject() {
    this.removeFromAwaiting();
  }



  async removeFromAwaiting() {
    try {
      await this.awaitingService
        .removeProductFromRunningOutList(this.product.product_details.id)
        .toPromise();

      var event = "removed-from-awaiting"
      await this.socketsService
        .sendSocketRequest(event, null)
        .toPromise();
    } catch (e) {
      console.error(e);
    }
  }

}
