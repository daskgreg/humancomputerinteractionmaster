import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { CustomService, SocketsService } from 'src/app/global/services';
import { Router } from '@angular/router';
import { Product } from 'src/app/global/models/products/product.model';
import { RunningOutProductsService } from 'src/app/global/services/products/running-out-products/running-out-products.service';

@Component({
  selector: 'ami-fullstack-awaiting-confirmation',
  templateUrl: './awaiting-confirmation.component.html',
  styleUrls: ['./awaiting-confirmation.component.scss']
})
export class AwaitingConfirmationComponent implements OnInit {
  public haveProducts = false;
  public productsArray$ = [];

  constructor(private awaitingService: RunningOutProductsService,
    private socketsService: SocketsService) {
    this.loadProducts()
  }

  ngOnInit() {
    this.socketsService.syncMessages("removed-from-awaiting").subscribe(data => {
      this.loadProducts();
    })
  }

  public async loadProducts() {
    var i = 0;
    try {
      var products: any = await this.awaitingService
        .getAllRunningOutProducts()
        .toPromise();


      if (!products) {
        console.log("No products found!");
        return;
      }

      this.haveProducts = true;

      console.log(products);//No fucking clue
      // while (products[0][i]) {
      //   this.productsArray$.push(products[0][i]);
      //   i++;
      // }

      this.productsArray$ = products.data

      // console.log(products);

    } catch (e) {
      console.error(e);
    }
  }
}
