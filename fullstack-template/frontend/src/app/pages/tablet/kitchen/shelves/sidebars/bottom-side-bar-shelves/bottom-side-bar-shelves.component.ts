import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-bottom-side-bar-shelves',
  templateUrl: './bottom-side-bar-shelves.component.html',
  styleUrls: ['./bottom-side-bar-shelves.component.scss']
})
export class BottomSideBarShelvesComponent implements OnInit {
  public showMenu = false;
  public showFilter = false;
  public LeftBottomBarTxtIcon = true;
  public addProduct = false;

  constructor(private socketService: SocketsService,
    private router: Router,
    private productService: ProductsService) { }

  ngOnInit() {
  }

  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }

  menuController() {
    this.showMenu = !this.showMenu;
    this.showFilter = false;
  }

  filterController() {
    this.showFilter = !this.showFilter;
    this.showMenu = false;
  }
  productController() {
    this.showMenu = false;
    this.showFilter = false;
    this.addProduct = !this.addProduct;
  }

  async addProductCustom() {
    var name = (document.getElementById('searchBar') as HTMLInputElement).value;
    var ammount = (document.getElementById('product_to_add_to_splist_ammount') as HTMLInputElement).value;
    try {
      var data = {
        name: name,
        ammount: ammount,
        category: "",
        exp_date: "",
        storage: ""
      };

      var s = await this.productService
        .addCustomProduct(data)
        .toPromise();
      if (!s) {
        console.log("error add product custom");
        return;
      }
      this.addProduct = false;
      console.log("false addproduct");
      var event = "add-custom-product";
      this.socketService.sendSocketRequest(event, null)
        .toPromise();
    } catch (e) {
      console.error(e);
    }
  }


}