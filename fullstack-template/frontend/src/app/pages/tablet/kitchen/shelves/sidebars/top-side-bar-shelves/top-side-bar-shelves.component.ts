import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-top-side-bar-shelves',
  templateUrl: './top-side-bar-shelves.component.html',
  styleUrls: ['./top-side-bar-shelves.component.scss']
})
export class TopSideBarShelvesComponent implements OnInit {
  today: number = Date.now();

  constructor(private router: Router) {
    setInterval(() => {this.today = Date.now()}, 1);
  }
  
  ngOnInit() {
  }

  redirect(target:String){
    console.log(target);
    this.router.navigate([target]);
  }
}
