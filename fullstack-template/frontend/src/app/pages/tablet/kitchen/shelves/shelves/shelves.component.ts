import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router'
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { CustomService, SocketsService } from 'src/app/global/services';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { Product } from 'src/app/global/models/products/product.model';
import { ShoppingListProduct } from 'src/app/global/models/products/shopping-list-product.model';

@Component({
  selector: 'ami-fullstack-shelves',
  templateUrl: './shelves.component.html',
  styleUrls: ['./shelves.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShelvesComponent implements OnInit {
  public haveProducts = false;
  public whereToSearch = this.product;
  public callerComponent = this;
  public timeoutHandler: any;

  public productsArray$ = [];

  constructor(private socketService: SocketsService, private product: ProductsService,
    private custom: CustomService,
    private shoppingList: ShoppingListProductService) {
    this.socketService.syncMessages("add-custom-product").subscribe((data) => {
      console.log("skasaskas");
      this.haveProducts = false;
      this.productsArray$ = [];
      var route = this;
      setTimeout(() => {
        route.loadProducts();
      }, 1);

    })
    this.loadProducts();
  }

  ngOnInit() {

  }

  public async loadProducts() {
    // console.log(this.callerComponent);
    var i = 0;
    try {
      var products: Product[] = await this.product
        .getProductByPosition('shelves')
        .toPromise();


      if (!products) {
        console.log("No products found!");
        return;
      }

      this.haveProducts = true;



      // console.log(products[0][0]);//No fucking clue
      while (products[0][i]) {
        if (products[0][i].details.ammount_remaining != 0) {
          this.productsArray$.push(products[0][i]);
        } else if (products[0][i].is_custom) {
          this.productsArray$.push(products[0][i]);
        }
        i++;
      }
      console.log(this.productsArray$);
    } catch (e) {
      console.error(e);
    }
  }

  public async addToSPList(id) {
    console.log(id);
    try {
      var prod: ProductsService;

      var findProduct = await this.product
        .getProductById(id)
        .toPromise();

      if (!findProduct) {
        console.log("Error - addProdToShoppingList - findProduct")
        return;
      }

      var p2i: ShoppingListProduct = {
        product_details: {
          id: "",
          name: ""
        },
        details: {
          ammount_to_buy: 0,
          category: "",
          ammount_unit: "",
          tags: [""]
        },
        image: {
          image_data: "",
          image_type: ""
        }
      }

      var prod2add = await this.shoppingList
        .addProductToShoppingList(p2i)
        .toPromise();

      if (!prod2add) {
        console.log("Error - addProdToShoppingList - findProduct")
        return;
      }


    } catch (e) {
      console.error(e);
    }
  }

}