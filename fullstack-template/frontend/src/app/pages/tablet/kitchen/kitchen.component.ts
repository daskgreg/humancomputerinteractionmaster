import { Component, OnInit } from '@angular/core';
import { CustomService } from 'src/app/global/services';
import { Product } from 'src/app/global/models/products/product.model';
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'ami-fullstack-kitchen',
  templateUrl: './kitchen.component.html',
  styleUrls: ['./kitchen.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class KitchenComponent implements OnInit {

  constructor(private router: Router,
    private custom: CustomService,
    private product: ProductsService) { }

  ngOnInit() {
  }
  FridgeCapacity() {
    var CapacityProduct0 = document.getElementById("product-capacity0");
    var CapacityProduct1 = document.getElementById("product-capacity1");
    var CapacityProduct2 = document.getElementById("product-capacity2");
    var CapacityProduct3 = document.getElementById("product-capacity3");
    var CapacityProduct4 = document.getElementById("product-capacity4");
    var CapacityProduct = document.getElementById("kitchen-products-capacity");
    var KitchenProducts = document.getElementById("kitchen-products");

    // openProducts(data){
    //   if(data == "fridge"){

    //     this.custom.redirect('fridge');
    //   }else if(data == "cabbinet"){
    //     this.custom.redirect('cabbinet');
    //   }
    // }

    // public async loadProducts(){
    //   try{
    //     var products : Product[] = await this.product
    //     .getProductByPosition('fridge')
    //     .toPromise();



    //     console.log(products);

    //   }catch (e){
    //     console.error(e);
    //   }
    // }


  }
  FridgePageIsOnNow() {
    var KitchenDashboard = document.getElementById("kitchen-dashboard");
    var FridgeDashboard = document.getElementById("fridge-dashboard");
    var TopSideBar = document.getElementById("topside-user-board");
    var TopSideBar2 = document.getElementById("topside-user-board2");
    var MenuBar = document.getElementById("sp-list-left-sidebar");
    if (KitchenDashboard.style.display === "none") {
      KitchenDashboard.style.display = "block";
    } else {
      KitchenDashboard.style.display = "none";
      FridgeDashboard.style.display = "block";
      TopSideBar.style.display = "block";
    }
  }

  redirect(location) {
    this.custom.redirect(location);
  }
}
