import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'ami-fullstack-bottom-side-bar-cabbinet',
  templateUrl: './bottom-side-bar-cabbinet.component.html',
  styleUrls: ['./bottom-side-bar-cabbinet.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BottomSideBarCabbinetComponent implements OnInit {
  public showMenu = false;
  public showFilter = false;
  public LeftBottomBarTxtIcon = true;
  public addProduct = false;
    constructor(private router: Router) { }
  
    ngOnInit() {
    }
  
    redirect(target:String){
      console.log(target);
      this.router.navigate([target]);
    }
  
    menuController(){
      this.showMenu = !this.showMenu;
      this.showFilter = false;
    }
  
    filterController(){
      this.showFilter = !this.showFilter;
      this.showMenu = false;
    }
    productController(){
      this.showMenu = false;
      this.showFilter = false;
      this.addProduct = !this.addProduct;
    }
}
