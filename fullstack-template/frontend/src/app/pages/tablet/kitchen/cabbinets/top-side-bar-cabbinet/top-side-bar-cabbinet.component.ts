import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'ami-fullstack-top-side-bar-cabbinet',
  templateUrl: './top-side-bar-cabbinet.component.html',
  styleUrls: ['./top-side-bar-cabbinet.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopSideBarCabbinetComponent implements OnInit {
  today: number = Date.now();

  constructor(private router: Router) {
    setInterval(() => {this.today = Date.now()}, 1);
  }
  
  ngOnInit() {
  }

  redirect(target:String){
    console.log(target);
    this.router.navigate([target]);
  }
  //shit
  
}
