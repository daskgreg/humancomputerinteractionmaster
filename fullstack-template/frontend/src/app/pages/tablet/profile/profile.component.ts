import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {
  public showEditProfile = false;
  public profilePage = true;
  constructor(private router : Router) { }

  ngOnInit() {
  }
  
  editProfile(){
    this.showEditProfile = !this.showEditProfile;
    this.profilePage = false;
  }

}
