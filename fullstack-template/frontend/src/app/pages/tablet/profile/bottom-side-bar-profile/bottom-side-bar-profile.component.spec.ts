import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomSideBarProfileComponent } from './bottom-side-bar-profile.component';

describe('BottomSideBarProfileComponent', () => {
  let component: BottomSideBarProfileComponent;
  let fixture: ComponentFixture<BottomSideBarProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomSideBarProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomSideBarProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
