import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ami-fullstack-bottom-side-bar-profile',
  templateUrl: './bottom-side-bar-profile.component.html',
  styleUrls: ['./bottom-side-bar-profile.component.scss']
})
export class BottomSideBarProfileComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
