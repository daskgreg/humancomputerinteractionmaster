import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/global/models/products/product.model';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { PoorMansPushNotificationComponent } from 'src/app/pages/poor-mans-push-notification/poor-mans-push-notification.component';
import { ShoppingListDeleteProducts } from 'src/app/global/services/frontend-services/shopping-list-delete-product.ts/shopping-list-delete-products';
import { CustomService } from 'src/app/global/services';
import { ActivatedRoute, Router } from '@angular/router';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ami-fullstack-super-market-product-item',
  templateUrl: './super-market-product-item.component.html',
  styleUrls: ['./super-market-product-item.component.scss']
})
export class SuperMarketProductItemComponent implements OnInit {

  @Input() index: number;
  @Input() product: Product;
  // @Input() id: string;

  public prodImg;

  constructor(
    private shoppingListService: ShoppingListProductService,
    private notificationService: NotificationsService,
    private push: PoorMansPushNotificationComponent,
    private shoppingListDeleteService: ShoppingListDeleteProducts,
    private custom: CustomService,
    private route: ActivatedRoute,
    private router: Router,
    private dom: DomSanitizer
  ) {
  }

  ngOnInit() {
    this.imgUrl();
  }


  deleteProduct() {
    var product_id: string = this.product.product_details.id.toString();
    this.deleteProductWithId(product_id);
    this.shoppingListDeleteService.updateStatus(true);
    this.shoppingListDeleteService.deletionDetected.emit(true);
  }

  public async deleteProductWithId(id: string) {
    try {
      var toDelete: any = await this.shoppingListService
        .removeProductFromShoppingList(id)
        .toPromise();

      if (!toDelete) {
        console.log("ERROR - deleteProductWithId - toDelete is null");
        return;
      }

      if (!toDelete.success) {
        console.log(toDelete.message.toString());
        return;
      }

      console.log("Deleted Product with id " + toDelete.productId);

      var n: SIMNotification = {
        id: "",
        index: 0,
        severity: "info",
        message: "Deleted Product with id " + toDelete.productId,
        source: "shopping-list",
        isRead: false
      };

      var push: PushNotification = {
        message: "Deleted Product with id " + toDelete.productId,
        source: "Shopping List"
      };
      this.notificationService.createNotification(n);
      this.push.generateNotification(push, false);

    } catch (e) {
      console.error(e);
    }
  }

  showDetails() {
    this.router.navigate(['product_details', this.product.product_details.id]);
  }

  imgUrl() {
    console.log(this.product);
    var tmp: any = this.product.image.image_data;
    var imgSrc = tmp.data;
    var imgD = this.custom._arrayBufferToBase64(imgSrc);
    var imgType = this.product.image.image_type;
    var t = "data: " + imgType + ";base64, " + imgD;
    this.prodImg = this.dom.bypassSecurityTrustUrl(t);
  }
}
