import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-super-market',
  templateUrl: './super-market.component.html',
  styleUrls: ['./super-market.component.scss']
})
export class SuperMarketComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }
}
