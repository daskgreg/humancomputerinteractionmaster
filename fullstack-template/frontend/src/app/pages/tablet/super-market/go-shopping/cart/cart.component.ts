import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from 'src/app/global/services/products/shopping/cart.service';

@Component({
  selector: 'ami-fullstack-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  public show: boolean = false;
  public cartProducts$ = [];

  constructor(
    private cartService: ShoppingCartService
  ) { }

  ngOnInit() {
    this.getAll();
  }

  async getAll() {
    try {
      var d: any = await this.cartService
        .getProducts()
        .toPromise();

      this.show = true;
      this.cartProducts$ = d.data;

    } catch (e) {
      console.error(e);
    }
  }

}
