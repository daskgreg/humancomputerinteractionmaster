import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-bottom-side-bar-goshopping',
  templateUrl: './bottom-side-bar-goshopping.component.html',
  styleUrls: ['./bottom-side-bar-goshopping.component.scss']
})
export class BottomSideBarGoshoppingComponent implements OnInit {
  public showMenu = false;
  public showFilter = false;
  public LeftBottomBarTxtIcon = true;
  public addProduct = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }

  menuController() {
    this.showMenu = !this.showMenu;
    this.showFilter = false;
  }

  filterController() {
    this.showFilter = !this.showFilter;
    this.showMenu = false;
  }
  productController() {
    this.showMenu = false;
    this.showFilter = false;
    this.addProduct = !this.addProduct;
  }

  showAddedToCart() {
    this.redirect('in-cart');
  }
}
