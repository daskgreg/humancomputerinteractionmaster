import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'ami-fullstack-top-side-bar-shopping-list',
  templateUrl: './top-side-bar-shopping-list.component.html',
  styleUrls: ['./top-side-bar-shopping-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopSideBarShoppingListComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  redirect(target:String){
    console.log(target);
    this.router.navigate([target]);
  }

}
