import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { CustomService } from 'src/app/global/services';
import { Router } from '@angular/router';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
// import {DashboardComponent} from './dashboard.component'


@Component({
  selector: 'ami-fullstack-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  showNotifications = false;
  constructor(private custom: CustomService,
    private router: Router,
    private notifications: NotificationsService) {

    if ((document.getElementById('db-prof-pic') as HTMLInputElement)) {
      console.log("exw comp");
      var user = JSON.parse(localStorage.getItem('userData'));
      var userImgSrc = URL.createObjectURL(custom.convertDataUrlToBlob(user.image_data));
      (document.getElementById('db-prof-pic') as HTMLInputElement).src = userImgSrc;
      (document.getElementById('db-nb-pfl-photo-txt') as HTMLInputElement).src = user.first_name;

    }
  }

  ngOnInit(): void {
    this.setNotificationsNumber();
  }

  public async setNotificationsNumber() {
    var badge = (document.getElementById('notificationsNumberBadge') as HTMLSpanElement);
    try {
      var notifNum: any = await this.notifications
        .getUnreadNotifications()
        .toPromise();

      if (!notifNum) {
        console.log("ERROR - setNotificationsNumber - notifNum is null");
        return;
      }
      var unread = parseInt(notifNum.unreadNotifications);
      var badgeText = "";
      if (unread > 10) {
        badge.style.visibility = "block";
        badgeText = "9+";
      } else if (unread == 0) {
        badge.style.visibility = "none";
      } else {
        badge.style.visibility = "block";
        badgeText = unread.toString();
      }
      badge.innerText = badgeText;
    } catch (e) {
      console.error(e);
    }
  }

  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }

  // showNotifs(){
  //   this.showNotifications = !this.showNotifications;
  // }
}
