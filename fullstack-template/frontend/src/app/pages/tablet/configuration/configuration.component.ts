import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { QrScannerComponent } from 'angular2-qrscanner';
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { ReceiptProductsService } from 'src/app/global/services/products/receipt-products/receipt-products.service';
import { Product } from 'src/app/global/models/products/product.model';
import * as Globals from 'src/app/global/variables/variables';
import { Classification } from 'src/app/global/models/classification/classification.model';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { ShoppingListProduct } from 'src/app/global/models/products/shopping-list-product.model';
import { UserSessionService } from 'src/app/global/services/session/user-session.service';
import { CustomService, SocketsService } from 'src/app/global/services';
import { RunningOutProductsService } from 'src/app/global/services/products/running-out-products/running-out-products.service';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { SuperMarketSingletonService } from 'src/app/global/services/frontend-services/super-market-singleton/super-market-singleton.service';
import { ContainersService } from 'src/app/global/services/containers/containers-service.service';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { VoiceAssistantLogsComponent } from '../../global-components/voice-assistant-logs/voice-assistant-logs.component';
import { identifierModuleUrl } from '@angular/compiler';
import { PoorMansPushNotificationComponent } from '../../poor-mans-push-notification/poor-mans-push-notification.component';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { SIMLog } from 'src/app/global/models/logger/logs.model';

// require('ngx-scanner');
// require('angular2-qrscanner');

@Component({
  selector: 'ami-fullstack-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit, OnDestroy {
  showUserBar: boolean;
  public scanQR: Boolean = false;
  private global_data;
  private classific;
  public superMarketData;

  public localStrClassify = {
    status: false,
    receipt_id: ""
  };

  constructor(private http: HttpClient,
    private router: Router,
    private product: ProductsService,
    private receipt: ReceiptProductsService,
    private shoppingListProduct: ShoppingListProductService,
    private custom: CustomService,
    private runningOut: RunningOutProductsService,
    private notification: NotificationsService,
    private containerService: ContainersService,
    private superMarketSingleton: SuperMarketSingletonService,
    private socketsService: SocketsService,
    private push: PoorMansPushNotificationComponent,
  ) {

    // this.classific = this.classification.currentMessage.subscribe(data => this.global_data = data);
    // console.log(this.global_data);

  }

  requestQR() {
    var postData = new HttpParams();
    postData = postData.set('qrdata', (document.getElementById('qrdata') as HTMLInputElement).value.toString());

    this.http.post('http://127.0.0.1:8080/api/configuration/generateQR', postData).subscribe(
      responseData => this.handleResponseQR(responseData),
      err => this.handleError(err)
    );
  }

  ngOnDestroy() {

  }

  async userCameHome() {
    try {
      var event = "fridge-scan-products";
      var data = {};
      var request = await this.socketsService
        .sendSocketRequest(event, data)
        .toPromise();

      if (!request) {
        console.log("Request to logger failed");
        return;
      }
    } catch (e) {
      console.error(e);
    }
  }

  postSupermarketReceipt() {
    var postData = new HttpParams();
    postData = postData.set("receipt_id", (document.getElementById('receipt_id') as HTMLInputElement).value.toString());



    this.http.post('http://127.0.0.1:8080/api/virtual/fetchReceipt', postData).subscribe(
      res => { console.log("Success:") },
      err => { console.log(err) });
  }

  postSupermarketReceiptQRScan(receipt_id: any) {
    var postData = new HttpParams();
    postData = postData.set("receipt_id", receipt_id);

    this.http.post('http://127.0.0.1:8080/api/virtual/fetchReceipt', postData).subscribe(
      res => { console.log("Success:") },
      err => { console.log(err) });
  }

  handleResponseQR(data) {
    console.log(data);
    var i = document.createElement('img');
    i.id = "i";
    i.src = data.message;

    if ((document.getElementById('i') as HTMLInputElement)) {
      (document.getElementById('i') as HTMLInputElement).src = data.message;
    } else {
      document.getElementsByTagName('body')[0].appendChild(i);
    }

  }

  handleError(err) {
    console.log(err);
  }

  ngOnInit() {
    this.socketsService.syncMessages("scanned-receipt").subscribe((data) => {
      this.localStrClassify = data.message;
    });

    this.socketsService.syncMessages("user-location").subscribe((data) => {
      console.log(data.message.location);
    });

    this.custom.setTitle("SIM | Configuration");

    // Global btn listener htat returns every button's name
    // if (document.addEventListener) {
    //   document.addEventListener("click", this.handleClick, false);
    // }
  }

  handleClick(event) {
    console.log(event.originalTarget.firstChild.data);
  }

  @ViewChild(QrScannerComponent, null) qrScannerComponent: QrScannerComponent;
  enableScan() {

    this.scanQR = true;

    this.qrScannerComponent.getMediaDevices().then(devices => {
      console.log(devices);
      const videoDevices: MediaDeviceInfo[] = [];
      for (const device of devices) {
        if (device.kind.toString() === 'videoinput') {
          videoDevices.push(device);
        }
      }
      if (videoDevices.length > 0) {
        let choosenDev;
        for (const dev of videoDevices) {
          if (dev.label.includes('front')) {
            choosenDev = dev;
            break;
          }
        }
        if (choosenDev) {
          this.qrScannerComponent.chooseCamera.next(choosenDev);
        } else {
          this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
        }
      }
    });

    this.qrScannerComponent.capturedQr.subscribe(result => {
      //QR Scanned

      console.log(result);
      this.postSupermarketReceiptQRScan(result);
      this.scanQR = false;
    });
  }
  //if classification is active
  //check wether the refileed product is part of the 
  //bought products nad scratch it from the list
  //else notify user that X product was refilled.
  //did he buy X product and refilled it, or did he mess up
  /**
   * ->Get containerr ID from input
   * ->Use API "getProductsByContainerId" to find out what product is stored in the container
   * ->Use API "isPartOfReceiptWithId" to check wether the product found above is part of the 
   *   latest receipt
   * ->Use API "refillProduct" to reset the container's current ammount to full
   */
  //ClassifictionSingleton
  public async refillContainer() {
    var container_id = (document.getElementById('refill_container_id') as HTMLInputElement)
      .value.toString();



    if (this.localStrClassify.status) {
      //get the default contained product of the container id
      try {
        var prod: any = await this.product
          .getProductByContainerId(container_id)
          .toPromise();
        if (!prod) {
          console.log("No container with id " + container_id + " found!");
          return;
        }

        //check if the refilled product is part of the bought products
        console.log(prod);

        var product2check = prod.products.product_details.id.toString();
        var receipt2check = this.localStrClassify.receipt_id;

        var p: any = await this.receipt
          .isPartOfReceiptWithId(product2check, receipt2check)
          .toPromise();

        if (!p) {
          console.log("No product with id " + product2check + " found in receipt " + receipt2check);
          return;
        }

        if (p.product_found) {
          //increase ammount to full
          var refill_prod = this.product
            .refillProduct(prod.products.product_details.id.toString(), 'refill')
            .toPromise();

          if (!refill_prod) {
            console.log("No products to refill found! if(@p.product_found)")
            return;
          }



          // alert("Product Reffiled. TODO na kanei scratch off to proion apo to super market list");
          //if so scratch it out from the bought prods list
          var data = {
            product_id: product2check
          }

          await this.socketsService
            .sendSocketRequest('remove-receipt-product', data)
            .toPromise();



          console.log("The product with id: " + prod.products.product_details.id.toString() +
            " and name: " + prod.products.product_details.name + " is part of the receipt with id: "
            + receipt2check + " and it was refilled. Scratching from Super Market products list");

          var log: SIMLog = {
            severity: "INFO",
            message: "Refilled " + prod.products.product_details.name + "("
              + prod.products.product_details.id.toString() + ").",
            source: "Configuration|Refill"
          }
          this.custom.writeToLogger(log);
        }



      } catch (e) {
        console.error(e);
      }
    } else {
      console.log("No active classification!");
    }



    //if so scratch it out from the bought prods list

  }

  public async removeAmmountFromContainer() {
    var ammount = (document.getElementById('remove_x_from_container_id') as HTMLInputElement).value;
    var container = (document.getElementById('remove_from_container_id') as HTMLInputElement).value;

    try {
      var productFromContainer: any = await this.product
        .getProductByContainerId(container)
        .toPromise();

      if (!productFromContainer) {
        console.log("No product that is inside container: " + container + " was found!");
      }
      //Product inside container
      var product2check: any = productFromContainer.products.product_details.id.toString();


      //remove ammount
      var productWithReducedAmmount: any = await this
        .product.removeAmmountFromProduct(product2check, ammount.toString())
        .toPromise();

      if (!productWithReducedAmmount) {
        console.log("No product with id: " + product2check + " was found!");
        return;
      }

      var log: SIMLog = {
        severity: "INFO",
        message: "Removed " + ammount + " from " + container + ".",
        source: "Configuration| Remove Ammount"
      }
      this.custom.writeToLogger(log);


      var checkIfProductLow: any = await this.product
        .checkProductBelowPercentage(product2check, "10")
        .toPromise();

      if (!checkIfProductLow) {
        console.log("No product with id: " + product2check + " was found!");
        return;
      }

      console.log(checkIfProductLow);

      if (checkIfProductLow.isLow) {

        var productToBeInserted: ShoppingListProduct = {
          product_details: {
            id: checkIfProductLow.product.product_details.id,
            name: checkIfProductLow.product.product_details.name
          },
          details: {
            ammount_to_buy: checkIfProductLow.product.details.ammount_to_buy,
            category: checkIfProductLow.product.details.category,
            ammount_unit: checkIfProductLow.product.details.ammountType,
            tags: checkIfProductLow.product.details.tags,//ISWS TROLLIA
          },
          image: {
            image_data: checkIfProductLow.product.image.image_data,
            image_type: checkIfProductLow.product.image.image_type,
          }
        }

        if (checkIfProductLow.isRecurring) {
          console.log("checkIfProductLow.isRecurring");
          console.log(checkIfProductLow.isRecurring);
          //check if product already exists in database
          var prod2insert: any = await this.shoppingListProduct
            .checkIfProductIsInShoppingList(product2check)
            .toPromise();

          if (prod2insert.exists) {
            console.log("Product " + product2check + " already exists in Shopping List");
            return;
          }
          //add to shopping list


          console.log("Data to send to shopping list: " + productToBeInserted);
          var addedToShoppingList: any = await this.shoppingListProduct
            .addProductToShoppingList(productToBeInserted)
            .toPromise();

          if (!addedToShoppingList) {
            console.log("Could not add the product " + product2check + " to the shopping list");
            return;
          }

          console.log("Added " + addedToShoppingList + " to shopping list");

          console.log(productToBeInserted);
          var dataP: PushNotification = {
            source: "Recurring Product",
            message: "Added " + productToBeInserted.product_details.name + " to shopping list"
          }
          this.push.generateNotification(dataP, false);
        } else {
          var checkIfProdIsAlreadyInRunningOutListRespone: any = await this.runningOut
            .checkIfProductIsInRunningOutList(product2check)
            .toPromise();

          if (!checkIfProdIsAlreadyInRunningOutListRespone) {
            console.log("Could not add the product " + product2check + " to the running out list");
            return;
          }

          if (checkIfProdIsAlreadyInRunningOutListRespone.exists) {
            console.log("Product " + product2check + " already exists in Running Out List");
            return;
          }

          var addProductToRunningOutListResponse = await this.runningOut
            .addProductToRunningOutList(productToBeInserted)
            .toPromise();

          if (!addProductToRunningOutListResponse) {
            console.log("Could not add the product " + product2check + " to the awaiting confirmation list");
            return;
          }

          console.log("Added " + product2check + " to awaiting confirmation list");
        }
      }

    } catch (e) {
      console.error(e);
    }
  }

  public async setFav() {
    var prod_id = (document.getElementById('set_fav_prod_id') as HTMLInputElement).value;

    try {
      var favProd: any = await this.product
        .setFavouriteProduct(prod_id)
        .toPromise();

      if (!favProd) {
        console.log("No product with id: " + prod_id + " found!");
        return;
      }

      alert(favProd.product.product_details.name + " was added to the favourite products list!");
    } catch (e) {
      console.error(e);
    }
  }

  public async remFav() {
    var prod_id = (document.getElementById('rem_fav_prod_id') as HTMLInputElement).value;

    try {
      var favProd: any = await this.product
        .removeFavouriteProduct(prod_id)
        .toPromise();

      if (!favProd) {
        console.log("No product with id: " + prod_id + " found!");
        return;
      }

      alert(favProd.product.product_details.name + " was added to the favourite products list!");
    } catch (e) {
      console.error(e);
    }
  }

  public async setRec() {
    var prod_id = (document.getElementById('set_rec_prod_id') as HTMLInputElement).value;

    try {
      var favProd: any = await this.product
        .setRecurringProduct(prod_id)
        .toPromise();

      if (!favProd) {
        console.log("No product with id: " + prod_id + " found!");
        return;
      }

      alert(favProd.product.product_details.name + " was added to the favourite products list!");
    } catch (e) {
      console.error(e);
    }
  }

  public async remRec() {
    var prod_id = (document.getElementById('rem_rec_prod_id') as HTMLInputElement).value;

    try {
      var favProd: any = await this.product
        .removeRecurringProduct(prod_id)
        .toPromise();

      if (!favProd) {
        console.log("No product with id: " + prod_id + " found!");
        return;
      }

      alert(favProd.product.product_details.name + " was added to the favourite products list!");
    } catch (e) {
      console.error(e);
    }
  }

  logout() {
    localStorage.removeItem('userData');
    this.custom.redirect('homepage');
  }

  public async addProductFromRunningOutToShoppingList() {
    var product_id = (document.getElementById('move_prod_from_running_out_to_shopping_list') as HTMLInputElement).value.toString();

    try {
      var productInRunningOut: any = await this.runningOut
        .checkIfProductIsInRunningOutList(product_id)
        .toPromise();

      if (!productInRunningOut) {
        console.log("Something Went Wrong - productInRunningOut");
        return;
      }

      if (productInRunningOut.exists) {
        //product is in rounning out list
        var product = productInRunningOut.product;
        if (!product) {
          console.log("Something Went Wrong - product is null");
          return;
        }

        var deleteProdResponse: any = await this.runningOut
          .removeProductFromRunningOutList(product.product_details.id)
          .toPromise();

        if (!deleteProdResponse) {
          console.log("Something Went Wrong - deleteProdResponse");
          return;
        }

        if (deleteProdResponse.isDeleted) {
          //product removed from running out
          //so add it to shopping list
          var addProdToShopListResponse = await this.shoppingListProduct
            .addProductToShoppingList(product)
            .toPromise();
        } else {
          console.log("Product with id " + product_id + " is not part of the running out list!");
          return;
        }

        if (!addProdToShopListResponse) {
          console.log("Something Went Wrong - addProdToShopListResponse");
          return;
        }

      } else {
        console.log("Product with id " + product_id + " is not part of the running out list!");
        return;
      }
    } catch (e) {
      console.error(e);
    }
  }

  async genNotif() {


    try {
      var d: SIMNotification = {
        id: "",
        index: 0,
        severity: "low",
        message: "",
        source: "",
        isRead: false,
      };
      d.id = this.custom.makeRandom();
      d.message = "Kati xalase";
      d.severity = "low";
      d.source = "Inventory Manager";

      var resp = await this.notification
        .createNotification(d)
        .toPromise();

      if (!resp) {
        return;
      }
      // var a = new DashboardComponent(this.custom, null, this.notification);
      // a.setNotificationsNumber();


      var newNotification = {
        status: true,
        notification: d
      }
      await this.socketsService
        .sendSocketRequest('created-notification', newNotification)
        .toPromise();

      console.log(resp);
    } catch (e) {
      console.error(e);
    }
  }

  public async placeContainerAtoB() {
    var container_id = (document.getElementById('place_container_id') as HTMLInputElement).value;
    var container_base_id = (document.getElementById('place_container_position_id') as HTMLInputElement).value;

    try {
      var replacement: any = await this.containerService
        .setCurrentContainerOfBase(container_id, container_base_id)
        .toPromise();

      if (!replacement) {
        console.log("ERROR - placeContainerAtoB - replacement is null");
        return;
      }

      if (!replacement.data.has_correct_container) {
        // var containerBase = await this.containerService
        // .getBaseByContainerId(replacement.data.corresponding_container.toString())
        // .toPromise();
        // console.log(containerBase);

        this.incorrectContainerWarning(container_id, replacement.data.corresponding_container);
        return;
      }
      console.log(replacement.message);
      //TODO
      //check if container is placed to the correct base

    } catch (e) {
      console.error(e);
    }
  }

  public async takeContainer() {
    //set current container id to null
    var base_id = (document.getElementById("take_container_base_id") as HTMLInputElement).value;
    try {
      var container2take = await this.containerService
        .takeContainerFromBaseWithId(base_id)
        .toPromise();

      if (!container2take) {
        console.log("ERROR - takeContainer - container2take is null");
        return;
      } else if (!container2take.current_container) {
        console.log("ERROR - current_container - container2take is null");
        return;
      } else {
        console.log("Took container with id \"" + container2take.current_container +
          "\" from base with id \"" + base_id + "\"!");


        this.startContainerTimer(base_id);
      }
    } catch (e) {
      console.error(e);
    }
  }

  startContainerTimer(base_id) {
    var root = this;
    setTimeout(function () {
      console.log("Please replace " + base_id);

      var data = {
        source: "Kitchen",
        info: "Please replace " + base_id
      };
      root.custom.sendVoiceAssistantNotification(data);
    }, 10000);

  }

  public async incorrectContainerWarning(container_id: string, corresponding_container: string) {
    console.log(corresponding_container);
    try {
      //get product of container
      var containerEntered: any = await this.product
        .getProductByContainerId(container_id)
        .toPromise();

      //get corresponding container of given base
      var containerBase: any = await this.containerService
        .getBaseByCorrespondingContainerId(corresponding_container.toString())
        .toPromise();

      if (!containerEntered || !containerBase) {
        console.log("ERROR - checkContainerWithBase - product is null or productEntered is null");
        return;
      }

      console.log(containerBase);

      var productWithOccupiedBase: any = await this.product
        .getProductByContainerId(containerBase.data.corresponding_container)
        .toPromise();

      if (!productWithOccupiedBase) {
        console.log("ERROR - checkContainerWithBase - productWithOccupiedBase is null");
        return;
      }
      var data: PushNotification = {
        source: "containers",
        message: "Warning!" + containerEntered.products.product_details.name +
          " has been placed on the base of " + productWithOccupiedBase.products.product_details.name
      };

      this.push.generateNotification(data, false);
      console.log("Warning!" + containerEntered.products.product_details.name +
        " has been placed on the base of " + productWithOccupiedBase.products.product_details.name);


    } catch (e) {
      console.error(e);
    }
  }

  async setExpDate() {
    var id = (document.getElementById("exp_date_id") as HTMLInputElement).value.toString();
    var date = (document.getElementById("exp_date") as HTMLInputElement).value;

    var year = parseInt(date.substring(0, 4));
    var month = parseInt(date.substring(5, 7));
    var day = parseInt(date.substring(8, 10));

    try {
      await this.product
        .setExpirationDateOfProduct(id, year.toString(), month.toString(), day.toString())
        .toPromise();
    } catch (e) {
      console.error(e);
    }
  }

  setUserLocationHome() {
    this.setUserLocation("home");
  }

  setUserLocationAway() {
    this.setUserLocation("away");
  }

  async setUserLocation(location: string) {
    try {
      var event = "user-location";
      var data = {
        "location": location
      }
      await this.socketsService
        .sendSocketRequest(event, data)
        .toPromise();

    } catch (e) {
      console.error(e);
    }
  }

  async getExpiringToday() {
    try {
      var exp = await this.product
        .getExpiringProductsNames()
        .toPromise();

      if (!exp) {
        console.log("ERROR - getExpiringToday - exp is null");
        return;
      }

      var data: PushNotification = {
        source: "Expired",
        message: exp.prod.length + " Products have expired"
      }
      this.push.generateNotification(data, true);
      var n: SIMNotification = {
        id: "",
        index: 0,
        severity: "low",
        message: exp.prod.length + " Products have expired",
        source: " Expired ",
        isRead: false
      }

      await this.notification.createNotification(n).toPromise();
      console.log(exp);

    } catch (e) {
      console.error(e);
    }
  }
}
