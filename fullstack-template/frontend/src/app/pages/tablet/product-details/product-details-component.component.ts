import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';

@Component({
  selector: 'ami-fullstack-product-details-component',
  templateUrl: './product-details-component.component.html',
  styleUrls: ['./product-details-component.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  // id : string;

  constructor(private route : ActivatedRoute,
  private productsService : ProductsService,
  ) {}

  ngOnInit() {
      this.getDetails();
  }

  public async getDetails(){
    var id = this.route.snapshot.params['id'];

    try{
      var details = await this.productsService
      .getProductById(id)
      .toPromise();

      console.log(details);
    }catch(e){
      console.error(e);
    }
  }

}
