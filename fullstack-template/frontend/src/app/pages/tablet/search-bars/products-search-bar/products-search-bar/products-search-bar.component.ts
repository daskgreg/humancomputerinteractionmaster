import { Component, OnInit, Input, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { Product } from 'src/app/global/models/products/product.model';

@Component({
  selector: 'ami-fullstack-products-search-bar',
  templateUrl: './products-search-bar.component.html',
  styleUrls: ['./products-search-bar.component.scss']
})
export class ProductsSearchBarComponent implements OnInit {
  public search: any = '';
  productsArray: any = [];
  @Input() collectionToSearch: any;//service object sent by caller component
  @Input() callerComponent: any;
  @Input() doAction: any;
  public showProducts = false;
  public showSearch = false;
  public showDropDown = false;
  public clickedProduct = false;
  public change_z_index = false;
  public change_z_index_clicked_search = false;
  public focused = false;
  public showContainer = false;


  public listSize;
  // public searchInputCheck = this.product;

  constructor(private products: ProductsService) { }


  ngOnInit() {
    this.initLocked();
  }

  public async initLocked() {
    try {
      var d: any = await this.collectionToSearch
        .getAllProductNames()
        .toPromise();

      // var d : any= await this.products//REMOVE
      // .getAllProductNames()
      // .toPromise();

      this.listSize = d.prod.length;

      for (var i = 0; i < d.prod.length; i++) {
        var t;

        t = {
          "Index": i,
          "Name": d.prod[i].product_details.name.toString(),
          "UniqueId": d.prod[i].product_details.id.toString()
        }
        // }
        // console.log(t);
        this.productsArray[i] = t;
      };

    } catch (e) {
      console.error(e);
    }


  }

  clickedProd(id) {
    console.log(this.productsArray[id]);
    this.clickedProduct = true;
    this.focusOut();

    this.doAction;
  }

  searchInputCheck(input) {
    if (input === "") {
      this.showProducts = false;
    }

    if (input) {
      console.log(input.length);
      if (input.length == 0) {
        this.showProducts = false;
      } else {
        this.showProducts = true;
      }
    }
  }

  @ViewChildren("searchBar") searchBar: QueryList<ElementRef>;
  searchBarController() {
    this.showContainer = !this.showContainer;
    this.showSearch = !this.showSearch;
    this.change_z_index_clicked_search = true;
    this.focused = true;
    document.getElementById('SearchContainer').style.zIndex = "9";


    this.showProducts = false;
    // if(document.getElementById('searchBar')){
    //   (document.getElementById('searchBar') as HTMLInputElement).value="";
    // }
    this.searchBar.changes.subscribe(() => {
      this.focusSearch();
    })
  }

  focusSearch() {
    if (!this.showSearch) return;
    document.getElementById('searchBar').focus();

    if (document.getElementById('searchBar') && this.focused) {
      console.log("reset");
      (document.getElementById('searchBar') as HTMLInputElement).value = "";
      console.log("reset" + (document.getElementById('searchBar') as HTMLInputElement).value);
      this.focused = false;
    }
  }

  focusOut() {
    if (this.clickedProduct) {
      this.showSearch = false;
      this.showProducts = false;
      this.clickedProduct = false;
    } else {
      this.showSearch = false;
      this.showProducts = false;
    }

  }

  containerClicked() {
    console.log("click!");
    if (!this.change_z_index_clicked_search) {
      document.getElementById('SearchContainer').style.zIndex = "0";
      this.change_z_index_clicked_search = false;
      if (this.clickedProduct) {
        this.showSearch = false;
        this.showProducts = false;
        this.clickedProduct = false;
      } else {
        this.showSearch = false;
        this.showProducts = false;
      }
    }
    this.change_z_index_clicked_search = false;

  }

  resetSearch(e) {
    //console.log(e);
  }
}