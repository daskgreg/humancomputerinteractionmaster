import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocketsService, CustomService } from 'src/app/global/services';
import { NotificationsComponent } from '../notifications/notifications.component';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';

@Component({
  selector: 'ami-fullstack-topside-user-bar',
  templateUrl: './topside-user-bar.component.html',
  styleUrls: ['./topside-user-bar.component.css']
})
export class TopsideUserBarComponent implements OnInit {


  public showUserBar = false;
  public loaded = false;
  public loadedImg = false;
  // public currPageUrl;
  public currVisiblePage;

  constructor(private socketsService: SocketsService,
    private router: Router,
    private custom: CustomService,
    private notification: NotificationsService) {
  }
  ngOnInit(): void {
    this.setUserImg();
    this.setUserName();
    this.setPageLocation();
    this.showNotifications();
  }

  setUserImg() {
    if (localStorage.getItem('userData')) {
      var userData = localStorage.getItem('userData');
      var imgType = JSON.parse(userData).image_type;
      var base64Img = JSON.parse(userData).image_data;
      (document.getElementById('user-profile') as HTMLImageElement).src =
        "data: " + imgType + ";base64, " + base64Img;
    }
  }

  setUserName() {
    if (localStorage.getItem('userData')) {
      var userData = localStorage.getItem('userData');
      var userName = JSON.parse(userData).first_name + " " + JSON.parse(userData).last_name;
      (document.getElementById('user-name') as HTMLDivElement).innerText = userName;

    }
  }

  setPageLocation() {
    var fullURl = this.router.url.toString();
    fullURl = fullURl.replace("-", " ");
    fullURl = fullURl.replace("/", "");
    var length = fullURl.length;
    for (var i = 0; i < length; i++) {
      if (i == 0) {
        var capital = fullURl[i].toUpperCase();
        fullURl = capital + fullURl.substr(1, fullURl.length)
      }
      if (fullURl[i] == "-") {
        fullURl = fullURl.replace("-", " ");
      }
      if (fullURl[i] == " ") {
        var capital = " " + fullURl[i + 1].toUpperCase();
        fullURl = fullURl.substr(0, i) + capital + fullURl.substr(i + 2, fullURl.length)
      }
    }
    (document.getElementById('top-side-bar-where-am-i') as HTMLDivElement).innerText = fullURl;
  }

  public async showNotifications() {
    try {
      var latestNotification: any = await this.notification
        .getLatestNotification()
        .toPromise();

      if (!latestNotification) {
        console.log("ERROR - showNotifications - latestNotification is null");
        return;
      }
      (document.getElementById('notification-txt') as HTMLDivElement).innerText =
        latestNotification.data.message;
    } catch (e) {
      console.error(e);
    }
  }

  redirect(target: string) {
    this.custom.redirect(target);
  }
}
