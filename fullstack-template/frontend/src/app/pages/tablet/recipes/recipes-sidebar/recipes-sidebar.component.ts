import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ami-fullstack-recipes-sidebar',
  templateUrl: './recipes-sidebar.component.html',
  styleUrls: ['./recipes-sidebar.component.scss']
})
export class RecipesSidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
