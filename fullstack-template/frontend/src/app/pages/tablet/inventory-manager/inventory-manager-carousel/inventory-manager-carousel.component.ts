import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ami-fullstack-inventory-manager-carousel',
  templateUrl: './inventory-manager-carousel.component.html',
  styleUrls: ['./inventory-manager-carousel.component.scss']
})
export class InventoryManagerCarouselComponent implements OnInit {
  title = 'angularowlslider';
  customOptions: any = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }
  
  constructor() { }

  ngOnInit() {
  }

}
