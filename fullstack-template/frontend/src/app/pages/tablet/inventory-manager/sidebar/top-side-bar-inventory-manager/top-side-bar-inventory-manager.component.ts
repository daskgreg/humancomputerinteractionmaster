import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'ami-fullstack-top-side-bar-inventory-manager',
  templateUrl: './top-side-bar-inventory-manager.component.html',
  styleUrls: ['./top-side-bar-inventory-manager.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopSideBarInventoryManagerComponent implements OnInit {
  today: number = Date.now();

  constructor(private router: Router) {
    setInterval(() => {this.today = Date.now()}, 1);
  }
  
  ngOnInit() {
  }

  redirect(target:String){
    console.log(target);
    this.router.navigate([target]);
  }
  
}
