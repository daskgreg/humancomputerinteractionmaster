import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'ami-fullstack-page-not-found-error',
  templateUrl: './page-not-found-error.component.html',
  styleUrls: ['./page-not-found-error.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PageNotFoundErrorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
