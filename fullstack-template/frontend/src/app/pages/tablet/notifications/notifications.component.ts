import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { CustomService, SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  constructor(private notification: NotificationsService,
    private custom: CustomService,
    private socketService: SocketsService) { }

  ngOnInit() {
    ;
  }



  redirect(target) {
    this.custom.redirect(target);
  }
}
