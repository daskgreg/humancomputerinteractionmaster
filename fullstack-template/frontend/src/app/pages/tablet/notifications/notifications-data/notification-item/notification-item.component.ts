import { Component, OnInit, Input } from '@angular/core';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { CustomService, SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-notification-item',
  templateUrl: './notification-item.component.html',
  styleUrls: ['./notification-item.component.scss']
})
export class NotificationItemComponent implements OnInit {
  @Input() index: number;
  @Input() notification: SIMNotification;//object
  @Input() styles: any;



  constructor(
    private notificationService: NotificationsService,
    private socketService: SocketsService
  ) { }

  ngOnInit() {

  }

  async doStuff() {
    try {
      console.log(this.notification);
      var d = await this.notificationService
        .deleteNotification(this.notification.index.toString())
        .toPromise();

      var event = 'delete-notif';

      this.socketService
        .sendSocketRequest(event, null)
        .toPromise();


    } catch (e) {
      console.error(e);
    }
  }

}
