import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ViewEncapsulation } from '@angular/core';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { Product } from 'src/app/global/models/products/product.model';
import { NotificationItemComponent } from './notification-item/notification-item.component';
import { Observable } from 'rxjs';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { HttpClient } from '@angular/common/http';
import { lowerCase } from 'lodash';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-notifications-data',
  templateUrl: './notifications-data.component.html',
  styleUrls: ['./notifications-data.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NotificationsDataComponent implements OnInit {
  public components = [];
  private _counter: number = 0;

  // public notifications;
  public changeNotificationBackgroundColor = false;
  public notifications$: SIMNotification;
  public showNotificationsList = false;

  constructor(private notification: NotificationsService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private http: HttpClient,
    private socketService: SocketsService) {
    this.loadNotifications();
    this.readAllNotifications();
    // this.getAllNotificationBySeverity("");

  }

  ngOnInit() {
    this.socketService.syncMessages("delete-notif").subscribe((data) => {
      console.log("skasaskas");
      this.loadNotifications();
    })

    this.loadNotifications();
  }

  changeColor() {
    if (this.changeNotificationBackgroundColor) {
      document.getElementById('bgcolor').style.backgroundColor = 'red';
      console.log("kokkino");
    } else {
      document.getElementById('bgcolor').style.backgroundColor = 'blue';
      console.log("mple");
    }
    this.changeNotificationBackgroundColor = !this.changeNotificationBackgroundColor;
  }


  public async readAllNotifications() {
    try {
      var readAll = await this.notification
        .readAllNotifications()
        .toPromise();

      if (!readAll) {
        console.log("ERROR - readAllNotifications - readAll is null");
        return;
      }
    } catch (e) {
      console.error(e);
    }
  }




  public async loadNotifications() {
    try {
      var notificationsArray = await this.notification
        .getAllNotifications()
        .toPromise();

      if (!notificationsArray) {
        console.log("ERROR");
        return;
      }
      this.showNotificationsList = true;
      this.notifications$ = notificationsArray;
    } catch (e) {
      console.error(e);
    }
  }

  public async getAllNotificationBySeverity(sev: string) {
    try {
      var getAll: any;
      if (sev == "") {
        getAll = await this.notification
          .getAllNotifications()
          .toPromise();
      } else {
        getAll = await this.notification
          .getAllNotificationsBySeverity(sev)
          .toPromise();
      }
      if (!getAll) {
        console.log("erorr - getallnotification - getall isnull");
        return;
      }
      this.showNotificationsList = true;
      this.notifications$ = getAll;
    } catch (e) {
      console.error(e);
    }
  }
  public showNotCat = true;
  filtClick(sev) {
    if (sev == "all") {
      this.getAllNotificationBySeverity("");
      document.getElementById("ptext").innerHTML = "All Notifications";
      return;
    }
    if (sev == "low") {
      this.getAllNotificationBySeverity("low");
      this.showNotificationsList = true;
      document.getElementById("ptext").innerHTML = "Waste Manager";
    }
    if (sev == "high") {
      this.getAllNotificationBySeverity("high");
      this.showNotificationsList = true;
      document.getElementById("ptext").innerHTML = "Products are Empty";
    }
    if (sev == "severe") {
      this.getAllNotificationBySeverity("severe");
      this.showNotificationsList = true;
      document.getElementById("ptext").innerHTML = "Products Running out";
    }
    if (sev == "info") {
      this.getAllNotificationBySeverity("info");
      this.showNotificationsList = true;
      document.getElementById("ptext").innerHTML = "Full Products";
    }

  }

}
