import { Component, OnInit, ViewEncapsulation, HostListener, Directive, Input } from '@angular/core';
import { CustomService, SocketsService } from 'src/app/global/services';
import { ReceiptProductsService } from 'src/app/global/services/products/receipt-products/receipt-products.service';
import { Router } from "@angular/router";
import * as Globals from '../../../global/variables/variables';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { PoorMansPushNotificationComponent } from '../../poor-mans-push-notification/poor-mans-push-notification.component';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { NotificationsComponent } from '../notifications/notifications.component';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { SendReceiptIdToScannedProducts } from 'src/app/global/services/frontend-services/send-receipt-id-to-scanned-products/send-receipt-id-to-scanned-products.service';
import { subscribeOn } from 'rxjs/operators';
import { runInThisContext } from 'vm';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';

@Component({
  selector: 'ami-fullstack-scanned-receipt-products',
  templateUrl: './scanned-receipt-products.component.html',
  styleUrls: ['./scanned-receipt-products.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ScannedReceiptProductsComponent implements OnInit {
  @Input() productsArrayInput;

  public whereToSearch = this.receipt;
  public action = this.addProduct(this);
  public showItems = false;
  public showSearch = false;
  public showScannedProductsList = false;

  public scannedProducts$ = [];

  public receipt_id;

  public placedProducts = [];

  constructor(private custom: CustomService,
    private push: PoorMansPushNotificationComponent,
    private notification: NotificationsService,
    private router: Router,
    private receipt: ReceiptProductsService,
    private receiptIdSingleton: SendReceiptIdToScannedProducts,
    private socketsService: SocketsService,
    private shoppingListService: ShoppingListProductService
  ) {



  }

  redirect(target: String) {
    // console.log(target);
    this.router.navigate([target]);
  }

  async getScannedProducts(rec) {

    if (!rec) {
      var push: PushNotification = {
        message: "No receipt scanned!",
        source: "Scanned receipt products"
      }

      this.push.generateNotification(push, false);

      var n: SIMNotification = new SIMNotification();
      n.message = "No receipt scanned!";
      n.source = "Scanned receipt products";
      n.severity = "low";

      this.notification.createNotification(n);

      this.custom.redirect('super-market');
      return;
    }

    try {
      var scannedProductsArray: any = await this.receipt
        .getProductsFromReceiptWithID(rec)
        .toPromise();

      if (!scannedProductsArray) {
        console.log("ERROR - scannedProductsArray - scannedProductsArray is null");
        return;
      }

      if (!scannedProductsArray.success) {
        console.log("ERROR - scannedProductsArray - scannedProductsArray failed");
        return;
      }

      this.scannedProducts$ = scannedProductsArray.data[0].products;

      this.crosscheckScannedWithShopList();
    } catch (e) {
      console.error(e);
    }
  }

  async crosscheckScannedWithShopList() {
    try {
      var shoppingList: any = await this.shoppingListService
        .getAllProductNames()
        .toPromise();

      if (!shoppingList) {
        console.log("ERROR - crossckeck - shoppingList");
        return;
      }

      var scannedProducts = this.scannedProducts$;
      var difference = [];
      var found = false;

      console.log(shoppingList.data);
      console.log(scannedProducts);


      for (var i = 0; i < shoppingList.data.length; i++) {
        for (var j = 0; j < scannedProducts.length; j++) {
          if (shoppingList.data[i].product_details.id == scannedProducts[j].product_details.prod_id) {
            found = true;
            break;
          }
        }
        if (!found) {
          difference.push(shoppingList.data[i].product_details);
        }
        found = false;
      }

      var data = {};

      if (difference.length > 0) {
        if (difference.length > 1) {
          data = {
            source: "Shopping List",
            info: "You didn't buy " + difference.length + " products from the shopping list"
          };
        } else {
          data = {
            source: "Shopping List",
            info: "You didn't buy " + difference[0].name + " from the shopping list"
          };
        }

        this.custom.sendVoiceAssistantNotification(data);
      }


      //this.purgeShoppingList();
    } catch (e) {
      console.log(e);
    }
  }

  ngOnInit() {
    this.socketsService.syncMessages("scanned-receipt").subscribe((data) => {
      if (data.message.status) {
        this.showScannedProductsList = data.message.status;
        this.getScannedProducts(data.message.receipt_id);
      } else {
        this.redirect('super-market');
      }
    });

    this.socketsService.syncMessages("end-classification").subscribe((data) => {
      Globals.classifyingProducts.receipt_id = "";
      Globals.classifyingProducts.status = false;
      this.redirect("super-market");
    });

    this.socketsService.syncMessages("remove-receipt-product").subscribe((data) => {
      this.placedProducts.push(data.message.product_id)
    });
  }

  async purgeShoppingList() {
    try {
      this.shoppingListService
        .emptyShoppingList()
        .toPromise();
    } catch (e) {
      console.error(e);
    }
  }

  searchController() {
    this.showSearch = !this.showSearch;
  }

  removeProduct(element) {
    console.log(element.getAttribute("product_id"));

  }

  addProduct(element) {
    //if the user adds a product, we take as a fact tha the product
    //already existed in the household, so we just refill
    //new field in model with vlaue before refill, so the user can undo it(??)

    //Future Work: add a product that came into the household for the fist time

  }

  openSearchBar() {
    this.showItems = true;
    this.showSearch = true;
  }

  endClassification() {
    Globals.classifyingProducts.receipt_id = "";
    Globals.classifyingProducts.status = false;

    this.checkUnplacedProducts();

    this.sendSocketReq();

    this.redirect("super-market");
  }

  checkUnplacedProducts() {
    var diff = this.scannedProducts$.length - this.placedProducts.length;
    if (diff > 0) {
      var data = {
        source: "Shopping List Classification Ended",
        info: "You haven't placed " + diff + " products"
      };
      this.custom.sendVoiceAssistantNotification(data);
    }
  }

  async sendSocketReq() {
    var data = {
      "status": false
    }
    var event: string = "scanned-receipt";
    try {
      var s = await this.socketsService
        .sendSocketRequest(event, data)
        .toPromise();
    } catch (e) {
      console.log(e);
    }
  }

}
