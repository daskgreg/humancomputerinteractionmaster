import { Component, OnInit, ViewEncapsulation, HostListener, Directive } from '@angular/core';
import { CustomService } from 'src/app/global/services';
import { ReceiptProductsService } from 'src/app/global/services/products/receipt-products/receipt-products.service';
import { Router } from "@angular/router";

@Component({
  selector: 'ami-fullstack-scanned-receipt-products',
  templateUrl: './scanned-receipt-products.component.html',
  styleUrls: ['./scanned-receipt-products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ScannedReceiptProductsComponent implements OnInit {

  public whereToSearch = this.receipt;

  constructor(private custom: CustomService,
    // private classification : ClassificationService,
    private router: Router,
    private receipt: ReceiptProductsService) { }

  redirect(target: String) {
    // console.log(target);
    this.router.navigate([target]);
  }
  ngOnInit() {
    let receipt = JSON.parse(localStorage.getItem('scannedReceipt'));

    if (!receipt) {
      this.custom.redirect('super-market');
      return;
    }
    // console.log(receipt);
    console.log('parsed receipt from local storage');
    localStorage.removeItem('scannedReceipt');
    // console.log(receipt.receipt.products);
    let i = 0;//sthlh pretty much
    var top;//apostash apo top, analoga thn grammh
    var rows = 0;//se poia gramm eimai, allazei kathe 4o if
    var gap = 42.51;//keno anamesa se prods
    var c_width = 377.49;//width tou container of every product
    var c_height = 131;//height tou container of every product
    receipt.receipt.products.forEach(element => {


      let container = document.createElement('div');
      container.id = 'product-container-' + i.toString();
      let ContId = container.id;
      container.classList.add('product_container');
      container.addEventListener('click',
        function () {
          console.log(this.id);
          // var default_color = this.style.backgroundColor;
          if (this.style.backgroundColor == "red") {
            this.style.backgroundColor = "#CBE6E9";
          } else {
            this.style.backgroundColor = "red";
          }

        });

      // container.setAttribute("(click)","productPlaced()");
      document.getElementById('scanned-products-container').appendChild(container);

      let prod_img = document.createElement('img');
      prod_img.id = 'product-img-' + i.toString();
      prod_img.src = '';
      prod_img.classList.add('product_image');
      document.getElementById('product-container-' + i.toString()).appendChild(prod_img);

      let prod_name = document.createElement('p');
      prod_name.id = 'product-name-' + i.toString();
      prod_name.innerText = element.product_details.name;
      prod_name.classList.add('product_name');
      document.getElementById('product-container-' + i.toString()).appendChild(prod_name);


      // prepei na ftiaxtei thesi sto json gia ta kila pou pairnei apo to supermarket /product

      // let prod_kg = document.createElement('p_kg');
      // prod_kg.id = 'prod-kg-' + i.toString();
      // prod_kg.innerText = element.product_details.kg;
      // prod_kg.classList.add('product_kg');
      // document.getElementById('product-container-' + i.toString()).appendChild(prod_kg);


      if (i % 3 == 0) {
        var temp_width = 29;
        if (rows == 0) {
          top = 27;//allazeis to 50 gia na megalwsei to keno apo thn KORYFH MONO gia thn 1h seira
        } else {
          top = rows * 350;//allazeis to 350 gia na megalwsei to keno apo prod se prod
        }
        top += c_height;
        var styles = document.createElement('style');
        styles.innerHTML =
          `#product-container-` + i.toString() +
          `{
            position:absolute;
            width:` + c_width.toString() + `px;
            height:` + c_height.toString() + `px;
            opacity: 71%;
            background-color: #5daa3b;
            border-radius: 10px;
            top:` + top.toString() + `px;
            left:` + temp_width + `px;
          }`;
        //  THESIS KOUMPIA, THESI PRODUCT TEXT, THESI PRODUCT BRAND, THESI KG
        // oles autes oi theseis einai gia 


        // var styles2 = document.createElement('styles2');
        // styles2.innerHTML=
        // `#product_name` +
        // `{
        //   font-size:25px;
        //   color:#fdf5db;
        //   font-family: Arial,Helvetica,sans-serif;
        // }`;
        // var styles4 = document.createElement('styles4');
        // styles4.innerHTML = 
        // `#brand_name` +
        // `{
        //   position:absolute;
        //   left:164px;
        //   top:45px;
        //   font-size:13px;
        //   font-family: "Franklin Gothic Medium", "Arial Narrow", Arial, sans-serif;
        //   color: #344a5e;
        // }`
        // var btnMinus = document.createElement('button');
        // `#minus-btn` +
        // `{
        //   position: absolute;
        //   width: 45px;
        //   height: 26px;
        //   left: 164px;
        //   top: 93px;
        //   background-repeat: no-repeat;
        //   background-size: contain;
        //   background-image: url("./../images/icons/sp-list/sp-list-v3/minus-button.png");
        //   border: none;
        // }`
        // var btnPlus = document.createElement('button');
        // `#plus-btn` +
        // `{
        //   position:absolute;
        //   width: 45px;
        //   height: 26px;
        //   left: 316px;
        //   top: 93px;
        //   background-repeat: no-repeat;
        //   background-size: contain;
        //   background-image: url("./../images/icons/sp-list/sp-list-v3/plus-button.png");
        //   border: none;
        // }`
        // var styles = document.createElement('styles');
        // `#how-much-kg` + 
        // `{
        //  position: absolute;
        // left: 241px;
        // top: 97px;
        // font-size: 18px;
        // font-family: Arial, Helvetica, sans-serif;
        // color: #344a5e;
        //}`




      } else if (i % 3 == 1) {
        var temp_width = 29 + c_width + gap;//100px to keno apo to aristero div kai c_width to width tou prod
        if (rows == 0) {
          top = 27;//px
        } else {
          top = rows * 350;//allazeis to 350 gia na megalwsei to keno apo prod se prod
        }
        top += c_height;
        var styles = document.createElement('style');
        styles.innerHTML =
          `#product-container-` + i.toString() +
          `{
            position: absolute;
            width:` + c_width.toString() + `px;
            height:` + c_height.toString() + `px;
            background-color: #CBE6E9;
            border-radius:15px;
            top:` + top.toString() + `px;
            left:` + temp_width.toString() + `px;
          }`;
      } else if (i % 3 == 2) {
        var temp_width = 29 + (2 * c_width + gap) + gap;
        if (rows == 0) {
          top = 27;//px
        } else {
          top = rows * 350;//allazeis to 350 gia na megalwsei to keno apo prod se prod
        }
        top += c_height;
        var styles = document.createElement('style');
        styles.innerHTML =
          `#product-container-` + i.toString() +
          `{
            width:` + c_width.toString() + `px;
            height:` + c_height.toString() + `px;
            background-color: #CBE6E9;
            border-radius:15px;
            position: absolute;
            top:` + top.toString() + `px;
            left:` + temp_width.toString() + `px;
          }`;
        rows++;
      }
      document.getElementsByTagName('body')[0].appendChild(styles);


      i++;
    });


  }

  openSearchBar() {
    var searchBoxMini = document.getElementById("search-box-mini");
    var searchBoxOpen = document.getElementById("search-box-open");

    if (searchBoxMini.style.display === "block") {
      searchBoxMini.style.display = "none";
      searchBoxOpen.style.display = "block";
    } else {
      searchBoxMini.style.display = "none";
      searchBoxOpen.style.display = "block";
    }
  }
  closeSearchBar() {
    var searchBoxMini = document.getElementById("search-box-mini");
    var searchBoxOpen = document.getElementById("search-box-open");

    if (searchBoxOpen.style.display === "block") {
      searchBoxOpen.style.display = "none";
      searchBoxMini.style.display = "block";
    } else {
      searchBoxOpen.style.display = "none";
      searchBoxMini.style.display = "block";
      alert("skata");
    }
  }


  endClassification() {
    // var j;
    // this.classification.getData().subscribe(data=>{
    //   j = data;
    // });

    //   if(j.status){

    //     //trial
    //     var data : Classification= {
    //       status: false,
    //       receipt_id: ""
    //     }
    //     this.classification.changeData(data);

    //     this.custom.redirect("dashboard");
    //   }else{
    //     console.log("No classification active");
    //   }
  }

}