import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'ami-fullstack-top-side-bar-scanned-receipt',
  templateUrl: './top-side-bar-scanned-receipt.component.html',
  styleUrls: ['./top-side-bar-scanned-receipt.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopSideBarScannedReceiptComponent implements OnInit {
  today: number = Date.now();

  constructor(private router: Router) {
    setInterval(() => {this.today = Date.now()}, 1);
  }
  
  ngOnInit() {
    this.setUserImg();
  }

  redirect(target:String){
    console.log(target);
    this.router.navigate([target]);
  }

  setUserImg(){
    if(localStorage.getItem('userData')){
      var userData = localStorage.getItem('userData');
      var imgType = JSON.parse(userData).image_type;
      var base64Img = JSON.parse(userData).image_data;
      (document.getElementById('user-profile') as HTMLImageElement).src=
      "data: " + imgType + ";base64, " + base64Img;
    }
  }
  
}
