import { Component, OnInit } from '@angular/core';
import { SocketsService, CustomService } from 'src/app/global/services';
import { PoorMansPushNotificationComponent } from '../../poor-mans-push-notification/poor-mans-push-notification.component';
import { ReceiptProductsService } from '../../../global/services/products/receipt-products/receipt-products.service';

@Component({
  selector: 'ami-fullstack-cabbinet-monitor',
  templateUrl: './cabbinet-monitor.component.html',
  styleUrls: ['./cabbinet-monitor.component.scss']
})
export class CabbinetMonitorComponent implements OnInit {

  public showSimLogo = true;
  public showScannedProducts = false;

  public socketEvent;

  public productsArray$;

  public today;

  constructor(
    private socketsService: SocketsService,
    private pushService: PoorMansPushNotificationComponent,
    private receiptService: ReceiptProductsService,
    private custom: CustomService
  ) {

  }

  ngOnInit() {
    this.socketsService.syncMessages("scanned-receipt").subscribe((data) => {
      this.showSimLogo = !data.message.status;
      this.showScannedProducts = data.message.status;
      if (data.message.status) {
        this.fetchProductsFromReceipt(data.message.receipt_id);
      }
    });

    this.socketsService.syncMessages("end-classification").subscribe((data) => {
      this.showSimLogo = true;
      this.showScannedProducts = false;
    });

    this.socketsService.syncMessages("remove-receipt-product").subscribe((data) => {
      var removedProductId = data.message.product_id;
      var l = this.productsArray$.length;
      for (var i = 0; i < l; i++) {
        if (this.productsArray$[i].product_details.prod_id == removedProductId) {
          this.productsArray$.splice(i, 1);
        }
      }
      console.log(this.productsArray$)
    });

    this.custom.setTitle("SIM | Cabbinets");
  }

  async fetchProductsFromReceipt(receipt_id) {
    try {
      var array: any = await this.receiptService
        .getProductsFromReceiptWithID(receipt_id)
        .toPromise();


      this.productsArray$ = array.data[0].products;

    } catch (e) {
      console.error(e);
    }
  }

  async endClassification() {
    this.endClassificationSocketReq();

  }


  async endClassificationSocketReq() {
    try {
      await this.socketsService
        .sendSocketRequest('end-classification', null)
        .toPromise();
    } catch (e) {
      console.error(e);
    }
  }

}
