import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeDevicesTestComponent } from './home-devices-test/home-devices-test.component';
import { HomeDevicesRoutingModule } from './home-devices.routing';
import { FridgeMonitorComponent } from './fridge-monitor/fridge-monitor.component';
import { CabbinetMonitorComponent } from './cabbinet-monitor/cabbinet-monitor.component';



@NgModule({
  declarations: [HomeDevicesTestComponent, FridgeMonitorComponent, CabbinetMonitorComponent],
  imports: [
    CommonModule,
    HomeDevicesRoutingModule
  ]
})
export class HomeDevicesModule { }
