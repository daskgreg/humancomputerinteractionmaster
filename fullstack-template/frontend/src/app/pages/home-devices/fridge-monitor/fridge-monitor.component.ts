import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { SocketsService, CustomService } from 'src/app/global/services';
import * as Globals from '../../../global/variables/variables';
import { PoorMansPushNotificationComponent } from '../../poor-mans-push-notification/poor-mans-push-notification.component';
import { ReceiptProductsService } from 'src/app/global/services/products/receipt-products/receipt-products.service';
import { QrScannerComponent } from 'angular2-qrscanner';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-fridge-monitor',
  templateUrl: './fridge-monitor.component.html',
  styleUrls: ['./fridge-monitor.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FridgeMonitorComponent implements OnInit {

  public showSimLogo = true;
  public showScannedProducts = false;

  public socketEvent;

  public productsArray$;

  public today;

  public rsp;
  public receiptValid: boolean = true;
  public showScanner = false;

  constructor(
    private socketsService: SocketsService,
    private pushService: PoorMansPushNotificationComponent,
    private receiptService: ReceiptProductsService,
    private custom: CustomService,
    private receiptProd: ReceiptProductsService,
    private notification: NotificationsService,
    private push: PoorMansPushNotificationComponent,
    private router: Router
  ) {

  }

  @ViewChild(QrScannerComponent, null) qrScannerComponent: QrScannerComponent;

  ngOnInit() {
    this.socketsService.syncMessages("scanned-receipt").subscribe((data) => {
      this.showSimLogo = !data.message.status;
      this.showScannedProducts = data.message.status;
      if (data.message.status) {
        this.fetchProductsFromReceipt(data.message.receipt_id);
      }
    });

    this.socketsService.syncMessages("end-classification").subscribe((data) => {
      this.showSimLogo = true;
      this.showScannedProducts = false;
    });

    this.socketsService.syncMessages("remove-receipt-product").subscribe((data) => {
      var removedProductId = data.message.product_id;
      var l = this.productsArray$.length;
      for (var i = 0; i < l; i++) {
        if (this.productsArray$[i].product_details.prod_id == removedProductId) {
          this.productsArray$.splice(i, 1);
        }
      }
      console.log(this.productsArray$)
    });

    this.socketsService.syncMessages("fridge-scan-products").subscribe((data) => {
      this.showSimLogo = !this.showSimLogo;
      this.showScanner = !this.showScanner;

      if (this.showSimLogo) {
        this.qrScannerComponent.getMediaDevices().then(devices => {
          console.log(devices);
          const videoDevices: MediaDeviceInfo[] = [];
          for (const device of devices) {
            if (device.kind.toString() === 'videoinput') {
              videoDevices.push(device);
            }
          }
          if (videoDevices.length > 0) {
            let choosenDev;
            for (const dev of videoDevices) {
              if (dev.label.includes('front')) {
                choosenDev = dev;
                break;
              }
            }
            if (choosenDev) {
              this.qrScannerComponent.chooseCamera.next(choosenDev);
            } else {
              this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
            }
          }
        });

        this.qrScannerComponent.capturedQr.subscribe(result => {
          this.checkIfReceiptValid(result);

        });
      }

    });

    this.custom.setTitle("SIM | Fridge");

  }

  async fetchProductsFromReceipt(receipt_id) {
    try {
      var array: any = await this.receiptService
        .getProductsFromReceiptWithID(receipt_id)
        .toPromise();


      this.productsArray$ = array.data[0].products;
      console.log(this.productsArray$);

    } catch (e) {
      console.error(e);
    }
  }

  async endClassification() {
    this.endClassificationSocketReq();

  }

  async endClassificationSocketReq() {
    try {
      await this.socketsService
        .sendSocketRequest('end-classification', null)
        .toPromise();
    } catch (e) {
      console.error(e);
    }
  }

  showScanReceipt() {
    this.custom.redirect('scan-receipt');
  }



  //############### Scan Receipt ################
  bypassQR() {
    var receipt_id = (document.getElementById('qr_bypass') as HTMLInputElement).value.toString();

    this.checkIfReceiptValid(receipt_id);

    this.showSimLogo = !this.showSimLogo;
    this.showScanner = !this.showScanner;
  }

  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }

  async checkIfReceiptValid(receipt_id) {
    try {
      var check: any = await this.receiptProd
        .getReceipt(receipt_id)
        .toPromise();

      if (check.success == false) {
        var push: PushNotification = {
          message: "No receipt found!",
          source: "Super Market"
        }
        this.push.generateNotification(push, false);

        var n: SIMNotification = new SIMNotification();
        n.message = "No receipt found!";
        n.source = "Super Market";
        n.severity = "low";

        this.notification.createNotification(n);
        this.receiptValid = false;
        return;
      }

      //this.redirect('scanned-receipt-products');

      var data = {
        "status": true,
        "receipt_id": receipt_id
      }
      var event: string = "scanned-receipt";
      await this.socketsService
        .sendSocketRequest(event, data)
        .toPromise();



      localStorage.setItem("scan", receipt_id);
    } catch (e) {
      console.error(e);
    }
  }
}
