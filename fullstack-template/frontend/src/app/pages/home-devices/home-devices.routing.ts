import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeDevicesTestComponent } from './home-devices-test/home-devices-test.component';

const routes: Routes = [
    { path: 'devices', component: HomeDevicesTestComponent },
];

@NgModule({
  imports: [],
  exports: [RouterModule]
})
export class HomeDevicesRoutingModule { }