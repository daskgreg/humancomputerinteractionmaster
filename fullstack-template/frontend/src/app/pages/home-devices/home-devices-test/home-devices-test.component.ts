import { Component, OnInit } from '@angular/core';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-home-devices-test',
  templateUrl: './home-devices-test.component.html',
  styleUrls: ['./home-devices-test.component.scss']
})
export class HomeDevicesTestComponent implements OnInit {

  public showTestDiv = false;

  constructor(private socketsService: SocketsService) { }

  ngOnInit() {
    this.socketsService.syncMessages("scanned-receipt").subscribe((data) => {
      this.showTestDiv = data.message.status;
    });
  }

}
