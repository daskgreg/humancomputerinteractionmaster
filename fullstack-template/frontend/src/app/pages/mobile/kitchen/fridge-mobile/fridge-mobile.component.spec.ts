import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FridgeMobileComponent } from './fridge-mobile.component';

describe('FridgeMobileComponent', () => {
  let component: FridgeMobileComponent;
  let fixture: ComponentFixture<FridgeMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FridgeMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FridgeMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
