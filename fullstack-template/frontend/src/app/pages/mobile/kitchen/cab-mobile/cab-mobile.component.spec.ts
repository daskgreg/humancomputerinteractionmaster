import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabMobileComponent } from './cab-mobile.component';

describe('CabMobileComponent', () => {
  let component: CabMobileComponent;
  let fixture: ComponentFixture<CabMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
