
  import { Component, OnInit, ViewEncapsulation } from '@angular/core';
  import { Product } from 'src/app/global/models/products/product.model';
  import { ProductsService } from 'src/app/global/services/products/products/products.service';
  import { CustomService } from 'src/app/global/services';
  import { Router } from '@angular/router';
  
@Component({
  selector: 'ami-fullstack-shelves-mobile',
  templateUrl: './shelves-mobile.component.html',
  styleUrls: ['./shelves-mobile.component.scss']
})
export class ShelvesMobileComponent implements OnInit {

 public haveProducts = false;
    public productsArray$ = [];
  
    constructor(private product: ProductsService,
      private custom: CustomService,
      private route: Router) {
      this.loadProducts()
    }
  
    ngOnInit() {
    }
  
    public async loadProducts() {
      // console.log("fridge async");
      var i = 0;
      try {
        var products: Product[] = await this.product
          .getProductByPosition('shelves')
          .toPromise();
  
  
        if (!products) {
          console.log("No products found!");
          return;
        }
  
        this.haveProducts = true;
  
        // console.log(products[0][0]);//No fucking clue
        while (products[0][i]) {
          if (products[0][i].details.ammount_remaining != 0) {
            this.productsArray$.push(products[0][i]);
          }
          i++;
        }
  
  
        // console.log(products);
  
      } catch (e) {
        console.error(e);
      }
    }
  
  
  
  
  




}
