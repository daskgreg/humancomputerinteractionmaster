import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelvesMobileComponent } from './shelves-mobile.component';

describe('ShelvesMobileComponent', () => {
  let component: ShelvesMobileComponent;
  let fixture: ComponentFixture<ShelvesMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelvesMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelvesMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
