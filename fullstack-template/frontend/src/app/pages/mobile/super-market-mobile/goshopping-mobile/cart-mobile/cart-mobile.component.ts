import { Component, OnInit } from '@angular/core';
  import { ShoppingCartService } from 'src/app/global/services/products/shopping/cart.service';
  
@Component({
  selector: 'ami-fullstack-cart-mobile',
  templateUrl: './cart-mobile.component.html',
  styleUrls: ['./cart-mobile.component.scss']
})
export class CartMobileComponent implements OnInit {
  
 public show: boolean = false;
    public cartProducts$ = [];
  
    constructor(
      private cartService: ShoppingCartService
    ) { }
  
    ngOnInit() {
      this.getAll();
    }
  
    async getAll() {
      try {
        var d: any = await this.cartService
          .getProducts()
          .toPromise();
  
        this.show = true;
        this.cartProducts$ = d.data;
  
      } catch (e) {
        console.error(e);
      }
    }
  
  }
  