import { Component, OnInit, ViewEncapsulation, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { Router } from '@angular/router';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { Product } from 'src/app/global/models/products/product.model';
import { ShoppingCartService } from 'src/app/global/services/products/shopping/cart.service';

@Component({
  selector: 'ami-fullstack-goshopping-mobile',
  templateUrl: './goshopping-mobile.component.html',
  styleUrls: ['./goshopping-mobile.component.scss']
})
export class GoshoppingMobileComponent implements OnInit {
    private _counter = 1;
    private rows = 0;
  
    public goShoppingProducts$;
    public showGoShoppingList = false;
  
    constructor(private router: Router,
      private shoppingListService: ShoppingListProductService,
      private cartService: ShoppingCartService
    ) { }
  
  
    ngOnInit() {
      this.fetchShoppingList();
    }
    redirect(target: String) {
      console.log(target);
      this.router.navigate([target]);
    }
  
    openSearchBar() {
      var searchBoxMini = document.getElementById("search-box-mini");
      var searchBoxOpen = document.getElementById("search-box-open");
  
      if (searchBoxMini.style.display === "block") {
        searchBoxMini.style.display = "none";
        searchBoxOpen.style.display = "block";
      } else {
        searchBoxMini.style.display = "none";
        searchBoxOpen.style.display = "block";
      }
    }
  
    closeSearchBar() {
      var searchBoxMini = document.getElementById("search-box-mini");
      var searchBoxOpen = document.getElementById("search-box-open");
  
      if (searchBoxOpen.style.display === "block") {
        searchBoxOpen.style.display = "none";
        searchBoxMini.style.display = "block";
      } else {
        searchBoxOpen.style.display = "none";
        searchBoxMini.style.display = "block";
        alert("skata");
      }
    }
  
    drawGoShopping() {
      this.fetchShoppingList();
    }
  
    public async fetchShoppingList() {
      try {
        var shoppingList: any = await this.shoppingListService
          .getAllProductNames()
          .toPromise();
  
        if (!shoppingList) {
          console.log("ERROR - fetchShoppingList - shoppingList is null");
          return;
        }
  
        // this.dynamicCSSPrep(shoppingList);
  
  
        this.showGoShoppingList = true;
        this.goShoppingProducts$ = shoppingList.data;
  
      } catch (e) {
        console.error(e);
      }
    }
  
    async addToCart(prod) {
      for (var i = 0; i < this.goShoppingProducts$.length; i++) {
        if (this.goShoppingProducts$[i].product_details.id == prod.product_details.id) {
          this.goShoppingProducts$.splice(i, 1);
        }
      }
      console.log(this.goShoppingProducts$);
      try {
        var response = this.cartService
          .addToCart(prod.product_details.id, prod.product_details.name)
          .toPromise();
      } catch (e) {
        console.error(e);
      }
    }
  
  }
  

