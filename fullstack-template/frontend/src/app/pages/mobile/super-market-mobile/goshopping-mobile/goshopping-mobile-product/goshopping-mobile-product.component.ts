import { Component, OnInit, Input } from '@angular/core';
import { ShoppingListProduct } from 'src/app/global/models/products/shopping-list-product.model';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-goshopping-mobile-product',
  templateUrl: './goshopping-mobile-product.component.html',
  styleUrls: ['./goshopping-mobile-product.component.scss']
})
export class GoshoppingMobileProductComponent implements OnInit {

  @Input() product : ShoppingListProduct;


  constructor(private router: Router,
    private shoppingListProduct : ShoppingListProductService,
    private http : HttpClient) { 
      
    }

  ngOnInit() {
    console.log(this.shoppingListProduct);
  }

 public prodDetails = false;

 openProd(s){
   
    if(this.prodDetails || s === 'close'){
      this.prodDetails = false;
      return;
    }
    this.prodDetails = true;
 }
 

  public async deleteProduct(){
    try{

      var productID = this.product.product_details.id;
      console.log(productID);

      var deleteTheProduct: any = await this.shoppingListProduct
      .removeProductFromShoppingList(productID)
      .toPromise();

    }catch(e){
      console.error(e);
    }
  }
}
