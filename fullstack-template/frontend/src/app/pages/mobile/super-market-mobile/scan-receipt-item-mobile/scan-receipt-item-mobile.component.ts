
import { Component, OnInit, Input } from '@angular/core';
import { ShoppingListProduct } from 'src/app/global/models/products/shopping-list-product.model';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-scan-receipt-item-mobile',
  templateUrl: './scan-receipt-item-mobile.component.html',
  styleUrls: ['./scan-receipt-item-mobile.component.scss']
})
export class ScanReceiptItemMobileComponent implements OnInit {
  @Input() product : ShoppingListProduct;


  constructor(private router: Router,
    private shoppingListProduct : ShoppingListProductService,
    private http : HttpClient) { 
      
    }

  ngOnInit() {
    console.log(this.shoppingListProduct);
  }

 public prodDetails = false;

 openProd(s){
   
    if(this.prodDetails || s === 'close'){
      this.prodDetails = false;
      return;
    }
    this.prodDetails = true;
 }
 

  public async deleteProduct(){
    try{

      var productID = this.product.product_details.id;
      console.log(productID);

      var deleteTheProduct: any = await this.shoppingListProduct
      .removeProductFromShoppingList(productID)
      .toPromise();

    }catch(e){
      console.error(e);
    }
  }

}
