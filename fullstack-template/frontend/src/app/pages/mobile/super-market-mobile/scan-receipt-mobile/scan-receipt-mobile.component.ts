
import { Component, OnInit, ViewChild, HostListener, ViewEncapsulation } from '@angular/core';
import { QrScannerComponent } from 'angular2-qrscanner';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { ReceiptProductsService } from 'src/app/global/services/products/receipt-products/receipt-products.service';
import { ReceiptProduct } from 'src/app/global/models/products/receipt-product.model';
import { Classification } from 'src/app/global/models/classification/classification.model';
import { SuperMarketSingletonService } from 'src/app/global/services/frontend-services/super-market-singleton/super-market-singleton.service';
import * as Globals from '../../../../global/variables/variables';
import { SendReceiptIdToScannedProducts } from 'src/app/global/services/frontend-services/send-receipt-id-to-scanned-products/send-receipt-id-to-scanned-products.service';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { PoorMansPushNotificationComponent } from 'src/app/pages/poor-mans-push-notification/poor-mans-push-notification.component';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';

@Component({
  selector: 'ami-fullstack-scan-receipt-mobile',
  templateUrl: './scan-receipt-mobile.component.html',
  styleUrls: ['./scan-receipt-mobile.component.scss']
})
export class ScanReceiptMobileComponent implements OnInit {
  public rsp;
  public receiptValid: boolean = true;
  constructor(private router: Router,
    private http: HttpClient,
    private receiptProd: ReceiptProductsService,
    private receiptIdSingleton: SendReceiptIdToScannedProducts,
    private push: PoorMansPushNotificationComponent,
    private notification: NotificationsService) { }

  @ViewChild(QrScannerComponent, null) qrScannerComponent: QrScannerComponent;

  ngOnInit() {
    this.qrScannerComponent.getMediaDevices().then(devices => {
      console.log(devices);
      const videoDevices: MediaDeviceInfo[] = [];
      for (const device of devices) {
        if (device.kind.toString() === 'videoinput') {
          videoDevices.push(device);
        }
      }
      if (videoDevices.length > 0) {
        let choosenDev;
        for (const dev of videoDevices) {
          if (dev.label.includes('front')) {
            choosenDev = dev;
            break;
          }
        }
        if (choosenDev) {
          this.qrScannerComponent.chooseCamera.next(choosenDev);
        } else {
          this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
        }
      }
    });

    this.qrScannerComponent.capturedQr.subscribe(result => {
      this.checkIfReceiptValid(result);

      // this.receiptIdSingleton.updateStatus(result);
      // this.receiptIdSingleton.sentReceiptId.emit(result);

      // // this.getReceiptAfterScan(result);
      // this.redirect('scanned-receipt-products');
    });
  }

  public async getReceiptAfterScan(result) {
    console.log("async");

    try {
      var receipt = await this.receiptProd
        .getReceipt(result)
        .toPromise();

      if (!receipt) {
        console.log("NO receipts found!");
      }
      console.log(receipt);
      // localStorage.setItem('scannedReceipt', JSON.stringify(receipt));
      this.redirect('print-receipt-mobile');
    } catch (e) {
      console.error(e);
    }
  }

  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }

  bypassQR() {
    var receipt_id = (document.getElementById('qr_bypass') as HTMLInputElement).value.toString();

    this.checkIfReceiptValid(receipt_id);
  }

  async checkIfReceiptValid(receipt_id) {
    try {
      var check: any = await this.receiptProd
        .getReceipt(receipt_id)
        .toPromise();

      if (check.success == false) {
        var push: PushNotification = {
          message: "No receipt found!",
          source: "Super Market"
        }

        this.push.generateNotification(push, false);

        var n: SIMNotification = new SIMNotification();
        n.message = "No receipt found!";
        n.source = "Super Market";
        n.severity = "low";

        this.notification.createNotification(n);
        this.receiptValid = false;
        return;
      }
      this.receiptIdSingleton.updateStatus(receipt_id);
      this.receiptIdSingleton.sentReceiptId.emit(receipt_id);

      this.redirect('print-receipt-mobile');

      localStorage.setItem("scan", receipt_id);
    } catch (e) {
      console.error(e);
    }
  }

}
