import { Component, OnInit, ViewEncapsulation, HostListener, Directive } from '@angular/core';
import { CustomService } from 'src/app/global/services';
import { ReceiptProductsService } from 'src/app/global/services/products/receipt-products/receipt-products.service';
import { Router } from "@angular/router";
import * as Globals from '../../../../global/variables/variables';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { PoorMansPushNotificationComponent } from '../../../poor-mans-push-notification/poor-mans-push-notification.component';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { NotificationsComponent } from '../../../tablet/notifications/notifications.component';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { SendReceiptIdToScannedProducts } from 'src/app/global/services/frontend-services/send-receipt-id-to-scanned-products/send-receipt-id-to-scanned-products.service';
import { subscribeOn } from 'rxjs/operators';

@Component({
  selector: 'ami-fullstack-print-receipt-mobile',
  templateUrl: './print-receipt-mobile.component.html',
  styleUrls: ['./print-receipt-mobile.component.scss']
})
export class PrintReceiptMobileComponent implements OnInit {


  public whereToSearch = this.receipt;
  public action = this.addProduct(this);
  public showItems = false;
  public showSearch = false;

  public scannedProducts$ = [];

  public receipt_id;

  constructor(private custom: CustomService,
    private push: PoorMansPushNotificationComponent,
    private notification: NotificationsService,
    private router: Router,
    private receipt: ReceiptProductsService,
    private receiptIdSingleton: SendReceiptIdToScannedProducts) {

    // this.receiptIdSingleton.sentReceiptId.subscribe(
    //   (data: string) => {
    //     if (data != "") {
    //       console.log(data);
    //       this.getScannedProducts(data);
    //       this.receipt_id = data;
    //     }
    //   }
    // )

    if (localStorage.getItem("scan")) {
      this.getScannedProducts(localStorage.getItem("scan"));
      localStorage.removeItem("scan");
    } else {
      this.router.navigate(['super-market-mobile']);
    }
  }

  redirect(target: String) {
    // console.log(target);
    this.router.navigate([target]);
  }

  async getScannedProducts(rec) {
    if (!rec) {
      var push: PushNotification = {
        message: "No receipt scanned!",
        source: "Scanned receipt products"
      }

      this.push.generateNotification(push, false);

      var n: SIMNotification = new SIMNotification();
      n.message = "No receipt scanned!";
      n.source = "Scanned receipt products";
      n.severity = "low";

      this.notification.createNotification(n);

      this.custom.redirect('super-market-mobile');
      return;
    }

    try {
      var scannedProductsArray: any = await this.receipt
        .getProductsFromReceiptWithID(rec)
        .toPromise();
      console.log("MPHKA");
      console.log(scannedProductsArray);

      if (!scannedProductsArray) {
        console.log("ERROR - scannedProductsArray - scannedProductsArray is null");
        return;
      }

      if (!scannedProductsArray.success) {
        console.log("ERROR - scannedProductsArray - scannedProductsArray failed");
        return;
      }

      // scannedProductsArray.data[0].products.forEach(element => {
      //   var id = element.product_details.prod_id;
      //   this.getProductWithIdFromReceipt(id, this.receipt_id);
      // });

      // this.showScannedProductsList = true;
      this.scannedProducts$ = scannedProductsArray.data[0].products;



    } catch (e) {
      console.error(e);
    }

  }

  public async getProductWithIdFromReceipt(prod_id, rec_id) {
    try {
      var product: any = await this.receipt
        .isPartOfReceiptWithId(prod_id, rec_id)
        .toPromise();


      if (!product) {
        console.log("ERROR - getProductWithIdFromReceipt - product is null");
        return;
      }


    } catch (e) {
      console.error(e);
    }
  }



  ngOnInit() {
  }


  searchController() {
    this.showSearch = !this.showSearch;
  }

  removeProduct(element) {
    console.log(element.getAttribute("product_id"));

  }

  addProduct(element) {
    //if the user adds a product, we take as a fact tha the product
    //already existed in the household, so we just refill
    //new field in model with vlaue before refill, so the user can undo it(??)

    //Future Work: add a products that came into the household for the fist time

  }

  openSearchBar() {
    this.showItems = true;
    this.showSearch = true;
  }

  endClassification() {
    console.log("Before: " + Globals.classifyingProducts.receipt_id + " " + Globals.classifyingProducts.status);
    Globals.classifyingProducts.receipt_id = "";
    Globals.classifyingProducts.status = false;
    console.log("After: " + Globals.classifyingProducts.receipt_id + " " + Globals.classifyingProducts.status);
  }

}
