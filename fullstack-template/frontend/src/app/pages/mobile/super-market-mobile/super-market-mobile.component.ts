import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-super-market-mobile',
  templateUrl: './super-market-mobile.component.html',
  styleUrls: ['./super-market-mobile.component.scss']
})
export class SuperMarketMobileComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirect(target:String){
    console.log(target);
    this.router.navigate([target]);
  }

}
