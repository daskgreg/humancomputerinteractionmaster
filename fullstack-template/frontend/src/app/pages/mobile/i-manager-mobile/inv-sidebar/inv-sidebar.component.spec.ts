import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvSidebarComponent } from './inv-sidebar.component';

describe('InvSidebarComponent', () => {
  let component: InvSidebarComponent;
  let fixture: ComponentFixture<InvSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
