
import { Component, OnInit, ComponentRef, ViewChild, ViewContainerRef, ComponentFactoryResolver, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ViewEncapsulation } from '@angular/core';
import { ProductsService } from 'src/app/global/services/products/products/products.service';
import { PushNotificationService } from 'src/app/global/services/push-notification/push-notification.service';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { PoorMansPushNotificationComponent } from '../../poor-mans-push-notification/poor-mans-push-notification.component';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Product } from 'src/app/global/models/products/product.model';
import { RunningOutProductsService } from 'src/app/global/services/products/running-out-products/running-out-products.service';
import { GetProductDetails } from 'src/app/global/services/frontend-services/get-product-details/GetProductDetails';
import { CustomService } from 'src/app/global/services';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ami-fullstack-i-manager-mobile',
  templateUrl: './i-manager-mobile.component.html',
  styleUrls: ['./i-manager-mobile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IManagerMobileComponent implements OnInit {

  customOptions: any = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {

      740: {
        items: 3
      }
    },
    nav: false
  }

  public productPosition = false;
  public hideIcon = true;
  public availabilityPercentage = false;


  public showProductDetails = false;
  public detailedProduct: Product;
  public detailedProductImg;
  public detailedProductMoreData = {
    exp_month: "",
    exp_day: "",
    ammount_remaining: "",
    ammount_unit: ""
  };


  public showProductLocation = false;

  public showTags = new Array(4);
  public tagsRemaining = "+";

  public availabilityImg = new Array(5);
  public availabilityValue;

  public hideIfRecurring = false;

  public tileData$ = new Array(5);
  public showTile = new Array(5);
  public showCarousel = true;

  public tileProductsQuantity = new Array(5);

  public allProductsArray$;


  public tiles = [];


  //Search Initializations
  public search: any = '';
  locked: any = [];


  constructor(private productService: ProductsService,
    private sendDetails: GetProductDetails,
    private custom: CustomService,
    private dom: DomSanitizer,
    private notificationObj: PoorMansPushNotificationComponent
  ) {

    this.sendDetails.haveData.subscribe(
      (data: any) => {
        if (!data.changed) return;
        // console.log(data);
        this.getProductDetails(data.product);
        //reset
        var d = {
          changed: false,
          product: null
        }
        this.sendDetails.updateStatus(d);
        this.sendDetails.haveData.emit(d)
      }
    )
  }

  ngOnInit() {
    this.initTiles();
    this.initTagBooleans();
  }

  //UNSUB - CHECK
  ngOnDestroy() {
    this.sendDetails.haveData;
  }

  initTiles() {
    this.tiles[0] = (document.getElementById('inv-manager-running-out') as HTMLDivElement);
    this.tiles[1] = (document.getElementById('inv-manager-empty') as HTMLDivElement);
    this.tiles[2] = (document.getElementById('inv-manager-all') as HTMLDivElement);
    this.tiles[3] = (document.getElementById('inv-manager-full') as HTMLDivElement);
    this.tiles[4] = (document.getElementById('inv-manager-wasted') as HTMLDivElement);


    this.initPoorMansCarouselClasses();

    this.getAllProducts("");
    this.getEmptyProducts("");
    this.getFullProducts("");
    this.getWastedProducts("");
    this.getRunningOutProducts("");

    for (var i = 0; i < this.showTile.length; i++) {
      this.showTile[i] = false;
    }
  }

  initTagBooleans() {
    for (var i = 0; i < this.showTags.length; i++) {
      this.showTags[i] = false;
    }
  }

  initImgBooleans() {
    for (var i = 0; i < this.availabilityImg.length; i++) {
      this.availabilityImg[i] = false;
    }
  }

  initPoorMansCarouselClasses() {
    for (var i = 0; i < 5; i++) {
      this.tiles[i].classList.add('position' + i.toString());
    }
  }

  moveLeft() {
    var currClass;

    for (var i = 0; i < 5; i++) {
      var elemClasses = this.tiles[i].classList.toString();
      var classArray = elemClasses.split(" ");

      for (var j = 0; j < classArray.length; j++) {
        if (classArray[j].includes('position')) {
          currClass = parseInt(classArray[j].replace("position", ""));
        }
      }

      this.tiles[i].classList.remove("position" + currClass.toString());

      var class2add = "position" + (currClass + 1).toString();
      if (class2add === "position5") {
        class2add = "position0";
      }

      this.tiles[i].classList.add(class2add);
    }
  }

  moveRight() {
    var currClass;

    for (var i = 0; i < 5; i++) {
      var elemClasses = this.tiles[i].classList.toString();
      var classArray = elemClasses.split(" ");

      for (var j = 0; j < classArray.length; j++) {
        if (classArray[j].includes('position')) {
          currClass = parseInt(classArray[j].replace("position", ""));
        }
      }
      //ANIMATION
      //Phgaine to class 2 sthn thesi tou class 1 to class 0 sthn thesh tou 4 etc
      this.tiles[i].classList.remove("position" + currClass.toString());

      var class2add = "position" + (currClass - 1).toString();
      if (class2add === "position-1") {
        class2add = "position4";
      }

      this.tiles[i].classList.add(class2add);
    }
  }

  animate(event) {
    event.target.previousSibling.setAttribute('class', 'animation');
    setTimeout(() => {
      event.target.previousSibling.removeAttribute('class')
    }, 2000)
  }

  onSwipe(evt) {
    var x;
    var deltaX = Math.abs(evt.deltaX);

    this.tiles[1].style.position = 'absolute';
    this.tiles[1].style.left = deltaX;

    if (deltaX > 40) {
      if (evt.deltaX > 0) {
        x = this.moveLeft();
      } else {
        x = this.moveRight();
      }
    }

    // this.eventText += `${x} ${y}<br/>`;
  }

  public async getRunningOutProducts(category: string) {
    var container_id = 0;
    try {
      var runningOutArray: any;

      if (category === "") {
        runningOutArray = await this.productService
          .getAlmostRanOutProducts()
          .toPromise();
      } else {
        runningOutArray = await this.productService
          .getAlmostRanOutProductsByCategory(category)
          .toPromise();
      }

      this.showTile[container_id] = true;
      this.tileData$[container_id] = runningOutArray.data;
      this.allProductsArray$ = runningOutArray.data;

      this.tileProductsQuantity[container_id] = runningOutArray.data.length;

    } catch (e) {
      console.error(e);
    }
  }

  public async getEmptyProducts(category: string) {
    var container_id = 1;
    try {
      var emptyProductsArray: any;

      if (category === "") {
        emptyProductsArray = await this.productService
          .getEmptyProducts()
          .toPromise();
      } else {
        emptyProductsArray = await this.productService
          .getEmptyProductsByCategory(category)
          .toPromise();
      }

      this.showTile[container_id] = true;
      this.tileData$[container_id] = emptyProductsArray.data;

      this.tileProductsQuantity[container_id] = emptyProductsArray.data.length;

    } catch (e) {
      console.error(e);
    }
  }

  public async getFullProducts(category: string) {
    var container_id = 3;
    try {
      var fullProductsArray: any
      if (category == "") {
        fullProductsArray = await this.productService
          .getFullProducts()
          .toPromise();
      } else {
        fullProductsArray = await this.productService
          .getFullProductsByCategory(category)
          .toPromise();
      }

      this.showTile[container_id] = true;
      this.tileData$[container_id] = fullProductsArray.data;

      this.tileProductsQuantity[container_id] = fullProductsArray.data.length;

    } catch (e) {
      console.error(e);
    }
  }

  public async getWastedProducts(category: string) {
    var container_id = 4;
    try {
      var wastedProductsArray: any;
      if (category === "") {
        wastedProductsArray = await this.productService
          .getExpiredProducts()
          .toPromise();
      } else {
        wastedProductsArray = await this.productService
          .getExpiredProductsByCategory(category)
          .toPromise();
      }

      this.showTile[container_id] = true;
      this.tileData$[container_id] = wastedProductsArray.data;

      this.tileProductsQuantity[container_id] = wastedProductsArray.data.length;

    } catch (e) {
      console.error(e);
    }
  }

  public async getAllProducts(category: string) {
    var container_id = 2;
    try {
      var allProds: any;

      if (category === "") {
        allProds = await this.productService
          .getAllProducts()
          .toPromise();
      } else {
        allProds = await this.productService
          .getAllProductsByCategory(category)
          .toPromise();
      }

      this.showTile[container_id] = true;
      this.tileData$[container_id] = allProds.data;

      this.tileProductsQuantity[container_id] = allProds.data.length;

    } catch (e) {
      console.error(e);
    }
  }

  getProductDetails(product) {
    this.detailedProduct = product;
    this.hideIfRecurring = false;
    //HOW TO ADD PICS FROM DB
    var imgData = product.image.image_data.data;
    var temp = this.custom._arrayBufferToBase64(imgData);
    var t = "data:" + product.image.image_type + "; base64, " + temp;
    this.detailedProductImg = this.dom.bypassSecurityTrustUrl(t);

    //date
    var options = { month: 'long' };
    var date = new Date(product.details.exp_date);

    this.detailedProductMoreData.exp_month = new Intl.DateTimeFormat('en-US', options).format(date).toString();
    this.detailedProductMoreData.exp_day = date.getDay().toString();
    this.detailedProductMoreData.ammount_remaining = product.details.ammount_remaining.toString();
    this.detailedProductMoreData.ammount_unit = product.details.ammount_unit.toString();

    //tags
    console.log(product.details.tags);

    var tagsArray = product.details.tags;
    if (tagsArray.length > 0) {
      if (tagsArray.length >= 3) {
        this.showTags[1] = true;
        this.showTags[2] = true;
        this.showTags[3] = true;

        var tmp = tagsArray.length - 2;
        this.tagsRemaining = "+" + tmp.toString();

      } else if (tagsArray.length >= 2) {
        this.showTags[1] = true;
        this.showTags[2] = true;

      } else {
        this.showTags[1] = true;

      }

      this.showTags[0] = true;
    }

    //ammount
    var ammount = product.details.ammount_remaining;
    var full = product.details.full_ammount_potential;
    var rate = (ammount / full) * 100;

    var imgSrc;

    if (rate == 0) {
      imgSrc = "assets/inv-manager-ammount/ammount/empty.svg";
    } else if (rate > 0 && rate <= 20) {
      imgSrc = "assets/inv-manager-ammount/ammount/running-out.svg";
    } else if (rate > 20 && rate <= 60) {
      imgSrc = "assets/inv-manager-ammount/ammount/half.svg";
    } else if (rate > 60 && rate <= 75) {
      imgSrc = "assets/inv-manager-ammount/ammount/70percentange.svg";
    } else {
      imgSrc = "assets/inv-manager-ammount/ammount/full-product.svg";
    }

    this.availabilityImg = imgSrc;
    this.availabilityValue = rate.toString() + "%";

    //recurring
    var recurring: boolean = product.is_recurring;
    if (recurring) {
      this.hideIfRecurring = true;
    } else {
      this.hideIfRecurring = false;
    }

    this.showProductDetails = true;
  }

  closeProdDetails() {
    this.showProductDetails = false;
  }

  clearProducts() {
    for (var i = 0; i < this.showTile.length; i++) {
      this.showTile[i] = false;
    }

    for (var i = 0; i < this.tileData$.length; i++) {
      this.tileData$[i] = null;
    }
  }

  filterClicked(category) {
    // this.clearProducts();

    if (category == "all") {
      this.getAllProducts("");
      this.getEmptyProducts("");
      this.getFullProducts("");
      this.getWastedProducts("");
      this.getRunningOutProducts("");
      return;
    }

    this.getAllProducts(category);
    this.getEmptyProducts(category);
    this.getFullProducts(category);
    this.getWastedProducts(category);
    this.getRunningOutProducts(category);
  }
  //------------------------------------------
  fridgePosition() {
    this.productPosition = !this.productPosition;
    this.showProductDetails = false;
  }

  hideNseek() {
    this.hideIcon = false;
    this.availabilityPercentage = true;
  }

  hideNseek2() {
    this.hideIcon = true;
    this.availabilityPercentage = false;
  }

  showProductDetailsWhileEmpty() {
    this.showProductDetails = !this.showProductDetails;
  }

  quantity: number = 1;
  i = 1
  plus() {
    if (this.i != 10) {
      this.i++;
      this.quantity = this.i;
    }
  }

  minus() {
    if (this.i != 1) {
      this.i--;
      this.quantity = this.i;
    }
  }

  push() {
    var n: PushNotification = {
      source: "NO LOCATION FOUND",
      message: "ITEM HAS RUNOUT",
    }
    this.notificationObj.generateNotification(n, false);
  }

  pushAccept() {
    var n: PushNotification = {
      source: "Product has successfully added to shopping list",
      message: "2kg",
    }
    this.notificationObj.generateNotification(n, false);
    this.showProductDetails = false;
  }

  flipIt() {
    if ((document.getElementsByClassName('flip-container')[0] as HTMLDivElement).classList.contains('flip-container-hover')) {
      (document.getElementsByClassName('flip-container')[0] as HTMLDivElement).classList.remove('flip-container-hover');
      (document.getElementsByClassName('flipper')[0] as HTMLDivElement).classList.remove('flip-container-hover');
      (document.getElementById('exp-back') as HTMLDivElement).classList.add('back');
      (document.getElementById('exp-back') as HTMLDivElement).classList.add('hidden');
      // (document.getElementById('exp-back') as HTMLDivElement).classList.add('fade-out-exp')
    } else {
      (document.getElementsByClassName('flip-container')[0] as HTMLDivElement).classList.add('flip-container-hover');
      (document.getElementsByClassName('flipper')[0] as HTMLDivElement).classList.add('flip-container-hover');
      (document.getElementById('exp-back') as HTMLDivElement).classList.remove('hidden');
      (document.getElementById('exp-back') as HTMLDivElement).classList.remove('back');
      (document.getElementById('exp-back') as HTMLDivElement).classList.add('fade-in-exp');
    }
  }

  getInvManLocation() {
    console.log(this.detailedProduct.location.general_storage_location);
    console.log(this.detailedProduct.location.inside_storage_location);

    this.showProductLocation = !this.showProductLocation;
  }

}
