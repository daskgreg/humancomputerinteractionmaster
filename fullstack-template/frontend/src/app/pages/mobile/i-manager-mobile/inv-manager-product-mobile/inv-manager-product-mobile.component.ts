import { Component, OnInit, Input, ComponentRef, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { Product } from 'src/app/global/models/products/product.model';
import { SendReceiptIdToScannedProducts } from 'src/app/global/services/frontend-services/send-receipt-id-to-scanned-products/send-receipt-id-to-scanned-products.service';
import { GetProductDetails } from 'src/app/global/services/frontend-services/get-product-details/GetProductDetails';
import { CustomService } from 'src/app/global/services';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ami-fullstack-inv-manager-product-mobile',
  templateUrl: './inv-manager-product-mobile.component.html',
  styleUrls: ['./inv-manager-product-mobile.component.scss']
})
export class InvManagerProductMobileComponent implements OnInit {


  @Input() product: any;

  public showProductDetails = false;
  public prodImg;

  constructor(private sendDetails: GetProductDetails,
    private custom: CustomService,
    private dom: DomSanitizer) { }

  ngOnInit() {
    var product_name = this.product.product_details.name;
    var imgSrc = this.product.image.image_data.data;
    var imgD = this.custom._arrayBufferToBase64(imgSrc);
    var imgType = this.product.image.image_type;
    var t = "data: " + imgType + ";base64, " + imgD;
    this.prodImg = this.dom.bypassSecurityTrustUrl(t);
  }

  detailsMenu() {
    var data = {
      changed: true,
      product: this.product
    }
    this.sendDetails.updateStatus(data);
    this.sendDetails.haveData.emit(data)
  }

}
