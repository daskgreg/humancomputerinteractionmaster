import { Component, OnInit, Input, ComponentFactoryResolver } from '@angular/core';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'ami-fullstack-notifications-item-mobile',
  templateUrl: './notifications-item-mobile.component.html',
  styleUrls: ['./notifications-item-mobile.component.scss']
})
export class NotificationsItemMobileComponent implements OnInit {

  @Input() index : number;
  @Input() notification : SIMNotification;//object
  @Input() styles : any;

  
  constructor() { }

  ngOnInit() {
  }


}
