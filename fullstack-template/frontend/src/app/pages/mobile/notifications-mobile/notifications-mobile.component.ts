import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { CustomService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-notifications-mobile',
  templateUrl: './notifications-mobile.component.html',
  styleUrls: ['./notifications-mobile.component.scss']
})
export class NotificationsMobileComponent implements OnInit {

  constructor(private notification : NotificationsService,
    private custom : CustomService) {}

  ngOnInit() {}

redirect(target){
  this.custom.redirect(target);
}

}
