import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-dashboard-phone',
  templateUrl: './dashboard-phone.component.html',
  styleUrls: ['./dashboard-phone.component.scss']
})
export class DashboardPhoneComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirect(target:String){
    console.log(target);
    this.router.navigate([target]);
  }
}
