import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwaitingMobileComponent } from './awaiting-mobile.component';

describe('AwaitingMobileComponent', () => {
  let component: AwaitingMobileComponent;
  let fixture: ComponentFixture<AwaitingMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwaitingMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwaitingMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
