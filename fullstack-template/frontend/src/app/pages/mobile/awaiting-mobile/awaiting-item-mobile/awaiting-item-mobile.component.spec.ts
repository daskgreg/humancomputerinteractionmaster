import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwaitingItemMobileComponent } from './awaiting-item-mobile.component';

describe('AwaitingItemMobileComponent', () => {
  let component: AwaitingItemMobileComponent;
  let fixture: ComponentFixture<AwaitingItemMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwaitingItemMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwaitingItemMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
