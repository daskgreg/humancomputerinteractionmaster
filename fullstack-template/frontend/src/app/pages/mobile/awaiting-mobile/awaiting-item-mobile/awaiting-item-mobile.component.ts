import { Component, OnInit, Input } from '@angular/core';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { RunningOutProductsService } from 'src/app/global/services/products/running-out-products/running-out-products.service';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-awaiting-item-mobile',
  templateUrl: './awaiting-item-mobile.component.html',
  styleUrls: ['./awaiting-item-mobile.component.scss']
})
export class AwaitingItemMobileComponent implements OnInit {

  @Input() product;

  constructor(
    private shoppingListService: ShoppingListProductService,
    private awaitingService: RunningOutProductsService,
    private socketsService: SocketsService
  ) { }

  ngOnInit() {
  }

  async accept() {
    try {
      var prod = {
        product_details: {
          id: this.product.product_details.id,
          name: this.product.product_details.name
        },
        details: {
          ammount_to_buy: 0,
          category: this.product.details.category,
          ammount_unit: this.product.details.ammount_unit,
          tags: this.product.details.tags
        },
        image: {
          image_type: this.product.image.image_type,
          image_data: this.product.image.image_data
        }
      }

      await this.shoppingListService
        .addProductToShoppingList(prod)
        .toPromise();

      this.removeFromAwaiting();
    } catch (e) {
      console.error(e);
    }
  }

  async reject() {
    this.removeFromAwaiting();
  }



  async removeFromAwaiting() {

    try {
      await this.awaitingService
        .removeProductFromRunningOutList(this.product.product_details.id)
        .toPromise();

      var event = "removed-from-awaiting"
      await this.socketsService
        .sendSocketRequest(event, null)
        .toPromise();
    } catch (e) {
      console.error(e);
    }
  }

}
