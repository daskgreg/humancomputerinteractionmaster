import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ami-fullstack-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.scss']
})
export class MenubarComponent implements OnInit {
  public showMenu = false;
  public showFilter = false;
  public addProduct = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }

  openMenu() {
    this.showMenu = !this.showMenu;
  }
  openFilter() {
    this.showFilter = !this.showFilter;
  }
  addProductF() {
    this.addProduct = !this.addProduct;
  }

  setPageLocation() {
    var fullURl = this.router.url.toString();
    fullURl = fullURl.replace("-", " ");
    fullURl = fullURl.replace("/", "");
    var length = fullURl.length;
    for (var i = 0; i < length; i++) {
      if (i == 0) {
        var capital = fullURl[i].toUpperCase();
        fullURl = capital + fullURl.substr(1, fullURl.length)
      }
      if (fullURl[i] == "-") {
        fullURl = fullURl.replace("-", " ");
      }
      if (fullURl[i] == " ") {
        var capital = " " + fullURl[i + 1].toUpperCase();
        fullURl = fullURl.substr(0, i) + capital + fullURl.substr(i + 2, fullURl.length)
      }
    }
    (document.getElementById('top-side-bar-where-am-i') as HTMLDivElement).innerText = fullURl;
  }
}
