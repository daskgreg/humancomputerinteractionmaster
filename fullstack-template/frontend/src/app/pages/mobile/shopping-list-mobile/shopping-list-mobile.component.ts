import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ProductTail } from 'src/app/global/models/greg/greg.model';
import { Product } from 'src/app/global/models/products/product.model';
import { ShoppingListProduct } from 'src/app/global/models/products/shopping-list-product.model';
import { ShoppingListProductService } from 'src/app/global/services/products/shopping-list-products/shopping-list-products.service';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'ami-fullstack-shopping-list-mobile',
  templateUrl: './shopping-list-mobile.component.html',
  styleUrls: ['./shopping-list-mobile.component.scss']
})
export class ShoppingListMobileComponent implements OnInit {
  @Input() filterProduct: ShoppingListProduct;

  constructor(private router: Router,
    private shoppingListProduct: ShoppingListProductService,
    private http: HttpClient) {
    this.fetchProducts();

  }

  ngOnInit(): void {

  }
  public showTheFilters = false;

  showFilters() {
    this.showTheFilters = !this.showTheFilters;
  }
  public searchBarOpening;

  openSearchBar() {
    this.searchBarOpening = !this.searchBarOpening;
  }

  public showForm = false;
  openTheForm() {
    this.showForm = !this.showForm;
  }
  public showfilter = true;
  public async theBestFilterEU() {
    try {
      var filterByCategory = this.filterProduct.details.category;

    } catch (e) {
      console.error(e);
    }
  }

  submit(form: NgForm) {
    this.addProduct(form);
    this.openTheForm();
  }


  public async addProduct(form: NgForm) {
    try {
      var value = form.value;
      var newProduct: ShoppingListProduct = {
        product_details: {
          id: "4500",
          name: value.productName,
        },
        details: {
          ammount_to_buy: value.productAmmount,
          category: "Coffee",
          ammount_unit: "55",
          tags: ["coffee"]

        },
        image: {
          image_data: "image",
          image_type: "image2"
        }
      };

      newProduct.product_details.id = "4500";
      newProduct.product_details.name = value.productName;
      newProduct.details.ammount_to_buy = value.productAmmount;

      var addProduct: any = await this.shoppingListProduct
        .addProductToShoppingList(newProduct)
        .toPromise();

      this.shoppingListProduct.addProductToShoppingList(newProduct);
      console.log("Product is created");

      this.fetchProducts();
      this.showSplproductList = false;
      this.showSplproductList = true;

    } catch (e) {
      console.error(e);
    }

  }

  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }

  public splistproducts$: ShoppingListProduct;
  public showSplproductList = false;

  public async fetchProducts() {
    try {

      var splproductArray: any = await this.shoppingListProduct
        .getAllProductNames()
        .toPromise();

      if (!splproductArray) {
        console.log("Error - spListProds");
        return;
      }
      console.log("products are in");
      this.showSplproductList = true;
      this.splistproducts$ = splproductArray;
      console.log("products are in1");
      return;

    } catch (e) {
      console.error(e);
    }
  }






}
