import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

import { CustomService } from '../../../../global/services/custom/custom.service';
import { environment } from '../../../../../environments/environment'
import { UserSession } from 'src/app/global/models/user/sessionUser.model';
import { UserSessionService } from 'src/app/global/services/session/user-session.service';
import { UserLogin } from 'src/app/global/models/user/loginUser.model';
@Component({
  selector: 'ami-fullstack-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPageComponent implements OnInit {

  constructor(private http: HttpClient,
    private router: Router,
    private custom: CustomService,
    private session: UserSessionService) {

    console.log("---------------");
    console.log(environment.host);
    if (localStorage.getItem("userData") != null) {
      // Redirect to dashboard if user logged in
      this.router.navigate(['dashboard-mobile']);
    }
  }


  ngOnInit(): void {

  }

  public async login() {
    console.log("async v2");
    try {
      var postData: UserLogin = {
        "email": (document.getElementById('login-username') as HTMLInputElement).value.toString(),
        "password": (document.getElementById('login-password') as HTMLInputElement).value.toString()
      }

      var result: UserSession = await this.session.loginUser(postData).toPromise();

      if (result) {
        console.log(result);
        var userData: UserSession = {
          "first_name": result.first_name,
          "last_name": result.last_name,
          "session_id": this.session.makeRandom(),
          "user_device": this.session.getUserDevice(),
          "image_type": result.image_type,
          "image_data": result.image_data
        };

        localStorage.setItem("userData", JSON.stringify(userData));

        this.router.navigate(['dashboard-mobile']);
      }
    } catch (e) {
      console.error(e);
    }
  }

  handleResponse(data) {
    if (data.status == 200) {
      console.log("static redirect");
    }
    // console.log(data);
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    const lengthOfCode = 7;

    var userData = {
      "first_name": data.first_name,
      "last_name": data.last_name,
      "image_type": data.image_type,
      "image_data": data.image_data
    };

    localStorage.setItem("userData", JSON.stringify(userData));

    this.router.navigate(['dashboard-mobile']);
  }

  handleError(err) {
    console.log(err);
  }

}
