import { Component, OnInit } from '@angular/core';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-home-devices-date-time',
  templateUrl: './home-devices-date-time.component.html',
  styleUrls: ['./home-devices-date-time.component.scss']
})
export class HomeDevicesDateTimeComponent implements OnInit {
  public today;

  public showNotifications = false;

  constructor(private socketsService: SocketsService) {
    setInterval(() => { this.today = Date.now() }, 1);

    this.socketsService.syncMessages("push-notification").subscribe((data) => {
      console.log(data);
    });

    this.socketsService.syncMessages("created-notification").subscribe((data) => {
      this.showNotifications = true;
      // data.message.notification
    });

    this.socketsService.syncMessages("read-notifications").subscribe((data) => {
      this.showNotifications = false;
      // data.message.notification
    });
  }

  ngOnInit() {
  }

}
