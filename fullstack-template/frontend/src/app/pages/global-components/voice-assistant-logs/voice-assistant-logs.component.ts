import { Component, OnInit } from '@angular/core';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-voice-assistant-logs',
  templateUrl: './voice-assistant-logs.component.html',
  styleUrls: ['./voice-assistant-logs.component.scss']
})
export class VoiceAssistantLogsComponent implements OnInit {

  public voiceAssistantArray$ = [];
  constructor(
    private socketsService: SocketsService
  ) { }

  ngOnInit() {
    this.socketsService.syncMessages("voice-assistant").subscribe((data) => {
      console.log(data);
      this.voiceAssistantArray$.push(data.message);
    });
  }

}
