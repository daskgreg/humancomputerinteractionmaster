import { Component, OnInit } from '@angular/core';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-home-devices-notif-list',
  templateUrl: './home-devices-notif-list.component.html',
  styleUrls: ['./home-devices-notif-list.component.scss']
})
export class HomeDevicesNotifListComponent implements OnInit {
  public notifications$;
  public showNotifications = false;

  constructor(
    private notificationsService: NotificationsService,
    private socketsService: SocketsService
  ) { }

  ngOnInit() {
    this.socketsService.syncMessages('created-notification').subscribe((data) => {
      if (data.message.status) {
        this.fetchNotifications();
      }
    });
  }

  async fetchNotifications() {
    try {
      var notificationsArray: any = await this.notificationsService
        .getUnreadNotifications()
        .toPromise();

      if (!notificationsArray) {
        console.log("ERROR - fetchNotifications - notificationsArray is null");
        return;
      }

      this.notifications$ = notificationsArray.data;
      this.showNotifications = true;

    } catch (e) {
      console.error(e);
    }
  }

}
