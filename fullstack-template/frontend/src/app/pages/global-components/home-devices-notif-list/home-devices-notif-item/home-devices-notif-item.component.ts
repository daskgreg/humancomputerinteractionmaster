import { Component, OnInit, Input } from '@angular/core';
import { SIMNotification } from 'src/app/global/models/notifications/notifications.model';
import { Router } from '@angular/router';
import { NotificationsService } from 'src/app/global/services/notifications/notifications.service';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-home-devices-notif-item',
  templateUrl: './home-devices-notif-item.component.html',
  styleUrls: ['./home-devices-notif-item.component.scss']
})
export class HomeDevicesNotifItemComponent implements OnInit {
  @Input() notification: SIMNotification;
  constructor(
    private router: Router,
    private notificationService: NotificationsService,
    private socketsService: SocketsService
  ) { }

  ngOnInit() {
  }

  tappedNotification() {
    var target = this.notification.source.toLowerCase();
    target = target.replace(" ", "-");

    this.readAllNotifications();

    this.router.navigate([target]);
  }

  async readAllNotifications() {
    try {
      var readAll = await this.notificationService
        .readAllNotifications()
        .toPromise();

      if (!readAll) {
        console.log("ERROR - readAllNotifications - readAll is null");
        return;
      }

      await this.socketsService
        .sendSocketRequest("read-notifications", null)
        .toPromise();

    } catch (e) {
      console.error(e);
    }
  }

}
