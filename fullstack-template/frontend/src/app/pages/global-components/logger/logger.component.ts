import { Component, OnInit } from '@angular/core';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-logger',
  templateUrl: './logger.component.html',
  styleUrls: ['./logger.component.scss']
})
export class LoggerComponent implements OnInit {
  public loggerArray$ = [];
  constructor(
    private socketsService: SocketsService
  ) { }

  ngOnInit() {
    this.socketsService.syncMessages("logger-write").subscribe((data) => {
      this.loggerArray$.push(data.message);
    });
  }

}
