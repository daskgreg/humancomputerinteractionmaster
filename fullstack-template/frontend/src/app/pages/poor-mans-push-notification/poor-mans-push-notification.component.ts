import { Component, OnInit, Injectable } from '@angular/core';
import { PushNotification } from 'src/app/global/models/notifications/push.model';
import { PushNotificationService } from 'src/app/global/services/push-notification/push-notification.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { SocketsService } from 'src/app/global/services';

@Component({
  selector: 'ami-fullstack-poor-mans-push-notification',
  templateUrl: './poor-mans-push-notification.component.html',
  styleUrls: ['./poor-mans-push-notification.component.scss']
})
@Injectable({
  providedIn: 'root'
})
export class PoorMansPushNotificationComponent implements OnInit {
  public startFade = true;
  public startAnimation = false;
  public currentState = 'initial';

  constructor(
    private socketsService: SocketsService

  ) {
    this.socketsService.syncMessages("push-notification").subscribe((data) => {
      console.log(data);
      this.generateNotification(data.message, false);
    })
  }

  public generateNotification(push: PushNotification, crossDevice: boolean) {
    if (crossDevice) this.sendPushCrossDevice(push);

    (document.getElementById('push-source') as HTMLInputElement).innerText = push.source;
    (document.getElementById('push-message') as HTMLInputElement).innerText = push.message;
    // (document.getElementById('push-notifications') as HTMLInputElement).innerText = push.notifications;
    this.fadeIn((document.getElementById('push-container') as HTMLDivElement));
    this.fadeIn((document.getElementById('warning-icon') as HTMLDivElement));


    setTimeout(function () {
      (document.getElementById('push-container') as HTMLDivElement).classList.add('hideMe');
      (document.getElementById('push-container') as HTMLDivElement).classList.remove('showMe');
    }, 3000);

  }

  fadeIn(el) {
    el.classList.add('showMe');
    el.classList.remove('hideMe');
  }

  async sendPushCrossDevice(push: PushNotification) {
    try {
      this.socketsService
        .sendSocketRequest("push-notification", push)
        .toPromise();
    } catch (e) {
      console.error(e);
    }
  }

  ngOnInit() {
  }

}
