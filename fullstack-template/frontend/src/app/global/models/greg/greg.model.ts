export class ProductTail {
    id:number;
    name:String;
    category: String;
    isRecurring: boolean;
    isFavourite: boolean;
    photoPath?: string;
    ammount: String
}