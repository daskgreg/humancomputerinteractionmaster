declare const Buffer;
export class UserSession {
    first_name:String;
    last_name:String;
    session_id:String;
    user_device:String;
    image_type: String;
    image_data:String;
      
    constructor(model?: any) {
      Object.assign(this, model);
    }
}
