export class ShoppingProduct {
    id: String;
    name: String;

    constructor(model?: any) {
        Object.assign(this, model);
    }
}