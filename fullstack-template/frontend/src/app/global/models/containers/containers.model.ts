export class Container{
    location: String;//ex. Cabbinet2
    base_id: String;//Container_7_base
    corresponding_container: String;//Container_7
    current_container: String;//any container(ex. Container_8)
    has_correct_container: Boolean;//if corresponding != current, this is false

    constructor(model?: any) {
        Object.assign(this, model);
    }
}