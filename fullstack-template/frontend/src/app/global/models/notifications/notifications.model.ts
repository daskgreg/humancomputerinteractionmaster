export class SIMNotification {
  id: String;
  index: Number;
  severity: String;
  message: String;
  source: String;
  isRead: Boolean;
  constructor(model?: any) {
    Object.assign(this, model);
  }
}
//beforemerge