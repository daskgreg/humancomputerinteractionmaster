export class PushNotification {
    message: string;
    source: string;
    constructor(model?: any) {
        Object.assign(this, model);
      }
}
