export class SIMLog {
    severity: String;
    message: String;
    source: String;
    constructor(model?: any) {
        Object.assign(this, model);
    }
}