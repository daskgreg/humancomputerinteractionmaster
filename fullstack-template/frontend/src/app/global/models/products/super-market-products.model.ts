export class SuperMarketProduct{
    product_details:{
        prod_id: String,
        name: String
    };
    details:{
        // ammount_unit: String,
        price: Number,
        ammountType: String
        category: String,
        tags: [String]
    };
   image:{
      image_type: String,
      image_data: String
   };

   constructor(model?: any) {
    Object.assign(this, model);
  }
}