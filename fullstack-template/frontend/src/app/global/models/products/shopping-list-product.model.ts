export class ShoppingListProduct {
    product_details: {
        id: String,
        name: String
    };
    details: {
        ammount_to_buy: Number,
        category: String,
        ammount_unit: String,
        tags: [String]
    };
    image: {
        image_type: String,
        image_data: String
    };

    constructor(model?: any) {
        Object.assign(this, model);
    }
}