export class ReceiptProduct {
    product_details:{
        prod_id: String,
        name: String
    };
    details:{
        ammount: Number,
        ammount_unit: String,
        price: Number,
        prod_date: String,
        exp_date: String
    };

    constructor(model?: any) {
        Object.assign(this, model);
      }
}