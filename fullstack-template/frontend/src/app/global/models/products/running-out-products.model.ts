export class RunningOutProduct{
    product_details:{
        id: String,
        name: String
    };
    details:{
        category: String,
        tags: [String]
    };
    image:{
        image_type: String,
        image_data: String
    };

    constructor(model?: any) {
        Object.assign(this, model);
      }
}