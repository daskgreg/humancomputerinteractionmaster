export class Product {
    product_details:{
        id: String,
        name: String
    };
    details:{
        category: String,
        ammount_remaining: Number,
        ammount_unit: String,
        prod_date: Date,
        exp_date: Date,
        tags: [String]
    };
    location:{
        room: String,
        general_storage: String,//
        general_storage_location: String,//upper, lower
        inside_storage_location: String,//upper part, lower part
        last_ammount: String
    };
    iot:{
        container_id: String,
        phonetic_name: String
    };
    image:{
        image_type: String,
        image_data: String
    };
    is_custom: {
        type: Boolean,
        default: false
    };
    is_favourite: {
        type:Boolean,
        default: false
    };
    is_recurring: {
        type: Boolean,
        default: false
    };
    
    constructor(model?: any) {
        Object.assign(this, model);
      }
}
