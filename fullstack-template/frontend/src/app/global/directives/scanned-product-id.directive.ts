import { Directive, ElementRef, Injectable } from '@angular/core';

@Directive({
  selector: '[ScannedProductId]'
})
@Injectable({
  providedIn: 'root'
})
export class ScannedProductIdDirective {

  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = 'yellow';
 }

}
