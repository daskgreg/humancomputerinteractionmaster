import { Directive, ElementRef, OnInit } from '@angular/core';
import { ShoppingListProductService } from '../services/products/shopping-list-products/shopping-list-products.service';
import { ProductsService } from '../services/products/products/products.service';
import { ShoppingListProduct } from '../models/products/shopping-list-product.model';

@Directive({
    selector: '[productsMenu]'
})
export class ProductsMenu implements OnInit {
    constructor(private elementRef: ElementRef,
        private shoppingList: ShoppingListProductService,
        private prod: ProductsService) {

    }

    ngOnInit() {
        //this.elementRef.nativeElement.onclick = this.addProdToShoppingList('this.elementRef.nativeElement.id');
    }

    public async addProdToShoppingList(id) {
        try {
            var prod: ProductsService;

            var findProduct = await this.prod
                .getProductById(id)
                .toPromise();

            if (!findProduct) {
                console.log("Error - addProdToShoppingList - findProduct")
                return;
            }

            var p2i: ShoppingListProduct = {
                product_details: {
                    id: "",
                    name: ""
                },
                details: {
                    ammount_to_buy: 0,
                    category: "",
                    ammount_unit: "",
                    tags: [""]
                },
                image: {
                    image_data: "",
                    image_type: ""
                }
            }

            var prod2add = await this.shoppingList
                .addProductToShoppingList(p2i)
                .toPromise();

            if (!prod2add) {
                console.log("Error - addProdToShoppingList - findProduct")
                return;
            }


        } catch (e) {
            console.error(e);
        }
    }
}