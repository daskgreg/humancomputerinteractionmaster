import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserSession } from '../../models/user/sessionUser.model';
import { ThrowStmt } from '@angular/compiler';
import { UserLogin } from '../../models/user/loginUser.model';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserSessionService {
  private hostURl : String;
  constructor(private http : HttpClient) { 
    this.hostURl = environment.host;
  }

  public loginUser(data: UserLogin) : Observable<UserSession>{
    return this.http
    .post<UserLogin>(`${this.hostURl}/api/user/login`, data)
    .pipe(map(result => new UserSession(result)));
  }

  public validateSession(){
    return localStorage.getItem("userData");
  }

  public makeRandom() {
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    const lengthOfCode = 7;
    let text= "";
    for (let i = 0; i < lengthOfCode; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
      return text;
  }

  public getUserDevice(){
  var ua = navigator.userAgent;
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua)){
      return "Mobile";
    }else if(/Chrome/i.test(ua)){
      return "Chrome";
    }else{
      return "Desktop";
    }
  }
}
