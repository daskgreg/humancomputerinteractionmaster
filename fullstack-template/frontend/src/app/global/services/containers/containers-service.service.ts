import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Container } from '../../models/containers/containers.model';

@Injectable({
  providedIn: 'root'
})
export class ContainersService {
  private hostURl : String;

  constructor(private http : HttpClient) { 
    this.hostURl = environment.host;
  }

  public deleteNotification(location : String): Observable<Container>{
    return this.http
    .get<Container>(`${this.hostURl}/api/containers/getContainersByLocation?location=${location}`)
    .pipe(map(result => new Container(result)));
  }

  public getAllContainers(): Observable<Container>{
    return this.http
    .get<Container>(`${this.hostURl}/api/containers/getAllContainers`)
    .pipe(map(result => new Container(result)));
  }

  public getBaseInfo(base_id : String): Observable<Container>{
    return this.http
    .get<Container>(`${this.hostURl}/api/containers/getBaseInfo?base_id=${base_id}`)
    .pipe(map(result => new Container(result)));
  }

  public getIncorrectlyPlaced(): Observable<Container>{
    return this.http
    .get<Container>(`${this.hostURl}/api/containers/getIncorrectlyPlaced`)
    .pipe(map(result => new Container(result)));
  }

  public setCurrentContainerOfBase(container : String, base_id : String): Observable<Container>{
    return this.http
    .get<Container>(`${this.hostURl}/api/containers/setCurrentContainerOfBase?container=${container}&base_id=${base_id}`)
    .pipe(map(result => new Container(result)));
  }

  public takeContainerFromBaseWithId(base_id : String): Observable<Container>{
    return this.http
    .get<Container>(`${this.hostURl}/api/containers/takeContainerFromBaseWithId?base_id=${base_id}`)
    .pipe(map(result => new Container(result)));
  }

  public getBaseByCorrespondingContainerId(container_id : string): Observable<Container>{
    return this.http
    .get<Container>(`${this.hostURl}/api/containers/getBaseByCorrespondingContainerId?container_id=${container_id}`)
    .pipe(map(result => new Container(result)));
  }
}
