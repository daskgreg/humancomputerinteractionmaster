import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Product } from '../../../models/products/product.model';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { ShoppingListProduct } from '../../../models/products/shopping-list-product.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListProductService {
  private hostURl: String;

  constructor(private http: HttpClient) {
    this.hostURl = environment.host;
  }

  public addProductToShoppingList(data: ShoppingListProduct): Observable<ShoppingListProduct> {
    return this.http
      .post<ShoppingListProduct>(`${this.hostURl}/api/shopping-list/addProductToShoppingList`, data)
      .pipe(map(result => new ShoppingListProduct(result)));
  }

  public removeProductFromShoppingList(prod_id: String): Observable<ShoppingListProduct> {
    return this.http
      .get<ShoppingListProduct>(`${this.hostURl}/api/shopping-list/removeProductFromShoppingList?productid=${prod_id}`)
      .pipe(map(result => new ShoppingListProduct(result)));
  }

  public checkIfProductIsInShoppingList(prod_id: String): Observable<ShoppingListProduct> {
    return this.http
      .get<ShoppingListProduct>(`${this.hostURl}/api/shopping-list/checkIfProductIsInShoppingList?productid=${prod_id}`)
      .pipe(map(result => new ShoppingListProduct(result)));
  }

  public getAllProductNames(): Observable<ShoppingListProduct> {
    return this.http
      .get<ShoppingListProduct>(`${this.hostURl}/api/shopping-list/getAllShoppingListNames`)
      .pipe(map(result => new ShoppingListProduct(result)));
  }

  public setAmmountToBuyOfProduct(id: string, ammount: number): Observable<ShoppingListProduct> {
    return this.http
      .get<ShoppingListProduct>(`${this.hostURl}/api/shopping-list/setAmmountToBuyOfProduct?productid=${id}&ammount=${ammount}`)
      .pipe(map(result => new ShoppingListProduct(result)));
  }

  public getAllProductsByCategory(category: string): Observable<ShoppingListProduct> {
    return this.http
      .get<ShoppingListProduct>(`${this.hostURl}/api/shopping-list/getAllProductsByCategory?category=${category}`)
      .pipe(map(result => new ShoppingListProduct(result)));
  }

  public emptyShoppingList(): Observable<ShoppingListProduct> {
    return this.http
      .get<ShoppingListProduct>(`${this.hostURl}/api/shopping-list/emptyShoppingList`)
      .pipe(map(result => new ShoppingListProduct(result)));
  }
}