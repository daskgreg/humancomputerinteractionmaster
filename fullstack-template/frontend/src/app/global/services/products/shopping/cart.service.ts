import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { RunningOutProduct } from 'src/app/global/models/products/running-out-products.model';
import { map } from 'rxjs/operators';
import { ShoppingProduct } from 'src/app/global/models/shopping/shopping.model';

@Injectable({
    providedIn: 'root'
})
export class ShoppingCartService {
    private hostURl: String;
    constructor(private http: HttpClient) {
        this.hostURl = environment.host;
    }

    public addToCart(prod_id: String, prod_name: String): Observable<ShoppingProduct> {
        return this.http
            .get<ShoppingProduct>(`${this.hostURl}/api/shopping-in-cart/addToCart?id=${prod_id}&name=${prod_name}`)
            .pipe(map(result => new ShoppingProduct(result)));
    }

    public getProducts(): Observable<ShoppingProduct> {
        return this.http
            .get<ShoppingProduct>(`${this.hostURl}/api/shopping-in-cart/getProducts`)
            .pipe(map(result => new ShoppingProduct(result)));
    }

}
