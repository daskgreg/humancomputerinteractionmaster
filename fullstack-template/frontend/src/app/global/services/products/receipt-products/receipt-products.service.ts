import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ReceiptProduct } from '../../../models/products/receipt-product.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReceiptProductsService {
  private hostURl: String;
  constructor(private http: HttpClient) {
    this.hostURl = environment.host;
  }

  public getReceipt(id: String): Observable<ReceiptProduct> {
    return this.http
      .get<ReceiptProduct>(`${this.hostURl}/api/virtual/getReceipt?receipt_id=${id}`)
      .pipe(map(result => new ReceiptProduct(result)));
  }

  public isPartOfReceiptWithId(prod_id: string, receipt_id: string): Observable<ReceiptProduct> {
    return this.http
      .get<ReceiptProduct>(`${this.hostURl}/api/virtual/getProductFromReceipt?product_id=${prod_id}&receipt_id=${receipt_id}`)
      .pipe(map(result => new ReceiptProduct(result)));
  }

  public getAllProductNames(): Observable<ReceiptProduct> {
    return this.http
      .get<ReceiptProduct>(`${this.hostURl}/api/virtual/getProductNamesFromShoppingList`)
      .pipe(map(result => new ReceiptProduct(result)));
  }

  public getReceiptProductById(id): Observable<ReceiptProduct> {
    return this.http
      .get<ReceiptProduct>(`${this.hostURl}/api/virtual/getReceiptProductById?id=${id}`)
      .pipe(map(result => new ReceiptProduct(result)));
  }

  public getProductsFromReceiptWithID(receipt_id: string): Observable<ReceiptProduct> {
    return this.http
      .get<ReceiptProduct>(`${this.hostURl}/api/virtual/getProductsFromReceiptWithID?receipt_id=${receipt_id}`)
      .pipe(map(result => new ReceiptProduct(result)));
  }

  public getProductsFromReceiptWithIDNoObservable(receipt_id: string) {
    return this.http
      .get<ReceiptProduct>(`${this.hostURl}/api/virtual/getProductsFromReceiptWithID?receipt_id=${receipt_id}`)
      .pipe(map(result => new ReceiptProduct(result)));
  }
}
