import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SuperMarketProduct } from 'src/app/global/models/products/super-market-products.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SuperMarketProductsService {
  private hostURl : String;
  constructor(private http : HttpClient) { 
    this.hostURl = environment.host;
  }

  public getAllProductNames(): Observable<SuperMarketProduct>{
    return this.http
    .get<SuperMarketProduct>(`${this.hostURl}/api/virtual/getSuperMarketProducts`)
    .pipe(map(result => new SuperMarketProduct(result)));
  }

  public getSuperMarketProductById(id : string): Observable<SuperMarketProduct>{
    return this.http
    .get<SuperMarketProduct>(`${this.hostURl}/api/virtual/getSuperMarketProductById?product_id=${id}`)
    .pipe(map(result => new SuperMarketProduct(result)));
  }
}
