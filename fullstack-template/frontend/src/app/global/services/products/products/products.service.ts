import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Product } from '../../../models/products/product.model';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private hostURl: String;

  constructor(private http: HttpClient) {
    this.hostURl = environment.host;
  }

  public addProduct(data: Product): Observable<Product> {
    return this.http
      .post<Product>(`${this.hostURl}/api/kitchen/addProduct`, data)
      .pipe(map(result => new Product(result)));
  }

  public getProductByPosition(position: String): Observable<Product[]> {
    return this.http
      .get<Product[]>(`${this.hostURl}/api/kitchen/getProductByPosition?productpos=${position}`)
      .pipe(map(result => _.map(result, (t) => new Product(t))));//CHECK if it is for each product or just the first
  }

  public getProductByCategory(category: String): Observable<Product[]> {
    return this.http
      .get<Product[]>(`${this.hostURl}/api/kitchen/getProductByCategory?productcat=${category}`)
      .pipe(map(result => _.map(result, (t) => new Product(t))));//CHECK if it is for each product or just the first
  }

  public getProductByTag(tag: String): Observable<Product[]> {
    return this.http
      .get<Product[]>(`${this.hostURl}/api/kitchen/getProductByTag?producttag=${tag}`)
      .pipe(map(result => _.map(result, (t) => new Product(t))));//CHECK if it is for each product or just the first
  }

  public getProductById(id: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/kitchen/getProductById?productid=${id}`)
      .pipe(map(result => new Product(result)));
  }

  public getProductByContainerId(cont_id: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/kitchen/getProductByContainerId?productcontid=${cont_id}`)
      .pipe(map(result => new Product(result)));//CHECK if it is for each product or just the first
  }

  public setFavouriteProduct(prod_id: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/setFavouriteProduct?productid=${prod_id}`)
      .pipe(map(result => new Product(result)));
  }

  public getFavouriteProducts(): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getFavouriteProducts`)
      .pipe(map(result => new Product(result)));
  }

  public removeFavouriteProduct(prod_id: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/removeFavouriteProduct?productid=${prod_id}`)
      .pipe(map(result => new Product(result)));
  }

  public setRecurringProduct(prod_id: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/setRecurringProduct?productid=${prod_id}`)
      .pipe(map(result => new Product(result)));
  }

  public getRecurringProducts(): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getRecurringProducts`)
      .pipe(map(result => new Product(result)));
  }

  public removeRecurringProduct(prod_id: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/removeRecurringProduct?productid=${prod_id}`)
      .pipe(map(result => new Product(result)));
  }

  public refillProduct(prod_id: String, action: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/refillProduct?productid=${prod_id}&action=${action}`)
      .pipe(map(result => new Product(result)));
  }

  public removeAmmountFromProduct(prod_id: String, ammount: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/removeAmmountFromProduct?productid=${prod_id}&ammount=${ammount}`)
      .pipe(map(result => new Product(result)));
  }

  public checkProductBelowPercentage(prod_id: String, percentage: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/checkProductBelowPercentage?productid=${prod_id}&percentage=${percentage}`)
      .pipe(map(result => new Product(result)));
  }

  public getAllProductNames(): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getAllProductNames`)
      .pipe(map(result => new Product(result)));
  }

  public removeProductFromProductsList(id: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/removeProductFromProductsList?product_id=${id}`)
      .pipe(map(result => new Product(result)));
  }

  public checkProductWithIdExpired(id: String): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/checkProductWithIdExpired?productid=${id}`)
      .pipe(map(result => new Product(result)));
  }

  public getExpiredProducts(): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getExpiredProducts`)
      .pipe(map(result => new Product(result)));
  }

  public getAlmostRanOutProducts(): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getAlmostRanOutProducts`)
      .pipe(map(result => new Product(result)));
  }

  public getEmptyProducts(): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getEmptyProducts`)
      .pipe(map(result => new Product(result)));
  }

  public getFullProducts(): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getFullProducts`)
      .pipe(map(result => new Product(result)));
  }

  public getAllProducts(): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getAllProducts`)
      .pipe(map(result => new Product(result)));
  }

  public getExpiredProductsByCategory(category: string): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getExpiredProductsByCategory?category=${category}`)
      .pipe(map(result => new Product(result)));
  }

  public getExpiringProducts(): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getExpiringProducts`)
      .pipe(map(result => new Product(result)));
  }

  public getAlmostRanOutProductsByCategory(category: string): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getAlmostRanOutProductsByCategory?category=${category}`)
      .pipe(map(result => new Product(result)));
  }

  public getEmptyProductsByCategory(category: string): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getEmptyProductsByCategory?category=${category}`)
      .pipe(map(result => new Product(result)));
  }

  public getFullProductsByCategory(category: string): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getFullProductsByCategory?category=${category}`)
      .pipe(map(result => new Product(result)));
  }

  public getAllProductsByCategory(category: string): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/getAllProductsByCategory?category=${category}`)
      .pipe(map(result => new Product(result)));
  }

  public setExpirationDateOfProduct(product_id: string, year: string, month: string, day: string): Observable<Product> {
    return this.http
      .get<Product>(`${this.hostURl}/api/products/setExpirationDateOfProduct?product_id=${product_id}&year=${year}&month=${month}&day=${day}`)
      .pipe(map(result => new Product(result)));
  }

  public addCustomProduct(data): Observable<any> {
    return this.http
      .post<any>(`${this.hostURl}/api/products/addCustomProduct`, data);
    // .pipe(map(result => new any(result)));
  }

  public getExpiringProductsNames(): Observable<any> {
    return this.http
      .get<any>(`${this.hostURl}/api/products/getExpiringProductsNames`);

  }
}
