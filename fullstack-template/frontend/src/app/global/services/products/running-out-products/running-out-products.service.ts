import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { RunningOutProduct } from 'src/app/global/models/products/running-out-products.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RunningOutProductsService {
  private hostURl : String;
  constructor(private http : HttpClient) {
    this.hostURl = environment.host;
   }

   public addProductToRunningOutList(data : RunningOutProduct): Observable<RunningOutProduct>{
    return this.http
    .post<RunningOutProduct>(`${this.hostURl}/api/running-out/addProductToRunningOutList`, data)
    .pipe(map(result => new RunningOutProduct(result)));
  }

  public removeProductFromRunningOutList(prod_id: String): Observable<RunningOutProduct>{
    return this.http
    .get<RunningOutProduct>(`${this.hostURl}/api/running-out/removeProductFromRunningOutList?productid=${prod_id}`)
    .pipe(map(result => new RunningOutProduct(result)));
  }

  public checkIfProductIsInRunningOutList(prod_id: String): Observable<RunningOutProduct>{
    return this.http
    .get<RunningOutProduct>(`${this.hostURl}/api/running-out/checkIfProductIsInRunningOutList?productid=${prod_id}`)
    .pipe(map(result => new RunningOutProduct(result)));
  }

  public getAllProductNames(prod_id: String): Observable<RunningOutProduct>{
    return this.http
    .get<RunningOutProduct>(`${this.hostURl}/api/running-out/getAllRunningOutListNames?productid=${prod_id}`)
    .pipe(map(result => new RunningOutProduct(result)));
  }
  
  public getAllRunningOutProducts(): Observable<RunningOutProduct>{
    return this.http
    .get<RunningOutProduct>(`${this.hostURl}/api/running-out/getAllRunningOutProducts`)
    .pipe(map(result => new RunningOutProduct(result)));
  }
  
}
