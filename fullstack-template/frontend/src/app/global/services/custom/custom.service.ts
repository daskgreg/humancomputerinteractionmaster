import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { SocketsService } from '../core/sockets.service';
import { SIMLog } from '../../models/logger/logs.model';


@Injectable({
  providedIn: 'root'
})
export class CustomService {

  constructor(
    private router: Router,
    private titleService: Title,
    private socketsService: SocketsService
  ) { }

  public makeRandom() {
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890,./;'[]\=-)(*&^%$#@!~`";
    const lengthOfCode = 5;
    let text = "";
    for (let i = 0; i < lengthOfCode; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }


  convertDataUrlToBlob(dataUrl): Blob {
    const byteString = window.atob(dataUrl);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  createDOM_Element(DOM_Elem: String, elem_id: String, elem_class: String, target_type: any, target: any) {
    var e = document.createElement(DOM_Elem.toString());
    if (elem_id != null) {
      e.id = elem_id.toString();
    }
    if (elem_class != null) {
      e.classList.add(elem_class.toString());
    }
    if (target_type == "id") {
      document.getElementById(target.toString()).appendChild(e);
    } else if (target_type == "tagname") {
      document.getElementsByTagName(target.toString())[0].appendChild(e);
    }
  }

  redirect(target: String) {
    console.log(target);
    this.router.navigate([target]);
  }

  createDOM_ImgTextContainer(target: string, i, img_src: string, name: string, namespace: string) {
    var container = document.createElement('div');
    container.id = namespace + "-container-" + i.toString();
    container.classList.add(namespace + "_container");
    // container.setAttribute("(click)","productPlaced()");
    document.getElementById(target).appendChild(container);

    var prod_img = document.createElement('img');
    prod_img.id = namespace + "-img-" + i.toString();
    if (img_src) {
      prod_img.src = img_src;
    } else {
      prod_img.src = "";
    }
    prod_img.classList.add("product_image");
    document.getElementById(namespace + "-container-" + i.toString()).appendChild(prod_img);

    var prod_name = document.createElement('p');
    prod_name.id = namespace + "-name-" + i.toString();
    prod_name.innerText = name;
    prod_name.classList.add(namespace + "_name");
    document.getElementById(namespace + "-container-" + i.toString()).appendChild(prod_name);
  }

  _arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  public getTitle() {
    return this.titleService.getTitle();
  }

  public setDefaultTitle() {
    this.titleService.setTitle("SIM");
  }

  public async sendVoiceAssistantNotification(data) {
    try {
      var event = "voice-assistant";
      var request = await this.socketsService
        .sendSocketRequest(event, data)
        .toPromise();

      if (!request) {
        console.log("Request to voice assistant failed");
        return;
      }
    } catch (e) {
      console.error(e);
    }
  }

  public async writeToLogger(data: SIMLog) {
    try {
      var event = "logger-write";
      var request = await this.socketsService
        .sendSocketRequest(event, data)
        .toPromise();

      if (!request) {
        console.log("Request to logger failed");
        return;
      }
    } catch (e) {
      console.error(e);
    }
  }

}


