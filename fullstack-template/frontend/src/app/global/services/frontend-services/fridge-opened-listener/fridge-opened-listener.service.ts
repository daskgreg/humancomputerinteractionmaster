import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class FridgeOpenedListener {

    private opened = false;

    hasOpened = new EventEmitter<any>();

    constructor() {
        this.hasOpened.subscribe((opened: any) => {
            this.opened = opened;
        }).unsubscribe();
    }

    updateStatus(opened: any) {
        this.opened = opened;
    }
}
