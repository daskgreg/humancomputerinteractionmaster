import { Injectable, EventEmitter } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SendReceiptIdToScannedProducts {
    private data: string = "";

    sentReceiptId = new EventEmitter<string>();

    constructor() {
        this.sentReceiptId.subscribe((value) => {
            this.data = value;
        });
    }

    updateStatus(receipt_id: string) {
        this.data = receipt_id;
    }
}
