import { Injectable, EventEmitter } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListDeleteProducts {
    private deletionStatus : boolean = false;

    deletionDetected = new EventEmitter<boolean>();

    constructor() {
        this.deletionDetected.subscribe((value) =>{
            this.deletionStatus = value;
        });
    }

    updateStatus(status :  boolean){
        this.deletionStatus = status;
    }
}
