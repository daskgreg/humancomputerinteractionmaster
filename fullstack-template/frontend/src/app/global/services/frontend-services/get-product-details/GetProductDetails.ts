import { Injectable, EventEmitter } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Product } from 'src/app/global/models/products/product.model';

@Injectable({
    providedIn: 'root'
})
export class GetProductDetails {

    private data = {
        changed: false,
        product: null
    }

    haveData = new EventEmitter<any>();

    constructor() {
        this.haveData.subscribe((data: any) => {
            this.data = data;
        }).unsubscribe();
    }

    updateStatus(data: any) {
        this.data = data;
    }
}
