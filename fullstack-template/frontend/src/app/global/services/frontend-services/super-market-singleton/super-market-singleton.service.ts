import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Classification } from 'src/app/global/models/classification/classification.model';

@Injectable({
  providedIn: 'root'
})
export class SuperMarketSingletonService {
  private data;

  constructor() {
    this.data = new Subject<Classification>();
   }

   classificationStarted(data:Classification){
    this.data.next(data);
   }

   getData() : Observable<Classification>{
     return this.data.asObservable();
   }
}
