import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { SIMNotification } from '../../models/notifications/notifications.model';
import { Observable } from 'rxjs';
import { CustomService } from '../custom/custom.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  private hostURl: String;

  constructor(private http: HttpClient,
    private custom: CustomService) {
    this.hostURl = environment.host;
  }

  public createNotification(data: SIMNotification): Observable<SIMNotification> {
    data.id = this.custom.makeRandom();
    return this.http
      .post<SIMNotification>(`${this.hostURl}/api/notifications/createNotification`, data)
      .pipe(map(result => new SIMNotification(result)));
  }

  public deleteNotification(id: String): Observable<SIMNotification> {
    return this.http
      .get<SIMNotification>(`${this.hostURl}/api/notifications/deleteNotification?notification_id=${id}`)
      .pipe(map(result => new SIMNotification(result)));
  }

  public getAllNotifications(): Observable<SIMNotification> {
    return this.http
      .get<SIMNotification>(`${this.hostURl}/api/notifications/getAllNotifications`)
      .pipe(map(result => new SIMNotification(result)));
  }

  public getAllNotificationsBySeverity(severity: String): Observable<SIMNotification> {
    return this.http
      .get<SIMNotification>(`${this.hostURl}/api/notifications/getAllNotificationsBySeverity?criteria=${severity}`)
      .pipe(map(result => new SIMNotification(result)));
  }

  public getLatestNotification(): Observable<SIMNotification> {
    return this.http
      .get<SIMNotification>(`${this.hostURl}/api/notifications/getLatestNotification`)
      .pipe(map(result => new SIMNotification(result)));
  }

  public readAllNotifications(): Observable<SIMNotification> {
    return this.http
      .get<SIMNotification>(`${this.hostURl}/api/notifications/readAllNotifications`)
      .pipe(map(result => new SIMNotification(result)));
  }

  public getUnreadNotifications(): Observable<SIMNotification> {
    return this.http
      .get<SIMNotification>(`${this.hostURl}/api/notifications/getUnreadNotifications`)
      .pipe(map(result => new SIMNotification(result)));
  }
}
