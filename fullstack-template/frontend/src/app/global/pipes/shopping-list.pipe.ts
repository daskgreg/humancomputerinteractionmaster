import { PipeTransform, Pipe } from '@angular/core';
import { ShoppingListProduct } from 'src/app/global/models/products/shopping-list-product.model';

@Pipe({
    name: 'productFilter'
})
export class ShoppingListPipe implements PipeTransform {
    transform(products: ShoppingListProduct[],searchProd: String): ShoppingListProduct[]{

        if(!products || !searchProd){
            return products;
        }

        return products.filter(products => products.product_details.name.toLocaleLowerCase().indexOf(searchProd.toLowerCase()) !== -1);
    }
}