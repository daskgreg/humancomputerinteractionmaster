# Details

Date : 2020-06-19 21:27:21

Directory d:\Documents\HY569_GitProject\hy569\fullstack-template\frontend\src\app\global

Total : 41 files,  1142 codes, 35 comments, 210 blanks, all 1387 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [frontend/src/app/global/directives/product-menu.directive.ts](/frontend/src/app/global/directives/product-menu.directive.ts) | TypeScript | 52 | 1 | 11 | 64 |
| [frontend/src/app/global/directives/scanned-product-id.directive.ts](/frontend/src/app/global/directives/scanned-product-id.directive.ts) | TypeScript | 12 | 0 | 4 | 16 |
| [frontend/src/app/global/directives/sp-list-remove.directive.ts](/frontend/src/app/global/directives/sp-list-remove.directive.ts) | TypeScript | 7 | 0 | 4 | 11 |
| [frontend/src/app/global/material/material.ts](/frontend/src/app/global/material/material.ts) | TypeScript | 74 | 0 | 2 | 76 |
| [frontend/src/app/global/models/classification/classification.model.ts](/frontend/src/app/global/models/classification/classification.model.ts) | TypeScript | 4 | 0 | 1 | 5 |
| [frontend/src/app/global/models/containers/containers.model.ts](/frontend/src/app/global/models/containers/containers.model.ts) | TypeScript | 10 | 0 | 1 | 11 |
| [frontend/src/app/global/models/greg/greg.model.ts](/frontend/src/app/global/models/greg/greg.model.ts) | TypeScript | 9 | 0 | 0 | 9 |
| [frontend/src/app/global/models/index.ts](/frontend/src/app/global/models/index.ts) | TypeScript | 1 | 0 | 1 | 2 |
| [frontend/src/app/global/models/notifications/notifications.model.ts](/frontend/src/app/global/models/notifications/notifications.model.ts) | TypeScript | 11 | 1 | 0 | 12 |
| [frontend/src/app/global/models/notifications/push.model.ts](/frontend/src/app/global/models/notifications/push.model.ts) | TypeScript | 7 | 0 | 1 | 8 |
| [frontend/src/app/global/models/products/product.model.ts](/frontend/src/app/global/models/products/product.model.ts) | TypeScript | 44 | 0 | 2 | 46 |
| [frontend/src/app/global/models/products/receipt-product.model.ts](/frontend/src/app/global/models/products/receipt-product.model.ts) | TypeScript | 16 | 0 | 1 | 17 |
| [frontend/src/app/global/models/products/running-out-products.model.ts](/frontend/src/app/global/models/products/running-out-products.model.ts) | TypeScript | 17 | 0 | 1 | 18 |
| [frontend/src/app/global/models/products/shopping-list-product.model.ts](/frontend/src/app/global/models/products/shopping-list-product.model.ts) | TypeScript | 19 | 0 | 1 | 20 |
| [frontend/src/app/global/models/products/super-market-products.model.ts](/frontend/src/app/global/models/products/super-market-products.model.ts) | TypeScript | 19 | 1 | 1 | 21 |
| [frontend/src/app/global/models/tasks/task.model.ts](/frontend/src/app/global/models/tasks/task.model.ts) | TypeScript | 8 | 0 | 4 | 12 |
| [frontend/src/app/global/models/user/loginUser.model.ts](/frontend/src/app/global/models/user/loginUser.model.ts) | TypeScript | 7 | 0 | 0 | 7 |
| [frontend/src/app/global/models/user/sessionUser.model.ts](/frontend/src/app/global/models/user/sessionUser.model.ts) | TypeScript | 12 | 0 | 2 | 14 |
| [frontend/src/app/global/pipes/safePipe.pipe.ts](/frontend/src/app/global/pipes/safePipe.pipe.ts) | TypeScript | 12 | 0 | 4 | 16 |
| [frontend/src/app/global/pipes/search.pipe.ts](/frontend/src/app/global/pipes/search.pipe.ts) | TypeScript | 14 | 0 | 5 | 19 |
| [frontend/src/app/global/pipes/shopping-list.pipe.ts](/frontend/src/app/global/pipes/shopping-list.pipe.ts) | TypeScript | 13 | 0 | 3 | 16 |
| [frontend/src/app/global/services/containers/containers-service.service.ts](/frontend/src/app/global/services/containers/containers-service.service.ts) | TypeScript | 50 | 0 | 10 | 60 |
| [frontend/src/app/global/services/core/sockets.service.ts](/frontend/src/app/global/services/core/sockets.service.ts) | TypeScript | 88 | 21 | 25 | 134 |
| [frontend/src/app/global/services/custom/custom.service.ts](/frontend/src/app/global/services/custom/custom.service.ts) | TypeScript | 89 | 0 | 4 | 93 |
| [frontend/src/app/global/services/custom/qrcode.min.js](/frontend/src/app/global/services/custom/qrcode.min.js) | JavaScript | 1 | 0 | 0 | 1 |
| [frontend/src/app/global/services/frontend-services/get-product-details/GetProductDetails.ts](/frontend/src/app/global/services/frontend-services/get-product-details/GetProductDetails.ts) | TypeScript | 21 | 0 | 6 | 27 |
| [frontend/src/app/global/services/frontend-services/send-receipt-id-to-scanned-products/send-receipt-id-to-scanned-products.service.ts](/frontend/src/app/global/services/frontend-services/send-receipt-id-to-scanned-products/send-receipt-id-to-scanned-products.service.ts) | TypeScript | 17 | 0 | 5 | 22 |
| [frontend/src/app/global/services/frontend-services/shopping-list-delete-product.ts/shopping-list-delete-products.ts](/frontend/src/app/global/services/frontend-services/shopping-list-delete-product.ts/shopping-list-delete-products.ts) | TypeScript | 17 | 0 | 5 | 22 |
| [frontend/src/app/global/services/frontend-services/super-market-singleton/super-market-singleton.service.ts](/frontend/src/app/global/services/frontend-services/super-market-singleton/super-market-singleton.service.ts) | TypeScript | 18 | 0 | 5 | 23 |
| [frontend/src/app/global/services/index.ts](/frontend/src/app/global/services/index.ts) | TypeScript | 3 | 0 | 1 | 4 |
| [frontend/src/app/global/services/notifications/notifications.service.ts](/frontend/src/app/global/services/notifications/notifications.service.ts) | TypeScript | 53 | 0 | 11 | 64 |
| [frontend/src/app/global/services/products/products/products.service.ts](/frontend/src/app/global/services/products/products/products.service.ts) | TypeScript | 156 | 0 | 31 | 187 |
| [frontend/src/app/global/services/products/receipt-products/receipt-products.service.ts](/frontend/src/app/global/services/products/receipt-products/receipt-products.service.ts) | TypeScript | 45 | 0 | 8 | 53 |
| [frontend/src/app/global/services/products/running-out-products/running-out-products.service.ts](/frontend/src/app/global/services/products/running-out-products/running-out-products.service.ts) | TypeScript | 40 | 0 | 8 | 48 |
| [frontend/src/app/global/services/products/shopping-list-products/shopping-list-products.service.ts](/frontend/src/app/global/services/products/shopping-list-products/shopping-list-products.service.ts) | TypeScript | 47 | 0 | 8 | 55 |
| [frontend/src/app/global/services/products/super-market-products/super-market-products.service.ts](/frontend/src/app/global/services/products/super-market-products/super-market-products.service.ts) | TypeScript | 25 | 0 | 4 | 29 |
| [frontend/src/app/global/services/push-notification/push-notification.service.ts](/frontend/src/app/global/services/push-notification/push-notification.service.ts) | TypeScript | 9 | 3 | 4 | 16 |
| [frontend/src/app/global/services/session/user-session.service.ts](/frontend/src/app/global/services/session/user-session.service.ts) | TypeScript | 44 | 0 | 6 | 50 |
| [frontend/src/app/global/services/tasks/tasks.service.ts](/frontend/src/app/global/services/tasks/tasks.service.ts) | TypeScript | 39 | 0 | 11 | 50 |
| [frontend/src/app/global/services/test-service.service.ts](/frontend/src/app/global/services/test-service.service.ts) | TypeScript | 7 | 0 | 3 | 10 |
| [frontend/src/app/global/variables/variables.ts](/frontend/src/app/global/variables/variables.ts) | TypeScript | 5 | 8 | 6 | 19 |

[summary](results.md)