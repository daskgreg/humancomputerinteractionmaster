# Summary

Date : 2020-06-19 21:27:22

Directory d:\Documents\HY569_GitProject\hy569\fullstack-template\frontend\src\app\global

Total : 41 files,  1142 codes, 35 comments, 210 blanks, all 1387 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript | 40 | 1,141 | 35 | 210 | 1,386 |
| JavaScript | 1 | 1 | 0 | 0 | 1 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 41 | 1,142 | 35 | 210 | 1,387 |
| directives | 3 | 71 | 1 | 19 | 91 |
| material | 1 | 74 | 0 | 2 | 76 |
| models | 14 | 184 | 2 | 16 | 202 |
| models\classification | 1 | 4 | 0 | 1 | 5 |
| models\containers | 1 | 10 | 0 | 1 | 11 |
| models\greg | 1 | 9 | 0 | 0 | 9 |
| models\notifications | 2 | 18 | 1 | 1 | 20 |
| models\products | 5 | 115 | 1 | 6 | 122 |
| models\tasks | 1 | 8 | 0 | 4 | 12 |
| models\user | 2 | 19 | 0 | 2 | 21 |
| pipes | 3 | 39 | 0 | 12 | 51 |
| services | 19 | 769 | 24 | 155 | 948 |
| services\containers | 1 | 50 | 0 | 10 | 60 |
| services\core | 1 | 88 | 21 | 25 | 134 |
| services\custom | 2 | 90 | 0 | 4 | 94 |
| services\frontend-services | 4 | 73 | 0 | 21 | 94 |
| services\frontend-services\get-product-details | 1 | 21 | 0 | 6 | 27 |
| services\frontend-services\send-receipt-id-to-scanned-products | 1 | 17 | 0 | 5 | 22 |
| services\frontend-services\shopping-list-delete-product.ts | 1 | 17 | 0 | 5 | 22 |
| services\frontend-services\super-market-singleton | 1 | 18 | 0 | 5 | 23 |
| services\notifications | 1 | 53 | 0 | 11 | 64 |
| services\products | 5 | 313 | 0 | 59 | 372 |
| services\products\products | 1 | 156 | 0 | 31 | 187 |
| services\products\receipt-products | 1 | 45 | 0 | 8 | 53 |
| services\products\running-out-products | 1 | 40 | 0 | 8 | 48 |
| services\products\shopping-list-products | 1 | 47 | 0 | 8 | 55 |
| services\products\super-market-products | 1 | 25 | 0 | 4 | 29 |
| services\push-notification | 1 | 9 | 3 | 4 | 16 |
| services\session | 1 | 44 | 0 | 6 | 50 |
| services\tasks | 1 | 39 | 0 | 11 | 50 |
| variables | 1 | 5 | 8 | 6 | 19 |

[details](details.md)