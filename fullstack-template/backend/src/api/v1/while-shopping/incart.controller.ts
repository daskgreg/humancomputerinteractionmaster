import { Request, Response, NextFunction, Router, request } from 'express';

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
let mongoose = require('mongoose');

var env = require('../../../config/environment');

var cart = require('../../../models/InCart');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req: any, res: any, next: any) {
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

    //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});

export class InCartController {
    /**
     * Apply all routes for files
     *
     *
     * @returns {Router}
     */
    public applyRoutes(): Router {
        const router = Router();

        router
            .get('/addToCart', this.addToCart)
            .get('/getProducts', this.getProducts);

        return router;
    }

    addToCart(req: Request, res: Response) {
        mongoose.Promise = global.Promise;
        mongoose.connect(mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).then(() => {
            var p = new cart();
            p.id = req.query.id;
            p.name = req.query.name

            p.save(function (err: any) {
                if (err) return console.log(err);
            });

            res.status(200);
            res.json({
                "product": p
            });
        },
            (error: any) => {
                console.log('Database could not connect: ' + error)
            })
    }

    getProducts(req: Request, res: Response) {
        cart.find({})
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false,
                        "message": "No products found!"
                    });
                    return;
                } else {
                    res.status(200);
                    res.send({
                        "success": true,
                        "data": prod
                    });
                    return;
                }
            })
    }

}