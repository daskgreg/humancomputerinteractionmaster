import { Request, Response, NextFunction, Router } from 'express';
import { NotFound, BadRequest } from 'http-errors';
import { DIContainer, MinioService, SocketsService } from '@app/services';
import { logger } from '../../../utils/logger';
import { getHostDomain } from '@app/config/environment';

var path = require('path');
var fs = require("fs");
var imgPath = '../../../ext_data/user_profiles/profile1.png';

var imgp = path.join(__dirname, imgPath);


var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var env = require('../../../config/environment');

var user = require('../../../models/Users');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function(req:any, res:any, next:any) {
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

//  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Pass to next layer of middleware
  next();
});

export class UserController {

  /**
   * Apply all routes for example
   *
   * @returns {Router}
   */
  public applyRoutes(): Router {
    const router = Router();

    router.use(bodyParser.json());
    router.use(bodyParser.urlencoded({ extended: true }));
    router
      .post('/register', this.register)
      .post('/login', this.login)
      .get('/get', this.test);

    return router;
  }

  public register(req: Request, res: Response) {
    var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
    let mongoose = require('mongoose');

    

    // Connecting with mongo db
    mongoose.Promise = global.Promise;
    mongoose.connect(mongoURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).then(() => {
          console.log('Database sucessfully connected')
          
          // var userInstance = new user({
          //   first_name: req.body.name,
          //   last_name: 'Keepo',
          //   Email: 'kappa@keepo.com'
          // })

          var userInstance = new user();
          userInstance.first_name = "Marc";
          userInstance.last_name = "Morrison";
          userInstance.email = "m.morrison@sim.com";
          userInstance.password = "Secret";
          userInstance.img_data = fs.readFileSync(imgp);
          userInstance.img_type = 'image/png';

          // var userInstance = new user();
          // userInstance.name.first = "Marc";
          // userInstance.name.last = "Morrison";
          // userInstance.email = "m.morrison@sim.com";
          // userInstance.password = "Secret";
          // userInstance.img.data = "fs.readFileSync(imgPath)";
          // userInstance.img.contentType = "image/png";
          
          userInstance.save(function(err:any){
            if(err) return console.log(err);
          });

          res.status(200);
          res.json({
          "message": req.body
          });
      },
      (error : any) => {
          console.log('Database could not connect: ' + error)
      }
    )
  }

  public login(req: Request, res: Response) {

    const user2log = user.findOne({email: req.body.email})
    .exec(function(err:any, user:any){
      if(err){
        //error
        return err;
      }else if(!user){
        //no user
        res.status(401);
        res.json({
          "status": "401",
          "message": "User not found"
        });
        return;
      }else{
        //check for password
        if(req.body.password == user.password){
          // console.log(user.name.first);
          // console.log(user);
          res.status(200);
          res.json({
            "status": "200",
            "message": "Login Successful",
            "first_name" : user.first_name,
            "last_name": user.last_name,
            "image_type": user.image_type,
            "image_data": Buffer.from(user.image_data).toString('base64')
          });
          // logger.info(`User ${user.first_name} ${user.last_name} logged in successfuly`);
        }else{
        res.status(401);
        res.json({
          "status": "401",
          "message": "Wrong Password"
        });
        return;
        }
      }
    });
  }

  public test(req: Request, res: Response) {

    res.status(200);
    res.json({
      "message": "ola komple"
    });
  }

}
