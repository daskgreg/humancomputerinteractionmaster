import Multer from 'multer';
import { Request, Response, NextFunction, Router, request } from 'express';
import { NotFound, BadRequest } from 'http-errors';
import { DIContainer, MinioService } from '@app/services';
import { template } from 'lodash';
import { ShoppingListController } from '../shopping-list/shopping-list.controller';

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
let mongoose = require('mongoose');

var env = require('../../../config/environment');

var notification = require('../../../models/Notifications');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req: any, res: any, next: any) {
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

    //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});

export class TestController {

    /**
     * Apply all routes for example
     *
     * @returns {Router}
     */
    public applyRoutes(): Router {
        const router = Router();

        router
            .post('/postTest', this.postTests)
            .get('/getTest', this.getTest);

        return router;
    }

    /**
     * Sens a message back as a response
     */
    public getTest(req: Request, res: Response) {

        console.log(req.query);
        res.status(200);
        res.json({
            "message": "GET Succesfull",
            "data": req.query
        });
    }

    /**
     * Broadcasts a received message to all connected clients
     */
    public postTests(req: Request, res: Response) {
        const message: string = req.body.message;
        const event: string = req.body.event;

        //Sending a broadcast message to all clients
        // const socketService = DIContainer.get(SocketsService);
        // socketService.broadcast(event, message);

        console.log(req.body);
        console.log(req.body.source);
        console.log(req.body.target);
        res.status(200);
        res.json({
            encapsulated: {
                e1: "Hello"
            },
            "message": "POST Succesfull",
            "data1": req.body,
            "data2": req.body.source,
            "data3": req.body.target,
        });

    }

}
