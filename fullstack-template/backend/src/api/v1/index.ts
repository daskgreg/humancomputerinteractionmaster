import * as express from 'express';
//import { ResourceController } from '../shared';
//import { ITask, TaskModel } from '@app/models';
import { FilesController } from './files/files.controller';
import { SocketEventsController } from './socket-events/socket-events.controller';
import { ExampleController } from './example/example.controller';
import { TestController } from './test/test.controller';
import { UserController } from './user/user.controller';
import { ConfigurationController } from './configuration/configuration.controller';
import { ExternalAPIController } from './external-apis/external-apis.controller';
import { KitchenController } from './kitchen/kitchen.controller';
import { ProductsController } from './products/products.controller';
import { ShoppingListController } from './shopping-list/shopping-list.controller';
import { RunningOutController } from './running-out/running-out.controller';
import { NotificationsController } from './notifications/notifications.controller';
import { ContainersController } from './containers/containers.controller';
// import { InCartController } from './while-shopping/incart.controller';
import { InCartController } from './while-shopping/incart.controller';

const apiV1Router = express.Router();



apiV1Router
  // Sockets events routes
  .use(
    '/socket-events',
    new SocketEventsController().applyRoutes()
  )

  // Sockets events routes
  .use(
    '/files',
    new FilesController().applyRoutes()
  )

  // Task routes
  // .use(
  //   '/tasks',
  //   new ResourceController<ITask>(TaskModel).applyRoutes()
  // )

  // Example routes
  .use(
    '/example',
    new ExampleController().applyRoutes()
  )

  .use(
    '/test',
    new TestController().applyRoutes()
  )

  .use(
    '/user',
    new UserController().applyRoutes()
  )

  .use(
    '/configuration',
    new ConfigurationController().applyRoutes()
  )

  .use(
    '/kitchen',
    new KitchenController().applyRoutes()
  )

  .use(
    '/products',
    new ProductsController().applyRoutes()
  )

  .use(
    '/shopping-list',
    new ShoppingListController().applyRoutes()
  )

  .use(
    '/running-out',
    new RunningOutController().applyRoutes()
  )

  .use(
    '/notifications',
    new NotificationsController().applyRoutes()
  )

  .use(
    '/containers',
    new ContainersController().applyRoutes()
  )

  // .use(
  //   '/shopping-incart',
  //   new InCartController().applyRoutes()
  // )

  .use(
    '/shopping-in-cart',
    new InCartController().applyRoutes()
  )

  .use(
    '/virtual',
    new ExternalAPIController().applyRoutes()
  );


export { apiV1Router };

