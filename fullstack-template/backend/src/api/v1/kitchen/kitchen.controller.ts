import { Request, Response, NextFunction, Router } from 'express';
import { NotFound, BadRequest } from 'http-errors';
import { DIContainer, MinioService, SocketsService } from '@app/services';
import { logger } from '../../../utils/logger';
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
let mongoose = require('mongoose');

var env = require('../../../config/environment');

var product = require('../../../models/Products');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req: any, res: any, next: any) {
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

    //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});
export class KitchenController {

    /**
     * Apply all routes for example
     *
     * @returns {Router}
     */
    public applyRoutes(): Router {
        const router = Router();

        router
            .post('/addProduct', this.addProduct)
            .get('/getProductByPosition', this.getProductByPosition)
            .get('/getProductByCategory', this.getProductByCategory)
            .get('/getProductByTag', this.getProductByTag)
            .get('/getProductById', this.getProductById)
            .get('/getProductByContainerId', this.getProductByContainerId)
            .get('/getProductByName', this.getProductByName)
            .get('/getProductByPhoneticName', this.getProductByPhoneticName);

        return router;
    }

    public addProduct(req: Request, res: Response) {
        //if(req.body.name == "override"){
        mongoose.Promise = global.Promise;
        mongoose.connect(mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).then(() => {
            var p = new product();
            p.product_details.id = "42276";
            p.product_details.name = "notha_prod3";
            p.details.category = "Vegetables";
            p.details.full_ammount_potential = "1000";
            p.details.ammount_remaining = "100";
            p.details.ammount_unit = "gr";
            p.details.prod_date = "2020-05-17";
            p.details.prod_date instanceof Date;
            p.details.exp_date = "2020-05-25";
            p.details.prod_date instanceof Date;
            p.details.tags = "Gluten-Free";
            p.location.room = "Kitchen";
            p.location.general_storage = "Fridge";//cabinet
            p.location.general_storage_location = "Fridge";//top left cabbinet
            p.location.inside_storage_location = "bottom right drawer";//top left INSIDE cabbinet
            p.iot.container_id = "FridgeContainer12";
            p.image.image_type = "image/png";
            p.image.image_data = "imageBase64";
            p.is_custom = false;
            p.is_favourite = false;

            p.save(function (err: any) {
                if (err) return console.log(err);
            });

            res.status(200);
            res.json({
                "message": p
            });
        },
            (error: any) => {
                console.log('Database could not connect: ' + error)
            })
        //}else{

        //}
    }

    public getProductByPosition(req: Request, res: Response) {
        // console.log(req.query.productpos);
        const prod2find = product.find({ "location.general_storage": req.query.productpos })
            .exec(function (err: any, products: any) {
                if (err) {
                    return err;
                } else if (!products) {
                    res.status(401);
                    res.json({
                        "status": "401",
                        "message": "No products found!"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "products": products
                    });
                }
            });

    }

    public getProductByCategory(req: Request, res: Response) {
        const prod2find = product.find({ "details.category": req.query.productcat })
            .exec(function (err: any, products: any) {
                console.log(products);
                if (err) {
                    return err;
                } else if (!products) {
                    res.status(401);
                    res.json({
                        "status": "401",
                        "message": "No products found!"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "products": products
                    });
                }
            });
    }

    public getProductByTag(req: Request, res: Response) {
        const prod2find = product.find({ "details.tag": req.query.producttag })
            .exec(function (err: any, products: any) {
                console.log(products);
                if (err) {
                    return err;
                } else if (!products) {
                    res.status(401);
                    res.json({
                        "status": "401",
                        "message": "No products found!"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "products": products
                    });
                }
            });
    }

    public getProductById(req: Request, res: Response) {
        const prod2find = product.find({ "product_details.id": req.query.productid })
            .exec(function (err: any, prod: any) {
                console.log(prod);
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(401);
                    res.json({
                        "status": "500",
                        "message": "No products found!",
                        "API": "getProductByContainerId"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "product": prod
                    });
                }
            });
    }

    public getProductByContainerId(req: Request, res: Response) {
        const prod2find = product.findOne({ "iot.container_id": req.query.productcontid })
            .exec(function (err: any, products: any) {
                console.log(products);
                if (err) {
                    return err;
                } else if (!products) {
                    res.status(401);
                    res.json({
                        "status": "500",
                        "message": "No products found!",
                        "API": "getProductByContainerId"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "products": products
                    });
                }
            });
    }

    public getProductByName(req: Request, res: Response) {
        const prod2find = product.findOne({ "product_details.name": req.query.productname.toString().toLowerCase() },
            "location")
            .exec(function (err: any, doc: any) {
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "message": "No products with name " + req.query.productname + " found!",
                        "API": "getProductByContainerId"
                    });
                    return;
                } else {
                    if (req.query && req.query.ischatscript === "true") {
                        console.log("from chatscript");
                    }
                    res.status(200);
                    res.json({
                        "product": doc
                    });
                }
            });
    }

    public getProductByPhoneticName(req: Request, res: Response) {
        const prod2find = product.findOne({ "iot.phonetic_name": req.query.productname.toString().toLowerCase() },
            "location")
            .exec(function (err: any, doc: any) {
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "message": "No products with name " + req.query.productname + " found!",
                        "API": "getProductByContainerId"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "product": doc
                    });
                }
            });
    }
}
