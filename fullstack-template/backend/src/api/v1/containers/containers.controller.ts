/**
 * addContainers: Register a new container
 * getContainersByLocation: 
 * getAllContainers: 
 * getBaseInfo: 
 * getIncorrectlyPlaced: 
 * setCurrentContainerOfBase: 
 * removeContainerFromBase
 */
import { Request, Response, Router } from 'express';

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
let mongoose = require('mongoose');

var env = require('../../../config/environment');

var container = require('../../../models/Containers');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req: any, res: any, next: any) {
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

    //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});

export class ContainersController {
    public applyRoutes(): Router {
        const router = Router();

        router.use(bodyParser.json());
        router.use(bodyParser.urlencoded({ extended: true }));
        router
            .post('/addContainers', this.addContainers)
            .get('/getContainersByLocation', this.getContainersByLocation)
            .get('/getAllContainers', this.getAllContainers)
            .get('/getBaseInfo', this.getBaseInfo)
            .get('/getIncorrectlyPlaced', this.getIncorrectlyPlaced)
            .get('/setCurrentContainerOfBase', this.setCurrentContainerOfBase)
            .get('/takeContainerFromBaseWithId', this.takeContainerFromBaseWithId)
            .get('/getBaseByCorrespondingContainerId', this.getBaseByCorrespondingContainerId);

        return router;
    }
    addContainers(req: Request, res: Response) {
        mongoose.Promise = global.Promise;
        mongoose.connect(mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).then(() => {
            var containerInstance = new container();

            containerInstance.location = "Cabbinet1"
            containerInstance.base_id = "container_1_base"
            containerInstance.corresponding_container = "container_1"
            containerInstance.current_container = "container_1"
            containerInstance.has_correct_container = true;

            containerInstance.save(function (err: any) {
                if (err) return console.log(err);
            });

            res.status(200);
            res.json({
                "message": "Container Added!"
            });


        },
            (error: any) => {
                console.log('Database could not connect: ' + error)
            })
    }

    getContainersByLocation(req: Request, res: Response) {
        var qLocation = req.query.location;
        container.find({ location: qLocation })
            .exec(function (err: any, doc: any) {
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "success": false,
                        "message": `No containers at location ` + qLocation + ` found!`
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": doc
                    });
                    return;
                }
            });
    }

    getAllContainers(req: Request, res: Response) {
        container.find({})
            .exec(function (err: any, doc: any) {
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "success": false,
                        "message": "No containers found!"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": doc
                    });
                    return;
                }
            });
    }

    getBaseInfo(req: Request, res: Response) {
        var qBaseId = req.query.base_id;
        container.findOne({ base_id: qBaseId })
            .exec(function (err: any, doc: any) {
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "success": false,
                        "message": `No base with id ` + qBaseId + ` was found!`
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": doc
                    });
                    return;
                }
            })
    }

    getIncorrectlyPlaced(req: Request, res: Response) {
        container.find({ has_correct_container: false })
            .exec(function (err: any, doc: any) {
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "success": true,
                        "allContainersCorrectlyPlaced": true,
                        "message": "No missplaced found!"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "success": true,
                        "allContainersCorrectlyPlaced": false,
                        "data": doc
                    });
                    return;
                }
            })
    }

    setCurrentContainerOfBase(req: Request, res: Response) {
        var qContainerToAdd = req.query.container;
        var qBaseId = req.query.base_id

        var query = { base_id: qBaseId };

        container.findOne({ base_id: qBaseId })
            .exec(function (err: any, doc: any, isCorrect: any) {
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "success": false,
                        "message": `No base with id ` + qBaseId + ` was found!`
                    });
                    return;
                } else {
                    var update;
                    if (doc.corresponding_container == qContainerToAdd.toString()) {
                        update = { current_container: qContainerToAdd, has_correct_container: true };
                    } else {
                        update = { current_container: qContainerToAdd, has_correct_container: false };
                    }

                    container.findOneAndUpdate(query, update, { new: true })
                        .exec(function (err: any, doc: any) {
                            if (err) {
                                return err;
                            } else if (!doc) {
                                res.status(200);
                                res.json({
                                    "success": false,
                                    "message": `No base with id ` + qBaseId + ` was found!`
                                });
                                return;
                            } else {
                                res.status(200);
                                res.json({
                                    "success": true,
                                    "message": `Current container of base with id ` + qBaseId + ` was changed to ` + qContainerToAdd + `!`,
                                    "data": doc
                                });
                            }
                        })
                }
            });

    }

    takeContainerFromBaseWithId(req: Request, res: Response) {
        var qBaseId = req.query.base_id;

        var query = { base_id: qBaseId };
        var update = { current_container: "none", has_correct_container: false };

        container.findOneAndUpdate(query, update, { upsert: true })
            .exec(function (err: any, doc: any) {
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "success": false,
                        "message": `No base with id '` + qBaseId + `' was found!`
                    });
                    return;
                } else {
                    if (doc.current_container == "none") {
                        res.status(200);
                        res.json({
                            "success": false,
                            "message": `Base with id '` + qBaseId + `' has no container!`
                        });
                    }
                    res.status(200);
                    res.json({
                        "success": true,
                        "message": `Container with id ` + doc.current_container.toString() + ` was taken from base with id ` + qBaseId + `!`,
                        "current_container": doc.current_container
                    });
                }
            })
    }

    getBaseByCorrespondingContainerId(req: Request, res: Response) {
        var container_id = req.query.container_id;
        container.findOne({ corresponding_container: container_id })
            .exec(function (err: any, doc: any) {
                console.log(doc);
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "success": false,
                        "message": `Container with id ` + container_id + ` was not found!`
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": doc
                    });
                    return;
                }
            })
    }

    handleQueryData(req: Request, res: Response, err: any, doc: any) {

    }
}