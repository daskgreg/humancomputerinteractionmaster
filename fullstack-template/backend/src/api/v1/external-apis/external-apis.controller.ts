import { Request, Response, Router } from 'express';

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
let mongoose = require('mongoose');

var env = require('../../../config/environment');

var receipt = require('../../../models/Receipts');
var rec_prod = require('../../../models/ReceiptProducts');
var supermaket_prod = require('../../../models/SuperMarketProducts');

var path = require('path');
var fs = require('fs');
// imgPath = '../../../../../frontend/src/app/pages/images/icons/milk-1.svg';

var path = require('path');
var fs = require("fs");
// var imgPath = '../../../ext_data/user_profiles/profile1.png';




app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req: any, res: any, next: any) {
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

    //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});

export class ExternalAPIController {
    public applyRoutes(): Router {
        const router = Router();

        router.use(bodyParser.json());
        router.use(bodyParser.urlencoded({ extended: true }));
        router
            .post('/generateReceipt', this.generateReceipt)
            .post('/fetchReceipt', this.fetchReceipt)
            .get('/getReceipt', this.fetchReceiptGet)
            .get('/getProductFromReceipt', this.getProductFromReceipt)
            .get('/getProductNamesFromShoppingList', this.getProductNamesFromShoppingList)
            .get('/getReceiptProductById', this.getReceiptProductById)
            .post('/addSuperMarketProducts', this.addSuperMarketProducts)
            .get('/getSuperMarketProducts', this.getSuperMarketProducts)
            .get('/getSuperMarketProductById', this.getSuperMarketProductById)
            .get('/getProductsFromReceiptWithID', this.getProductsFromReceiptWithID);

        return router;
    }

    public generateReceipt(req: Request, res: Response) {
        mongoose.Promise = global.Promise;
        mongoose.connect(mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).then(() => {
            console.log('Database sucessfully connected')

            var receiptInstance = new receipt();
            receiptInstance.receipt_id = "receipt1";//QR Code
            receiptInstance.general_info.date = "2020-05-17";
            receiptInstance.general_info.date instanceof Date;
            receiptInstance.super_market.name = "Holmes";
            receiptInstance.super_market.address = "221B Baker Str.";
            receiptInstance.payment_details.ammount = "56.82";
            receiptInstance.payment_details.currency = "EUR";
            receiptInstance.payment_details.method = "Bitcoin";
            receiptInstance.payment_details.vat_percentage = "24";
            receiptInstance.user_details.name.first = "Suzan";
            receiptInstance.user_details.name.last = "Morrison";
            receiptInstance.image.imageType = "image/png";
            receiptInstance.image.imageData = "base 64 img";

            var recProd;
            for (var i = 0; i < 10; i++) {
                var imgPath2 = '../images/product (' + (i + 1).toString() + ').png';
                // require('../images/cheese-' + (i+1).toString() + '.svg');
                var imgp2 = path.join(__dirname, imgPath2);
                recProd = {
                    product_details: {
                        prod_id: 40002 + i + 1,
                        name: "Product" + i.toString(),
                        receipt_id: "receipt1"
                    },
                    details: {
                        ammount: "1",
                        ammount_unit: "pcs",
                        price: "7",
                        prod_date: "10 May 2020",
                        exp_date: "10 May 2021"
                    },
                    image: {
                        image_data: fs.readFileSync(imgp2),
                        image_type: 'image/png'
                    }

                };

                receiptInstance.products.push(recProd);
            }



            receiptInstance.remarks = "";

            receiptInstance.save(function (err: any) {
                if (err) return console.log(err);
            });

            res.status(200);
            res.json({
                "message": "Receipt Created!"
            });
        },
            (error: any) => {
                console.log('Database could not connect: ' + error)
            })
    }

    public fetchReceipt(req: Request, res: Response) {
        const receipt2find = receipt.findOne({ receipt_id: req.body.receipt_id }, "-products.image.image_data")
            .exec(function (err: any, receipt: any) {
                if (err) {
                    return err;
                } else if (!receipt) {
                    res.status(401);
                    res.json({
                        "status": "401",
                        "message": "Receipt not found"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "status": "200",
                        "message": "Receipt Found!",
                        "receipt": receipt
                        // ,"img_data": Buffer.from(receipt.image.image_data).toString('base64');
                    });
                }
            });
    }

    public fetchReceiptGet(req: Request, res: Response) {
        console.log("GET REQ ID: " + req.query.receipt_id);
        const receipt2find = receipt.findOne({ receipt_id: req.query.receipt_id })
            .exec(function (err: any, receipt: any) {
                if (err) {
                    return err;
                } else if (!receipt) {
                    res.status(200);
                    res.json({
                        "status": "200",
                        "success": false,
                        "message": "Receipt not found"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "status": "200",
                        "success": true,
                        "message": "Receipt Found!",
                        "receipt": receipt
                    });
                }
            });
    }

    getProductFromReceipt(req: Request, res: Response) {
        var prod = req.query.product_id;
        var rec = req.query.receipt_id;

        const prod2find = receipt.findOne({ receipt_id: rec, "products.product_details.prod_id": prod }, "products")
            .exec(function (err: any, receipt: any) {
                if (err) {
                    return err;
                } else if (!receipt) {
                    res.status(500);
                    res.json({
                        "product": null,
                        "API": "getProductFromReceipt",
                        "product_id": prod,
                        "receipt_id": rec
                    });
                    return;
                } else {
                    // receipt.products.array.forEach((element:any) => {
                    //     console.log(element);
                    //     if(element.product_details.receipt_id.toString() == rec.toString()){
                    //         res.status(200);
                    //         res.json({
                    //             "product" : element
                    //         });
                    //         return;
                    //     }
                    // });
                    // res.status(401);
                    // res.json({
                    //     "product" : null
                    // });
                    res.status(200);
                    res.json({
                        "product_found": true,
                        "data": receipt
                    });
                    return;
                    return;
                }
            });
    }

    getProductNamesFromShoppingList(req: Request, res: Response) {
        receipt.find({}, "products.product_details.name products.product_details.prod_id")
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "status": "500",
                        "message": "No products found!"
                    });
                    return;
                } else {
                    var names: any = [];
                    console.log(prod[0].products.length);
                    for (var i = 0; i < prod[0].products.length; i++) {
                        var c: any = {};

                        var item: any = {};
                        item['name'] = prod[0].products[i].product_details.name.toString();
                        item['id'] = prod[0].products[i].product_details.prod_id.toString();

                        c['product_details'] = item;
                        // item = `{
                        //     product_details: {
                        //         id: `+prod[0].products[i].product_details.prod_id.toString() +
                        //         `name: `+prod[0].products[i].product_details.name.toString() +
                        //     `}
                        // }`;
                        console.log(c);
                        names.push(c)
                    }


                    // console.log(prod[0].products[0]);
                    // console.log(names);
                    console.log(names);
                    res.status(200);
                    res.send({
                        "prod": names
                    });
                    return;
                }
            })
    }

    getReceiptProductById(req: Request, res: Response) {
        const prod2find = receipt.findOne({ "products._id": req.query.id }, "products")
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "product": null,
                        "API": "getReceiptProductById"
                    });
                    return;
                } else {

                    for (var i = 0; i < prod.products.length; i++) {
                        var element = prod.products[i];
                        console.log(element._id);
                        if (element._id.toString() == req.query.id.toString()) {
                            console.log("OKAY");
                            res.status(200);
                            res.json({
                                "product": element
                            });
                            return;
                        }
                    }
                    res.status(500);
                    res.json({
                        "product": "Not Found"
                    });
                    return;
                }
            });
    }

    addSuperMarketProducts(req: Request, res: Response) {
        var id = 40020;
        for (var i = 0; i < 60; i++) {
            mongoose.Promise = global.Promise;
            mongoose.connect(mongoURL, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }).then(() => {
                console.log('Database sucessfully connected')

                var prodInstance = new supermaket_prod;


                var products = ["Milk", "Coffee", "Cola", "Chocolate", "Sugar", "Cheese", "Eggs"];
                var categories = ["Dairy", "Vegetables", "Beverages", "Coffee", "Dessert"]
                var tags = ["Vegan", "Gluten-Free", "Lactose-Free", "Suitable for Diabetics", "Pesceterian", "Kosher"]
                var ammountType = ["pcs", "gr"]


                var p = products[Math.floor(Math.random() * products.length)];
                var x = Math.floor((Math.random() * 9) + 1);
                var y = Math.floor((Math.random() * 10) + 1) / 10;

                prodInstance.product_details.prod_id = id++;
                prodInstance.product_details.name = p;
                prodInstance.details.category = categories[Math.floor(Math.random() * categories.length)];
                prodInstance.details.tags = tags[Math.floor(Math.random() * tags.length)];
                prodInstance.details.price = x + y;
                prodInstance.details.ammountType = ammountType[Math.floor(Math.random() * ammountType.length)];
                prodInstance.image.image_type = "image/png";
                prodInstance.image.image_data = "image base 64";



                prodInstance.save(function (err: any) {
                    if (err) return console.log(err);
                });

                res.status(200);
                res.json({
                    "message": "Products Added!"
                });
            },
                (error: any) => {
                    console.log('Database could not connect: ' + error)
                })
        }
    }

    getSuperMarketProducts(req: Request, res: Response) {
        supermaket_prod.find({}, "product_details.name product_details.prod_id")
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "product": null,
                        "API": "getSuperMarketProducts"
                    });
                    return;
                } else {

                    var names: any = [];
                    console.log(prod);
                    for (var i = 0; i < prod.length; i++) {
                        var c: any = {};

                        var item: any = {};
                        item['name'] = prod[i].product_details.name.toString();
                        item['id'] = prod[i].product_details.prod_id.toString();

                        c['product_details'] = item;
                        console.log(c);
                        names.push(c)
                    }
                    res.status(200);
                    res.json({
                        "products_list": names
                    });
                    return;
                }
            })
    }

    getSuperMarketProductById(req: Request, res: Response) {
        supermaket_prod.findOne({ "product_details.prod_id": req.query.product_id })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "product": null,
                        "API": "getSuperMarketProductById"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "products_list": prod
                    });
                    return;
                }
            })
    }

    getProductsFromReceiptWithID(req: Request, res: Response) {
        receipt.find({ "receipt_id": req.query.receipt_id }, "products")
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(200);
                    res.json({
                        "success": false,
                        "API": "getProductsFromReceiptWithID"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": prod
                    });
                }
            })
    }
}
