import { Request, Response, NextFunction, Router } from 'express';
import { DIContainer, SocketsService } from '@app/services';
import { NO_CONTENT } from 'http-status-codes';


export class SocketEventsController {

  /**
   * Apply all routes for socket events
   * POST /socket-events/broadcast/:event   Broadcasts an event to all clients
   *
   * @returns {Router}
   */
  public applyRoutes(): Router {
    const router = Router();

    router.post('/broadcast/:event', this.broadcast())
      .post('/broadcast2/', this.broadcast2());

    return router;
  }

  /**
   * Broadcasts an event to all clients
   */
  public broadcast() {
    return async (req: Request, res: Response, next?: NextFunction): Promise<Response> => {
      try {
        const socketService = DIContainer.get(SocketsService);
        socketService.broadcast(req.params.event, req.body);

        return res.sendStatus(NO_CONTENT);

      } catch (e) {
        next(e);
      }
    };
  }

  public broadcast2() {
    return async (req: Request, res: Response, next?: NextFunction): Promise<Response> => {
      try {
        console.log(req.body);
        const socketService = DIContainer.get(SocketsService);
        socketService.broadcast(req.body.event, req.body);

        return res.sendStatus(NO_CONTENT);

      } catch (e) {
        next(e);
      }
    };
  }

}