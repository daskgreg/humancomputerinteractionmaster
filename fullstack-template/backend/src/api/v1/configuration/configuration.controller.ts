import { Request, Response, NextFunction, Router } from 'express';

var QRCode = require('qrcode');

var bodyParser = require('body-parser');

export class ConfigurationController{
  /**
   * Apply all routes for example
   *
   * @returns {Router}
   */   
    public applyRoutes(): Router {
    const router = Router();

    router.use(bodyParser.json());
    router.use(bodyParser.urlencoded({ extended: true }));
    router
      .post('/generateQR', this.generateQR);

    return router;
  }

    generateQR(req: Request, res: Response){
        var data : String = req.body.qrdata.toString();
        QRCode.toDataURL(data, function (err:any, url:any) {
            console.log(url)
            res.status(200);
            res.json({
                "message":url
            });
          })
    }
}