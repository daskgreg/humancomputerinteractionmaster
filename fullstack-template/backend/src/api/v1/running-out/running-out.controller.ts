import Multer from 'multer';
import { Request, Response, NextFunction, Router } from 'express';
import { NotFound, BadRequest } from 'http-errors';
// import { DIContainer, MinioService } from '@app/services';

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
let mongoose = require('mongoose');

var env = require('../../../config/environment');

var runningOutProduct = require('../../../models/RunningOutProducts');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req: any, res: any, next: any) {
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

    //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});

export class RunningOutController {

    /**
     * Apply all routes for files
     * POST /files/upload               Upload a new file
     * GET /files/download/:filename    Download a file by its name
     *
     * @returns {Router}
     */
    public applyRoutes(): Router {
        const router = Router();

        router
            .post('/addProductToRunningOutList', this.addProductToRunningOutList)
            .get('/removeProductFromRunningOutList', this.removeProductFromRunningOutList)
            .get('/checkIfProductIsInRunningOutList', this.checkIfProductIsInRunningOutList)
            .get('/getAllRunningOutListNames', this.getAllRunningOutListNames)
            .get('/getAllRunningOutProducts', this.getAllRunningOutProducts);

        return router;
    }

    addProductToRunningOutList(req: Request, res: Response) {
        mongoose.Promise = global.Promise;
        mongoose.connect(mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).then(() => {
            var p = new runningOutProduct();
            p.product_details.id = req.body.product_details.id;
            p.product_details.name = req.body.product_details.name;
            p.details.category = req.body.details.category;
            p.details.tags = req.body.details.tags;
            p.image.image_type = req.body.image.image_type;
            p.image.image_data = req.body.image.image_data;

            p.save(function (err: any) {
                if (err) return console.log(err);
            });

            res.status(200);
            res.json({
                "product": p
            });
        },
            (error: any) => {
                console.log('Database could not connect: ' + error)
            })
    }

    removeProductFromRunningOutList(req: Request, res: Response) {
        runningOutProduct.deleteOne({ "product_details.id": req.query.productid })
            .exec(function (err: any, doc: any) {
                if (err) {
                    return err;
                } else if (!res) {
                    res.status(500);
                    res.json({
                        "message": "No product found in Running Out List"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "isDeleted": true,
                        "productId": req.query.productid
                    });
                }
            })
    }

    checkIfProductIsInRunningOutList(req: Request, res: Response) {
        runningOutProduct.findOne({ "product_details.id": req.query.productid })
            .exec(function (err: any, doc: any) {
                if (err) {
                    return err;
                } else if (!doc) {
                    res.status(200);
                    res.json({
                        "exists": false
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "exists": true,
                        "product": doc
                    });
                }
            })
    }

    getAllRunningOutListNames(req: Request, res: Response) {
        runningOutProduct.find({}, "product_details.name product_details.id")
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "status": "500",
                        "message": "No products found!"
                    });
                    return;
                } else {
                    var names;

                    console.log(prod);
                    res.status(200);
                    res.send({
                        prod
                    });
                    return;
                }
            })
    }

    getAllRunningOutProducts(req: Request, res: Response) {
        runningOutProduct.find({},)
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false,
                        "message": "No products found!"
                    });
                    return;
                } else {
                    console.log(prod);
                    res.status(200);
                    res.send({
                        "success": true,
                        "data": prod
                    });
                    return;
                }
            })
    }
}