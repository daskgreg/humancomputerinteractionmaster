import Multer from 'multer';
import { Request, Response, NextFunction, Router, request } from 'express';
import { NotFound, BadRequest } from 'http-errors';
import { DIContainer, MinioService } from '@app/services';
import { template } from 'lodash';
import { ShoppingListController } from '../shopping-list/shopping-list.controller';

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@157.230.111.163:27000/ami-fullstack?authSource=admin';
var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
let mongoose = require('mongoose');

var env = require('../../../config/environment');

var notification = require('../../../models/Notifications');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req: any, res: any, next: any) {
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

  //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Pass to next layer of middleware
  next();
});

var notificationCounter = 0;
var newNotifications = 0;
var latestRead: number;
export class NotificationsController {
  /**
   * Apply all routes for files
   *
   *
   * @returns {Router}
   */
  public applyRoutes(): Router {
    const router = Router();


    router
      .post('/createNotification', this.generateNotification)
      .get('/deleteNotification', this.deleteNotification)
      .get('/getAllNotifications', this.getAllNotifications)
      .get('/getAllNotificationsBySeverity', this.getAllNotificationsBySeverity)
      .get('/getLatestNotification', this.getLatestNotification)
      .get('/readAllNotifications', this.readAllNotifications)
      .get('/getUnreadNotifications', this.getUnreadNotifications);

    return router;
  }



  generateNotification(req: Request, res: Response) {
    var index: number;
    notification.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      .exec(function (err: any, doc: any) {
        if (err) {
          return err;
        } else if (!res) {
          console.log("ERROR - generateNotification  - index is null")
          return;
        } else {
          console.log(doc);
          index = doc.index;
        }
      })
    console.log(index);
    mongoose.Promise = global.Promise;
    mongoose.connect(mongoURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).then(() => {
      console.log('Database sucessfully connected')

      var notifInstance = new notification();
      //Normal way
      notifInstance.id = "notif" + index.toString();
      notifInstance.index = index + 1;
      notifInstance.severity = req.body.severity;
      notifInstance.message = req.body.message;
      notifInstance.source = req.body.source;
      notifInstance.isRead = false;

      //debug way
      // console.log(notificationCounter.toString());
      // notifInstance.id = "notif" + notificationCounter.toString();
      // notifInstance.index = index + 1;
      // notifInstance.severity = "low";
      // notifInstance.message = "Pethane h gata mou " + index.toString();
      // notifInstance.source = "Waste Management"
      // notifInstance.isRead = false;

      latestRead = index;
      notificationCounter++;
      newNotifications++;

      notifInstance.save(function (err: any) {
        if (err) return console.log(err);
      });

      res.status(200);
      res.json({
        "message": notifInstance
      });
    },
      (error: any) => {
        console.log('Database could not connect: ' + error)
      })
  }

  deleteNotification(req: Request, res: Response) {
    console.log(req.query.notification_id);
    notification.deleteOne({ "index": parseInt(req.query.notification_id.toString()) })
      .exec(function (err: any, doc: any) {
        if (err) {
          return err;
        } else if (!res) {
          res.status(500);
          res.json({
            "message": "No product found in Running Out List"
          });
          return;
        } else {
          res.status(200);
          res.json({
            "isDeleted": true,
            "notificationId": req.query.notificationid
          });
        }
      })
  }

  getAllNotifications(req: Request, res: Response) {
    notification.find({})
      .exec(function (err: any, doc: any) {
        if (err) {
          return err;
        } else if (!res) {
          res.status(500);
          res.json({
            "message": "No notifications"
          });
          return;
        } else {
          res.status(200);
          res.json({
            "notifications": doc
          });
        }
      })
  }

  getAllNotificationsBySeverity(req: Request, res: Response) {
    var criteria;
    var query;

    if (!req.query.criteria) {
      res.status(500);
      res.json({
        "message": "No Criteria",
        "API": "getAllNotificationsBySeverity"
      });
      return;
    } else {
      criteria = req.query.criteria.toString();
    }

    if (criteria == 'info' ||
      criteria == 'low' ||
      criteria == 'high' ||
      criteria == 'severe') {
      query = {
        severity: criteria
      };
    } else {
      res.status(500);
      res.json({
        "message": "Incorrect Criteria",
        "API": "getAllNotificationsBySeverity"
      });
      return;
    }
    notification.find(query)
      .exec(function (err: any, doc: any) {
        if (err) {
          return err;
        } else if (!res) {
          res.status(500);
          res.json({
            "message": "No notifications"
          });
          return;
        } else {
          res.status(200);
          res.json({
            "notifications": doc
          });
        }
      })
  }

  getLatestNotification(req: Request, res: Response) {
    notification.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      .exec(function (err: any, doc: any) {
        if (err) {
          return err;
        } else if (!res) {
          res.status(500);
          res.json({
            "success": false,
            "message": "No notifications"
          });
          return;
        } else {
          res.status(200);
          res.json({
            "success": true,
            "data": doc
          });
          return doc.index;
        }
      })
  }

  readAllNotifications(req: Request, res: Response) {
    var query = { isRead: false };
    var update = { isRead: true };
    notification.updateMany(query, update, { upsert: false })
      .exec(function (err: any, doc: any) {
        console.log(doc);
        if (err) {
          return err;
        } else if (!doc) {
          res.status(500);
          res.json({
            "success": false,
            "message": "No notifications"
          });
          return;
        } else {
          // this.latestRead = doc.index;//up to date
          // this.newNotifications = 0;
          res.status(200);
          res.json({
            "success": true
          });
        }
      })
  }

  getUnreadNotifications(req: Request, res: Response) {
    notification.find({ isRead: false })
      .exec(function (err: any, doc: any) {
        if (err) {
          return err;
        } else if (!res) {
          res.status(500);
          res.json({
            "success": false,
            "message": "No notifications"
          });
          return;
        } else {
          res.status(200);
          res.json({
            "success": true,
            "data": doc,
            "unreadNotifications": doc.length
          });
        }
      })
  }
}

