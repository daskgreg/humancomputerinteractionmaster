import Multer from 'multer';
import { Request, Response, NextFunction, Router } from 'express';
import { NotFound, BadRequest } from 'http-errors';
import { ContainersController } from '../containers/containers.controller';
// import { DIContainer, MinioService } from '@app/services';

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
let mongoose = require('mongoose');

var env = require('../../../config/environment');

var shoppingListProduct = require('../../../models/ShoppingListProduct');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req: any, res: any, next: any) {
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

  //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Pass to next layer of middleware
  next();
});

export class ShoppingListController {

  /**
   * Apply all routes for files
   * POST /files/upload               Upload a new file
   * GET /files/download/:filename    Download a file by its name
   *
   * @returns {Router}
   */
  public applyRoutes(): Router {
    const router = Router();

    router
      .post('/addProductToShoppingList', this.addProductToShoppingList)
      .get('/removeProductFromShoppingList', this.removeProductFromShoppingList)
      .get('/getAllShoppingListNames', this.getAllShoppingListNames)
      .get('/checkIfProductIsInShoppingList', this.checkIfProductIsInShoppingList)
      .get('/setAmmountToBuyOfProduct', this.setAmmountToBuyOfProduct)
      .get('/getAllProductsByCategory', this.getAllProductsByCategory)
      .get('/emptyShoppingList', this.emptyShoppingList);

    return router;
  }

  addProductToShoppingList(req: Request, res: Response) {
    console.log("inside addProduct");
    console.log(req.body);
    if (req.body.product_details.id === "") {
      res.status(500);
      res.json({
        "message": "product id id empty"
      });
      return;
    }
    mongoose.Promise = global.Promise;
    mongoose.connect(mongoURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).then(() => {
      var p = new shoppingListProduct();
      p.product_details.id = req.body.product_details.id;
      p.product_details.name = req.body.product_details.name;
      p.details.ammount_to_buy = req.body.details.ammount_to_buy;
      p.details.category = req.body.details.category;
      p.details.tags = req.body.details.tags;
      p.image.image_type = req.body.image.image_type;
      p.image.image_data = req.body.image.image_data;

      p.save(function (err: any) {
        if (err) return console.log(err);
      });

      res.status(200);
      res.json({
        "success": true
      });
    },
      (error: any) => {
        console.log('Database could not connect: ' + error)
      })
  }

  removeProductFromShoppingList(req: Request, res: Response) {
    shoppingListProduct.deleteOne({ "product_details.id": req.query.productid })
      .exec(function (err: any, doc: any) {
        if (err) {
          return err;
        } else if (!res) {
          res.status(500);
          res.json({
            "success": false,
            "message": "No product found"
          });
          return;
        } else {
          res.status(200);
          res.json({
            "success": true,
            "productId": req.query.productid
          });
        }
      })
  }

  checkIfProductIsInShoppingList(req: Request, res: Response) {
    shoppingListProduct.findOne({ "product_details.id": req.query.productid })
      .exec(function (err: any, doc: any) {
        if (err) {
          return err;
        } else if (!doc) {
          res.status(200);
          res.json({
            "exists": false
          });
          return;
        } else {
          console.log(doc);
          if (doc.product_details.id == req.query.productid) {

            res.status(200);
            res.json({
              "exists": true
            });
            return;
          }
          res.status(500);
          res.json({
            "exists": false
          });
          return;
        }
      })
  }

  getAllShoppingListNames(req: Request, res: Response) {
    shoppingListProduct.find({})
      .exec(function (err: any, prod: any) {
        if (err) {
          return err;
        } else if (!prod) {
          res.status(500);
          res.json({
            "success": false,
            "message": "No products found!"
          });
          return;
        } else {
          var names;

          console.log(prod);
          res.status(200);
          res.send({
            "success": true,
            "data": prod
          });
          return;
        }
      })
  }

  getAllProductsByCategory(req: Request, res: Response) {
    shoppingListProduct.find({ "details.category": req.query.category.toString().toLowerCase() })
      .exec(function (err: any, prod: any) {
        if (err) {
          return err;
        } else if (!prod) {
          res.status(500);
          res.json({
            "success": false,
            "message": "No products found!"
          });
          return;
        } else {
          console.log(prod);
          res.status(200);
          res.send({
            "success": true,
            "data": prod
          });
          return;
        }
      })
  }

  setAmmountToBuyOfProduct(req: Request, res: Response) {
    var query = { "product_details.id": req.query.productid };
    var update = { "details.ammount_to_buy": (req.query.ammount) };

    shoppingListProduct.findOneAndUpdate(query, update, { upsert: false },
      function (err: any, doc: any) {
        if (err) {
          res.status(500);
          res.json({
            "error": err
          });
          return;
        }
        res.status(200);
        res.json({
          "success": true
        });
        return;
      });
  }

  emptyShoppingList(req: Request, res: Response) {
    shoppingListProduct.remove({}).
      exec(function (err: any, doc: any) {
        if (err) {
          res.status(500);
          res.json({
            "error": err
          });
          return;
        }
        res.status(200);
        res.json({
          "success": true
        });
        return;
      });
  }

}