import Multer from 'multer';
import { Request, Response, NextFunction, Router, request } from 'express';
import { NotFound, BadRequest } from 'http-errors';
import { DIContainer, MinioService } from '@app/services';
import { template } from 'lodash';
import { ShoppingListController } from '../shopping-list/shopping-list.controller';

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var mongoURL = 'mongodb://ami-fullstack-admin:i7iPL2ABWzC0J3euvEaKATnU8D9DKZCg@database:27000/ami-fullstack?authSource=admin';
let mongoose = require('mongoose');

var env = require('../../../config/environment');

var product = require('../../../models/Products');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req: any, res: any, next: any) {
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

    //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});

export class ProductsController {
    /**
     * Apply all routes for files
     *
     *
     * @returns {Router}
     */
    public applyRoutes(): Router {
        const router = Router();

        router
            .get('/setFavouriteProduct', this.setFavouriteProduct)
            .get('/getFavouriteProducts', this.getFavouriteProduct)
            .get('/removeFavouriteProduct', this.removeFavouriteProduct)
            .get('/setRecurringProduct', this.setRecurringProduct)
            .get('/getRecurringProducts', this.getRecurringProduct)
            .get('/removeRecurringProduct', this.removeRecurringProduct)
            .get('/refillProduct', this.refillProduct)
            .get('/removeAmmountFromProduct', this.removeAmmountFromProduct)
            .get('/checkProductBelowPercentage', this.checkProductBelowPercentage)
            .get('/getAllProductNames', this.getAllProductNames)
            .get('/removeProductFromProductsList', this.removeProductFromProductsList)
            .get('/addProductToProductsList', this.addProductToProductsList)
            .get('/checkIfProductExists', this.checkIfProductExists)
            .get('/getExpiringProducts', this.getExpiringProducts)
            .get('/checkProductWithIdExpired', this.checkProductWithIdExpired)
            .get('/getExpiringProductsNames', this.getExpiringProductsNames)//chatbot
            .get('/getProductWithPhoneticName', this.getProductWithPhoneticName)
            .get('/getExpiredProducts', this.getExpiredProducts)
            .get('/getAlmostRanOutProducts', this.getAlmostRanOutProducts)
            .get('/getEmptyProducts', this.getEmptyProducts)
            .get('/getFullProducts', this.getFullProducts)
            .get('/getAllProducts', this.getAllProducts)
            .get('/getExpiredProductsByCategory', this.getExpiredProductsByCategory)
            .get('/getAlmostRanOutProductsByCategory', this.getAlmostRanOutProductsByCategory)
            .get('/getEmptyProductsByCategory', this.getEmptyProductsByCategory)
            .get('/getFullProductsByCategory', this.getFullProductsByCategory)
            .get('/getAllProductsByCategory', this.getAllProductsByCategory)
            .get('/setExpirationDateOfProduct', this.setExpirationDateOfProduct)
            .post('/addCustomProduct', this.addCustomProduct);

        return router;
    }

    setFavouriteProduct(req: Request, res: Response) {
        var query = { "product_details.id": req.query.productid };
        var update = { is_favourite: true };

        product.findOneAndUpdate(query, update, { upsert: false },
            function (err: any, doc: any) {
                if (err) {
                    res.status(500);
                    res.json({
                        "error": err
                    });
                    return;
                }
                res.status(200);
                res.json({
                    "Success": true,
                    "product": doc
                });
                return;
            });
    }

    getFavouriteProduct(req: Request, res: Response) {
        product.find({ is_favourite: true },
            function (err: any, docs: any) {
                if (err) {
                    res.status(500);
                    res.json({
                        "success": false,
                        "error": err
                    });
                    return;
                }
                res.status(200);
                res.json({
                    "success": true,
                    "data": docs
                });
                return;
            });
    }

    removeFavouriteProduct(req: Request, res: Response) {
        var query = { "product_details.id": req.query.productid };
        var update = { is_favourite: false };

        product.findOneAndUpdate(query, update, { upsert: false },
            function (err: any, doc: any) {
                if (err) {
                    res.status(500);
                    res.json({
                        "error": err
                    });
                    return;
                }
                res.status(200);
                res.json({
                    "Success": true,
                    "product": doc
                });
                return;
            });
    }

    setRecurringProduct(req: Request, res: Response) {
        var query = { "product_details.id": req.query.productid };
        var update = { is_recurring: true };

        product.findOneAndUpdate(query, update, { upsert: false },
            function (err: any, doc: any) {
                if (err) {
                    res.status(500);
                    res.json({
                        "error": err
                    });
                    return;
                }
                res.status(200);
                res.json({
                    "Success": true,
                    "product": doc
                });
                return;
            });
    }

    getRecurringProduct(req: Request, res: Response) {
        product.find({ is_recurring: true },
            function (err: any, docs: any) {
                if (err) {
                    res.status(500);
                    res.json({
                        "success": false,
                        "data": err
                    });
                    return;
                }
                res.status(200);
                res.json({
                    "success": true,
                    "data": docs
                });
                return;
            });
    }

    removeRecurringProduct(req: Request, res: Response) {
        var query = { "product_details.id": req.query.productid };
        var update = { is_recurring: false };

        product.findOneAndUpdate(query, update, { upsert: false },
            function (err: any, doc: any) {
                if (err) {
                    res.status(500);
                    res.json({
                        "error": err
                    });
                    return;
                }
                res.status(200);
                res.json({
                    "Success": true,
                    "product": doc
                });
                return;
            });
    }

    refillProduct(req: Request, res: Response) {//CHECK
        var max;

        product.find({ "product_details.id": req.query.productid })
            .exec(function (err: any, prod: any) {

                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "status": "500",
                        "message": "No products found!"
                    });
                    return;
                } else {

                    if (req.query.action == "undo") {
                        //undo refill
                        //settarw amount = last ammount

                    } else if (req.query.action == "refill") {

                        if (prod[0].details.full_ammount_potential == prod[0].details.ammount_remaining) {
                            res.status(200);
                            res.json({
                                "already_full": true
                            });
                            return;
                        }
                        max = prod[0].details.full_ammount_potential;

                        prod[0].details.last_ammount = prod[0].details.ammount_remaining;

                        var query = { "product_details.id": req.query.productid };

                        var update = {
                            "details.ammount_remaining": max,
                            "details.last_ammount": prod[0].details.ammount_remaining.toString()
                        };

                        console.log("else");
                        product.findOneAndUpdate(query, update, { upsert: false },
                            function (err: any, doc: any) {
                                if (err) {
                                    res.status(500);
                                    res.json({
                                        "error": err
                                    });
                                    return;
                                }
                                res.status(200);
                                res.json({
                                    "Success": true
                                });
                                return;
                            });

                    } else {
                        res.status(500);
                        res.json({
                            "message": "Illegal Action"
                        });
                        return;
                    }



                }
            })


    }

    removeAmmountFromProduct(req: Request, res: Response) {
        var currentAmmount;

        product.find({ "product_details.id": req.query.productid })
            .exec(function (err: any, prod: any) {

                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "status": "500",
                        "message": "No products found!"
                    });
                    return;
                } else {
                    console.log("else");
                    currentAmmount = prod[0].details.ammount_remaining;


                    var newAmmount = parseInt(currentAmmount) - parseInt(req.query.ammount.toString());

                    if (newAmmount < 0) {
                        newAmmount = 0;
                    }

                    var query = { "product_details.id": req.query.productid };
                    var update = { "details.ammount_remaining": newAmmount.toString() };

                    console.log("else");
                    product.findOneAndUpdate(query, update, { upsert: false, new: true },
                        function (err: any, doc: any) {
                            if (err) {
                                res.status(500);
                                res.json({
                                    "error": err
                                });
                                return;
                            }
                            res.status(200);
                            res.json({
                                "Success": true,
                                "product": doc
                            });
                            return;
                        });
                }
            })
    }

    checkProductBelowPercentage(req: Request, res: Response) {
        var product_id = req.query.productid;
        var percentage = parseInt(req.query.percentage.toString());

        product.findOne({ "product_details.id": product_id })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "status": "500",
                        "message": "No products found!"
                    });
                    return;
                } else {
                    var rem = parseInt(prod.details.ammount_remaining);
                    var full = parseInt(prod.details.full_ammount_potential);

                    var remainingPercentage = (parseInt(prod.details.ammount_remaining) / parseInt(prod.details.full_ammount_potential) * 100);
                    console.log(remainingPercentage);



                    if (remainingPercentage <= percentage) {
                        console.log("Below safe zone");
                        //TODO add to shopping list
                        res.status(200);
                        res.json({
                            "Success": true,
                            "isLow": true,
                            "isRecurring": prod.is_recurring,
                            "product": prod
                        });
                        return;
                    }
                    res.status(200);
                    res.json({
                        "Success": true,
                        "isLow": false,
                        "isRecurring": null,
                        "product": prod
                    });
                    return;
                }
            });
    }

    getAllProductNames(req: Request, res: Response) {
        product.find({}, "product_details.name product_details.id location.general_storage")
            // .distinct('product_details.name')
            // .select('product_details.name')
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "status": "500",
                        "message": "No products found!"
                    });
                    return;
                } else {
                    var names;

                    console.log(prod);
                    res.status(200);
                    res.send({
                        prod
                    });
                    return;
                }
            })
    }

    removeProductFromProductsList(req: Request, res: Response) {
        product.deleteOne({ "product_details.id": req.query.product_id })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "status": "500",
                        "message": "No products found!"
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "isDeleted": true,
                        "productId": req.query.product_id
                    });
                    return;
                }
            })
    }

    addProductToProductsList(req: Request, res: Response) {
        mongoose.Promise = global.Promise;
        mongoose.connect(mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).then(() => {
            var p = new product();
            // p.product_details.id = req.body.product_details.id;
            // p.product_details.name = req.body.product_details.name;
            // p.details.category = req.body.details.category;
            // p.details.tags = req.body.details.tags;
            // p.image.image_type = req.body.image.image_type;
            // p.image.image_data = req.body.image.image_data;


            p.save(function (err: any) {
                if (err) return console.log(err);
            });

            res.status(200);
            res.json({
                "product": p
            });
        },
            (error: any) => {
                console.log('Database could not connect: ' + error)
            })
    }

    checkIfProductExists(req: Request, res: Response) {
        product.findOne({ "product_details.id": req.query.product_id })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(200);
                    res.json({
                        "exists": false
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "exists": false,
                        "product": prod
                    });
                    return;
                }
            })
    }

    getExpiringProducts(req: Request, res: Response) {
        product.find({ "details.exp_date": { "$lt": new Date() } })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(200);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        prod
                    });
                    return;
                }
            })
    }

    getExpiringProductsNames(req: Request, res: Response) {
        var date = new Date();
        var d = date.setDate(date.getDate() - 1);
        product.find({ "details.exp_date": { "$lt": new Date(), "$gt": d } }, "product_details.name")
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(200);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    var names: any = [];
                    for (var i = 0; i < prod.length; i++) {
                        names.push(prod[i].product_details.name);
                    }
                    res.status(200);
                    res.json({
                        prod
                    });
                    return;
                }
            })
    }

    checkProductWithIdExpired(req: Request, res: Response) {
        product.findOne({ "product_details.id": req.query.productid })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(200);
                    res.json({
                        "success": false,
                        "message": "couldn't find product with id " + req.query.productid
                    });
                    return;
                } else {
                    if (prod.details.exp_date <= new Date()) {
                        res.status(200);
                        res.json({
                            "success": true,
                            "expired": true,
                            "product": prod
                        });
                        return;
                    }
                    res.status(200);
                    res.json({
                        "success": true,
                        "expired": false,
                        "product": prod
                    });
                    return;
                }
            })
    }

    getProductWithPhoneticName(req: Request, res: Response) {
        var phonetic = req.query.phoneticname.toString();
        console.log(phonetic);
        phonetic = phonetic.replace("%20", "");
        phonetic = phonetic.replace(" ", "");
        product.findOne({ "iot.phonetic_name": phonetic }, "product_details.id")
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(200);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    var names: any = [];
                    for (var i = 0; i < prod.length; i++) {
                        names.push(prod[i].product_details.name);
                    }
                    res.status(200);
                    res.json({
                        prod
                    });
                    return;
                }
            })
    }

    getExpiredProducts(req: Request, res: Response) {
        product.find({ "details.exp_date": { "$lt": new Date() } })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(200);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": prod
                    });
                    return;
                }
            })
    }

    getAlmostRanOutProducts(req: Request, res: Response) {
        product.find()
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    var responseArray: any = [];
                    prod.forEach((element: any) => {
                        var currentAmmount = element.details.ammount_remaining;
                        var maxAmmount = element.details.full_ammount_potential;
                        var remainingRate = currentAmmount / maxAmmount;
                        var remainingPercentage = remainingRate * 100;
                        if ((remainingPercentage < 25) && (remainingPercentage > 0)) {
                            responseArray.push(element);
                        }
                        if (remainingPercentage > 100) {
                            console.log("Minor Error - product remaining greater that 100%.")
                        }
                    });
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": responseArray
                    })
                }
            })
    }

    getEmptyProducts(req: Request, res: Response) {
        product.find({})
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    var responseArray: any = [];
                    prod.forEach((element: any) => {
                        var currentAmmount = element.details.ammount_remaining;
                        var maxAmmount = element.details.full_ammount_potential;
                        var remainingRate = currentAmmount / maxAmmount;
                        var remainingPercentage = remainingRate * 100;
                        if (remainingPercentage == 0) {
                            responseArray.push(element);
                        }
                        if (remainingPercentage > 100) {
                            console.log("Minor Error - product remaining greater that 100%.")
                        }
                    });
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": responseArray
                    })
                }
            })
    }

    getFullProducts(req: Request, res: Response) {
        product.find({})
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    var responseArray: any = [];
                    prod.forEach((element: any) => {
                        if (element.details.exp_date > new Date()) {
                            var currentAmmount = element.details.ammount_remaining;
                            var maxAmmount = element.details.full_ammount_potential;
                            var remainingRate = currentAmmount / maxAmmount;
                            var remainingPercentage = remainingRate * 100;
                            if (remainingPercentage > 26) {
                                responseArray.push(element);
                            }
                            if (remainingPercentage > 100) {
                                console.log("Minor Error - product remaining greater that 100%.")
                            }
                        }
                    });
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": responseArray
                    })
                }
            })
    }

    getAllProducts(req: Request, res: Response) {
        product.find({})
            // .distinct('product_details.name')
            // .select('product_details.name')
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false,
                        "message": "No products found!"
                    });
                    return;
                } else {
                    var names;

                    console.log(prod);
                    res.status(200);
                    res.send({
                        "success": true,
                        "data": prod
                    });
                    return;
                }
            })
    }

    getExpiredProductsByCategory(req: Request, res: Response) {
        product.find({ "details.category": req.query.category, "details.exp_date": { "$lt": new Date() } })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(200);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": prod
                    });
                    return;
                }
            })
    }

    getAlmostRanOutProductsByCategory(req: Request, res: Response) {
        product.find({ "details.category": req.query.category })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    var responseArray: any = [];
                    prod.forEach((element: any) => {
                        var currentAmmount = element.details.ammount_remaining;
                        var maxAmmount = element.details.full_ammount_potential;
                        var remainingRate = currentAmmount / maxAmmount;
                        var remainingPercentage = remainingRate * 100;
                        if ((remainingPercentage < 26) && (remainingPercentage > 0)) {
                            responseArray.push(element);
                        }
                        if (remainingPercentage > 100) {
                            console.log("Minor Error - product remaining greater that 100%.")
                        }
                    });
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": responseArray
                    })
                }
            })
    }

    getEmptyProductsByCategory(req: Request, res: Response) {
        product.find({ "details.category": req.query.category })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    var responseArray: any = [];
                    prod.forEach((element: any) => {
                        var currentAmmount = element.details.ammount_remaining;
                        var maxAmmount = element.details.full_ammount_potential;
                        var remainingRate = currentAmmount / maxAmmount;
                        var remainingPercentage = remainingRate * 100;
                        if (remainingPercentage == 0) {
                            responseArray.push(element);
                        }
                        if (remainingPercentage > 100) {
                            console.log("Minor Error - product remaining greater that 100%.")
                        }
                    });
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": responseArray
                    })
                }
            })
    }

    getFullProductsByCategory(req: Request, res: Response) {
        product.find({ "details.category": req.query.category })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false
                    });
                    return;
                } else {
                    var responseArray: any = [];
                    prod.forEach((element: any) => {
                        if (element.details.exp_date > new Date()) {
                            var currentAmmount = element.details.ammount_remaining;
                            var maxAmmount = element.details.full_ammount_potential;
                            var remainingRate = currentAmmount / maxAmmount;
                            var remainingPercentage = remainingRate * 100;
                            if (remainingPercentage > 26) {
                                responseArray.push(element);
                            }
                            if (remainingPercentage > 100) {
                                console.log("Minor Error - product remaining greater that 100%.")
                            }
                        }
                    });
                    res.status(200);
                    res.json({
                        "success": true,
                        "data": responseArray
                    })
                }
            })
    }

    getAllProductsByCategory(req: Request, res: Response) {
        product.find({ "details.category": req.query.category })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false,
                        "message": "No products found!"
                    });
                    return;
                } else {
                    var names;

                    console.log(prod);
                    res.status(200);
                    res.send({
                        "success": true,
                        "data": prod
                    });
                    return;
                }
            })
    }

    setExpirationDateOfProduct(req: Request, res: Response) {
        var query = { "product_details.id": req.query.product_id };
        console.log(req.query.month);
        var date = new Date(parseInt(req.query.year.toString()), parseInt(req.query.month.toString()) - 1, parseInt(req.query.day.toString()));
        var update = { "details.exp_date": date };

        product.findOneAndUpdate(query, update, { new: false })
            .exec(function (err: any, prod: any) {
                if (err) {
                    return err;
                } else if (!prod) {
                    res.status(500);
                    res.json({
                        "success": false,
                        "message": "No products found!"
                    });
                    return;
                } else {
                    res.status(200);
                    res.send({
                        "success": true
                    });
                    return;
                }
            })
    }

    addCustomProduct(req: Request, res: Response) {
        mongoose.Promise = global.Promise;
        mongoose.connect(mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).then(() => {
            var p = new product();
            p.product_details.id = "";
            p.product_details.name = req.body.name;
            p.details.category = "custom";
            p.details.full_ammount_potential = "";
            p.details.ammount_remaining = "";
            p.details.ammount_unit = "";
            p.details.prod_date = new Date();
            p.details.prod_date instanceof Date;
            p.details.exp_date = "";
            p.details.prod_date instanceof Date;
            p.details.tags = "";
            p.location.room = "Kitchen";
            p.location.general_storage = "shelves";//cabinet
            p.location.general_storage_location = "";//top left cabbinet
            p.location.inside_storage_location = "";//top left INSIDE cabbinet
            p.iot.container_id = "";
            p.image.image_type = "image/png";
            p.image.image_data = "";
            p.is_custom = true;
            p.is_favourite = false;
            p.is_recurring = false;

            p.save(function (err: any) {
                if (err) return console.log(err);
            });

            res.status(200);
            res.json({
                "message": p
            });
        },
            (error: any) => {
                console.log('Database could not connect: ' + error)
            })
    }
}
