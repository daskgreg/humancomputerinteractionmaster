var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var notificationSchema = new Schema({
    id: String,
    index: Number,
    severity: {
        type: String,
        enum: ['info', 'low', 'high', 'severe']
    },
    message: String,
    source: String,
    remainingNotifications: Number,//kitchen, shoppinglist etc
    isRead: Boolean
});
//ACTIVE SCHEMA
//var user = mongoose.model('User', userSchema);
module.exports = mongoose.model('Notification', notificationSchema);
// export const mongooseProdSchema = mongoose.model('ShoppingListProduct', shoppingListProductSchema);
//ACTIVE SCHEMA