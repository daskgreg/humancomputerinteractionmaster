
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var shoppingListProductSchema = new Schema({
    product_details: {
        id: String,
        name: String
    },
    details: {
        ammount_to_buy: Number,
        category: String,
        ammount_type: String,
        tags: [String]
    },
    image: {
        image_type: String,
        image_data: Buffer
    }
});
//ACTIVE SCHEMA
//var user = mongoose.model('User', userSchema);
module.exports = mongoose.model('ShoppingListProduct', shoppingListProductSchema);
// export const mongooseProdSchema = mongoose.model('ShoppingListProduct', shoppingListProductSchema);
//ACTIVE SCHEMA