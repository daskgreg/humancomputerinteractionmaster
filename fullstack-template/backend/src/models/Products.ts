//ACTIVE SCHEMA
/*
The main schema used by SIM in order to keep track of basic
information regarding the products.

product_details.id
product_details.name

details.category
details.ammount_remaining
details.ammount_unit
details.prod_date
details.exp_date
details.tags

location.
location.
location.
location.

iot

image.
image.

is_custom
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var productSchema = new Schema({
    product_details:{
        id: String,
        name: String
    },
    details:{
        category: String,
        ammount_remaining: String,//CHANGED CHECK
        full_ammount_potential: String,
        ammount_unit: String,
        /*ammount_unit:{
            type: String,
            enum: ['gr', 'pcs']
        } ,*/
        prod_date: Date,
        exp_date: Date,
        tags: [String],
        last_ammount: String
    },
    location:{
        room: String,
        general_storage: String,//
        general_storage_location: String,//upper, lower
        inside_storage_location: String//upper part, lower part
    },
    iot:{
        container_id: String,
        phonetic_name: String
    },
    image:{
        image_type: String,
        image_data: Buffer
    },
    is_custom:{
        type: Boolean,
        default: false
    },
    is_recurring:{
        type: Boolean,
        default: false
    },
    is_favourite:{
        type: Boolean,
        default: false
    }
});
//ACTIVE SCHEMA
//var user = mongoose.model('User', userSchema);
module.exports = mongoose.model('Product', productSchema);
export const mongooseProdSchema = mongoose.model('Product', productSchema);
//ACTIVE SCHEMA