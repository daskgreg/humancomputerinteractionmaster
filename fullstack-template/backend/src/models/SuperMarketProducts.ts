var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var superMarketProductSchema = new Schema({
    product_details:{
        prod_id: String,
        name: String
    },
    details:{
        // ammount_unit: String,
        price: Number,
        ammountType: String,
        category: String,
        tags: [String]
    },
   image:{
      image_type: String,
      image_data: Buffer
   }
});
//ACTIVE SCHEMA
//var user = mongoose.model('User', userSchema);
module.exports = mongoose.model('SuperMarketProduct', superMarketProductSchema);
// export const mongooseProdSchema = mongoose.model('ShoppingListProduct', shoppingListProductSchema);
//ACTIVE SCHEMA