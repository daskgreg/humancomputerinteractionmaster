var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: {
      first: String,
      last: String
    },
    email:{
      type: String
    } ,
    password:{
      type: String
    } ,
    img:{data: Buffer, contentType: String}
});

//var user = mongoose.model('User', userSchema);
module.exports = mongoose.model('User', userSchema);