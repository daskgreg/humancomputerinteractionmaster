var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var receiptProductSchema = new Schema({
    product_details:{
        prod_id: String,
        name: String,
        receipt_id: String
    },
    details:{
        ammount: Number,
        ammount_unit: String,
        price: Number,
        prod_date: String,
        exp_date: String
    },
   image:{
      image_type: String,
      image_data: Buffer
   }
});

module.exports = mongoose.model('ReceiptProduct', receiptProductSchema);
