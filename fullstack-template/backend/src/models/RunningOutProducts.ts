var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var RunningOutProductSchema = new Schema({
    product_details:{
        id: String,
        name: String
    },
    details:{
        category: String,
        tags: [String]
    },
    image:{
        image_type: String,
        image_data: Buffer
    }
});
//ACTIVE SCHEMA
//var user = mongoose.model('User', userSchema);
module.exports = mongoose.model('RunningOutProduct', RunningOutProductSchema);
// export const mongooseProdSchema = mongoose.model('RunningOutProduct', RunningOutProductSchema);
//ACTIVE SCHEMA