var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var containerSchema = new Schema({
    location: String,//ex. Cabbinet2
    base_id: String,//Container_7_base
    corresponding_container: String,//Container_7
    current_container: String,//any container(ex. Container_8)
    has_correct_container: Boolean//if corresponding != current, this is false
});
//ACTIVE SCHEMA
module.exports = mongoose.model('Container', containerSchema);
//ACTIVE SCHEMA