//ACTIVE SCHEMA
/*
This Schema is used when the receipt is scanned, in order for the
user to validate the products they bought. The products schema in
this model is IRRELEVANT to the products schema that will be used
by the S.I.M. in order for it to keep track of product-based info
such as in-gouse position, exp. dates, etc.
###################### receiptProductSchema ######################
product_details.id the product's id universal for all Supermarkets
product_details.name the prosuct's name

details.ammount: the ammount of the product bought
details.ammount_unit: Kg, gr, pcs etc.
details.price: the product's price
details.prod_date: production date
details.exp_date: expiration date

######################### receiptSchema ###########################
general_info.date: The date of the transaction
general_info.receipt_id: Receipt's uniquie id

super_market.name: The store's name
super_market.address: The store's address

payment_details.ammount: Total money paid
payment_details.currency: currency in which money was paid
payment_details.method: Card, cash, bitcoin, kidney etc.
payment_details.vat_percent: Taxation

user_details.name.first
user_details.name.last

image.imageType: the file of the receipt's image
image.imageData: base64 image file of the receipt

products: array of all the products on the receipt

remarks: Any remarks regarrding the transaction
*/ 

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
const recProd = require('./ReceiptProducts');

var receiptSchema = new Schema({
    receipt_id: String,
    general_info:{
        date: {
          type: Date
        }
    },
    super_market:{
        name: String,
        address: String
    },
    payment_details: {
        ammount: String,
        currency: String,
        method: String,
        vat_percent: String
    },
    user_details:{
        name:{
            first: String,
            last: String
        }
    },
    image:{
        imageType: String,
        imageData: Buffer
    },
    products:[{
        product_details:{
            prod_id: String,
            name: String,
            receipt_id: String
        },
        details:{
            ammount: String,
            ammount_unit: String,
            price: String,
            prod_date: String,
            exp_date: String
        },
        image:{
            image_type: String,
            image_data: Buffer
        }
    }],
    remarks:{
        type: String
    }
});

//ACTIVE SCHEMA
module.exports = mongoose.model('Receipt', receiptSchema);
//ACTIVE SCHEMA
