var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var inCart = new Schema({
    name: String,
    id: String
});

module.exports = mongoose.model('InCartProducts', inCart);