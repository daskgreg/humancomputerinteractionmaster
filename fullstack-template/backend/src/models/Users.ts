//ACTIVE SCHEMA
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    first_name:{
      type: String
    } ,
    last_name:{
      type: String
    } ,
    email: {
      type:String,
      unique: true
    },
    password:{
      type: String
    },
    image_type: {
      type: String
    },
    image_data: {
      type: Buffer
    }
});
//ACTIVE SCHEMA
//var user = mongoose.model('User', userSchema);
module.exports = mongoose.model('User', userSchema);
//ACTIVE SCHEMA